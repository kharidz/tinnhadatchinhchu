<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_Payment extends Model
{
    protected $table = 'history_payment';

    protected $fillable = ['content', 'created_at'];
}
