<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'district';

    protected $primaryKey = 'code';

    protected $fillable = ['name', 'type','province_code'];

    protected $timestamp = false;
}
