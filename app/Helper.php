<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Helper extends Model
{
	/**
	 * create slug
	 * @param  string $string ex: Giới thiệu
	 * @return string         echo slug
	 */
    public static function create_slug($string){
        $string = preg_replace('/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/', 'a', $string);
        $string = preg_replace('/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/', 'e', $string);
        $string = preg_replace('/ì|í|ị|ỉ|ĩ/', 'i', $string);
        $string = preg_replace('/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/', 'o', $string);
        $string = preg_replace('/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/', 'u', $string);
        $string = preg_replace('/ỳ|ý|ỵ|ỷ|ỹ/', 'y', $string);
        $string = preg_replace('/(đ)/', 'd', $string);
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return strtolower($slug);
    }
    /**
     * Set value seo desc
     * @param  string  $string description
     * @param  integer $total  total substr
     * @return string
     */
    public static function desctoseo($string = null, $total = 0){
    	$string = strip_tags($string);
    	$string = substr($string, 0, $total);
    	return $string;
    }
}
