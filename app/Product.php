<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Product_categories;
class Product extends Model
{
    use Sluggable;
    protected $table = 'products';

    protected $fillable = ['name', 'slug', 'content', 'province_id', 'district_id', 'address', 'price', 'area', 'contact_phone', 'status', 'created_at'];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getcate()
    {
        $cates = $this->hasMany('App\Product_cate_product', 'product_id')->get();
        $result = '';
        foreach ($cates as $cate) {
            $cate = Product_categories::where('id', $cate->product_cat_id)->first();
            $result .= ', '.($cate['name']) ? $cate['name'] : "";
        }
        $result = (!$result) ? 'Không danh mục': $result;
        return trim($result, ' ,');
    }

    public function getprovince(){
        $province = $this->belongsTo('App\Province', 'province_id')->first();
        return ($province) ? $province->name : "Rỗng";
    }

    public function getdistrict(){
        $district = $this->belongsTo('App\District', 'district_id')->first();
        return ($district) ? $district->name : "Rỗng";
    }

    public function pro_cate()
    {
        return $this->hasMany('App\Product_cate_product', 'product_id', 'id');
    }

    public function get_user(){
        return $this->hasMany('App\User_Products', 'pro_id');
    }
}
