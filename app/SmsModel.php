<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsModel extends Model
{
    protected $phone, $content;
    /**
     * @param string $phone
     * @return SmsModel object
     */
    public static function to($phone = ''){
        $model = new static();
        $model->phone = $phone;
        return $model;
    }

    /**
     * @param string $content
     * @return object response
     */
    public function send($content = ''){
        $APIKey=env('KEY_SMS');
        $SecretKey=env('SECRET_SMS');
        $YourPhone=$this->phone;
        $Content= ($content) ? $content : env('APP_NAME');

        $SendContent=urlencode($Content);
        $data="http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=$YourPhone&ApiKey=$APIKey&SecretKey=$SecretKey&Content=$SendContent&SmsType=6";
        //De dang ky brandname rieng vui long lien he hotline 0902435340 hoac nhan vien kinh Doanh cua ban
        $curl = curl_init($data);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $obj = json_decode($result,true);
        return $obj;
    }
}
