<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Products extends Model
{
    protected $table = 'user_products';

    protected $fillable = ['user_id', 'pro_id'];

    public $timestamps = false;
}
