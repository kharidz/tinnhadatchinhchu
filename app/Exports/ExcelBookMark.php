<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Product;
class ExcelBookMark implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view() :View
    {
        $products = Product::whereHas('get_user', function($q){
            $q->where('user_id', \Auth::user()->id);
        })->get();

        return view('exports.products', ['products' => $products]);
    }
}
