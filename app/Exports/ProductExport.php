<?php

namespace App\Exports;

use App\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductExport implements FromView
{
    protected $wheres, $cate, $whedate, $title;

    public function __construct($wheres = null, $cate = '', $whedate = null, $title = '')
    {
        $this->wheres = $wheres;
        $this->cate = $cate;
        $this->whedate = $whedate;
        $this->title = $title;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $wheres = $this->wheres;
        $cate = $this->cate;
        $whedate = $this->whedate;
        $title = $this->title;
        $products = Product::whereIn('id', function($q) use ($wheres, $cate, $whedate, $title){
            $q->select('product_id')->from('product_cat_product');
            if (isset($cate)){
                $q->where('product_cat_id', $cate);
            }
            $q->where($wheres);
            if (!empty($whedate)) {
                $q->whereBetween('created_at', [$whedate['to']->format('Y-m-d')." 00:00:00", $whedate['form']->format('Y-m-d')." 23:59:59"]);
            }
            if($title){
                $q->where('name', 'like', "%$title%");
            }
        })->orderBy('created_at', 'desc')->get();
        return view('exports.products', ['products'=>$products]);
    }
}
