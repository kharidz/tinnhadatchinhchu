<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Payment extends Model
{
    protected $table = 'user_payments';

    protected $fillable = ['user_id', 'amount', 'content'];

    public function getmail(){
    	$user = $this->belongsTo('App\User', 'user_id')->first();
    	if (empty($user)){
    	    return "";
        }
    	return ($user->email) ? $user->email : "";
    }
}
