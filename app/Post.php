<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use Sluggable;
    protected $table = 'posts';

    protected $fillable = ['name', 'slug', 'excerpt', 'content', 'seo_title', 'seo_desc', 'seo_images'];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getcate()
    {
        $cates = $this->hasMany('App\Categorie_Post', 'post_id')->get();
        $result = "";
        foreach ($cates as $value) {
            $cate = \App\Categorie::where('id', $value->cate_id)->first();
            $result .= ', '.$cate->name;
        }
        $result = (!$result) ? 'Không danh mục': $result;
        return trim($result, ',');
    }
    public function cate()
    {
        return $this->belongsTo('App\Categorie_Post', 'id', 'post_id');
    }
}
