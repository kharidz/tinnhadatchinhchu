<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_cate_product extends Model
{
    protected $table = 'product_cat_product';

    protected $fillable = ['product_cat_id', 'product_id'];

    public $timestamps = false;
}
