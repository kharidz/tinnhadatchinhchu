<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Campaign;
use App\Send_Campaign;
use App\User;
use App\Subscribe;
use App\Mail\Campagin;
use Illuminate\Support\Facades\DB;
class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:campaign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaign = Campaign::where('status', 0)->first();
        if (empty($campaign)){
            return response("empty");
        }
        $users = DB::table('users')->select('email');
        if ($campaign->type == 1){
            $subscribes = DB::table('subscribes')
                ->select('email');
            $emails = $subscribes->union($users)->get();
        }else if ($campaign->type == 2){
            $emails = User::all('email');
        }else if($campaign->type == 3){
            $emails = Subscribe::all('email');
        }else{
            $emails = Send_Campaign::where('id_campaign', $campaign->id)->select('email')->get();
        }
        foreach($emails as $email){
            \Mail::to($email->email)->send(new Campagin($email->email, $campaign));
            echo $email->email.'\n';
        }
        $campaign->status = 1;
        $campaign->save();
        return response('done');
    }
}
