<?php

namespace App\Providers;

use App\Product;
use Illuminate\Support\ServiceProvider;
use View;
use App\Option;
use App\Banner;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('products') && Schema::hasTable('options')) {
            $contpro = Product::all()->count();
            $title = $this->getSetup('title');
            $logo = $this->getSetup('logo');
            $address = $this->getSetup('address');
            $phone = $this->getSetup('phone');
            $phone = explode('|', $phone);
            $about_img = $this->getSetup('about-img');
            $about_content = $this->getSetup('about-content');
            $link_facebook = $this->getSetup('link-facebook');
            $link_youtube = $this->getSetup('link-youtube');
            $new_title = $this->getSetup('new_title');
            $new_content = $this->getSetup('new_content');
            $new_image = $this->getSetup('new_image');
            $amount = $this->getSetup('amount');
            $text_home = $this->getSetup('text_home');
            $bg_banner = $this->getSetup('bgbanner');

            $cuscss = $this->getSetup('customcss');
            $banners = Banner::orderby('id', 'desc')->get();
            View::share([
                'contpro' => $contpro,
                'title' => $title,
                'address' => $address,
                'logo' => $logo,
                'phone' => $phone,
                'about_img' => $about_img,
                'about_content' => $about_content,
                'link_facebook' => $link_facebook,
                'link_youtube' => $link_youtube,
                'new_title' => $new_title,
                'new_content' => $new_content,
                'new_img' => $new_image,
                'amount' => $amount,
                'banners' => $banners,
                'text_home' => $text_home,
                'bg_banner' => $bg_banner,
                'cuscss' => $cuscss
            ]);
        }
    }

    protected function getSetup($key = null)
    {
        $result = Option::where('opt_key', $key)->first();
        return ($result) ? $result->opt_value : 0;
    }
}
