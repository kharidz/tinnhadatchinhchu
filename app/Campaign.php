<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';

    protected $fillable = ['name', 'subject', 'content', 'schedule', 'type', 'status'];
}
