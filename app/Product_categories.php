<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product_categories extends Model
{
	use Sluggable;
    protected $table = 'product_category';

    protected $fillable = ['name', 'slug', 'description', 'parent_id', 'status', 'seo_title', 'seo_image', 'seo_desc'];


    public function sluggable(){
    	return [
    		'slug'=>[
    			'source' => 'name'
    		]
    	];
    }
    /**
     * Categories Parent
     * @return parent categories
     */
    public function childs(){
        return $this->hasMany('App\Product_categories', 'parent_id');
    }

    public function parent()
    {
        $parent = $this->belongsTo('App\Product_categories', 'parent_id')->first();
        if (empty($parent)) {
            return 'Gốc';
        }
        return $parent->name;
    }
}
