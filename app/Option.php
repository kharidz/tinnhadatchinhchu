<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    protected $fillable = ['opt_key', 'opt_title', 'opt_value'];

    public $timestamps = false;
}
