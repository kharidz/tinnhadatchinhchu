<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View_products extends Model
{
    protected $table = 'view_product';

    protected $fillable = ['user_id', 'pro_id'];

    public $timestamps = false;
}
