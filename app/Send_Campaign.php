<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send_Campaign extends Model
{
    protected $table = 'sends_campaign';
    protected $fillable = ['email', 'id_campaign'];
}
