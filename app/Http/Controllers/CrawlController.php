<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Product;
use App\Product_categories;
use App\Product_cate_product;
use Illuminate\Http\Request;
use Goutte\Client;
use App\District;
use Carbon\Carbon;
class CrawlController extends Controller
{
    public function batdongsanchinhchu(Request $req){
        $id = ($req->input('id')) ? $req->input('id') : null;
        if (!$id){
            return "hi";
        }
        $arr = [3, 12, 21,22];
        if (!in_array($id, $arr)){
            return "empty";
        }
        $hn = [
            'user' => 'toannang.apt@gmail.com',
            'pass' => 'tadenday',
            'province' => 1
        ];
        $this->GetData(1, $id, $hn);
    }
    public function bdschHCM(Request $req){
        $id = ($req->input('id')) ? $req->input('id') : null;
        if (!$id){
            return "hi";
        }
        $arr = [3, 12, 21,22];
        if (!in_array($id, $arr)){
            return "empty";
        }
        $hcm = [
            'user' => 'quytm2408@gmail.com',
            'pass' => 'minhquy2408',
            'province' => 79
        ];
        $this->GetData(1, $id, $hcm);
    }

    public function OldBatdongsanchinhchu(Request $req){
        $id = ($req->input('id')) ? $req->input('id') : null;
        if (!$id){
            return "hi";
        }
        $arr = [3, 12, 21,22];
        if (!in_array($id, $arr)){
            return "empty";
        }
        if ($id == 3){
            $file = 'dat.txt';
        }elseif($id == 12){
            $file = 'thue.txt';
        }elseif($id == 21){
            $file = 'thuedat.txt';
        }
        else{
            $file = 'batdat.txt';
        }
        $fold = file_get_contents(__DIR__.'/file/'.$file);
        $f=fopen(__DIR__.'/file/'.$file,'w');
        fwrite($f,$fold-1);
        fclose($f);
        if($fold < 0){
            return "done";
        }
        $this->GetData($fold, $id, 'date_from=18%2F02%2F2019&date_to=18%2F04%2F2019');
    }

    public function GetData($i = 1, $idcate = 3,$user = null,$data = null){
        $client = new Client();
        $url = 'https://batdongsanchinhchu.vn/wp-login.php';
        $params = ['log' => $user['user'], 'pwd'=>$user['pass'], 'wp-submit'=>'Đăng nhập', 'redirect_to'=> "https://batdongsanchinhchu.vn/tim-kiem-bat-dong-san/page/$i?type_estate=$idcate&$data"];
        $crawler = $client->request('POST', $url, $params);
//        $new = $crawler->filter('.table.table-bordered > tr');
//        foreach(array_reverse($new) as $k => $ne){
//            if ($k != count($new)-1){
//                $a = $new->eq($k);
//                var_dump($a->filter('h4.item-bds-title > a', 0)->text());
//            }
//        }
        $crawler->filter('.table.table-bordered > tr')->each(function ($node, $k) use ($idcate, $i, $user){
            if ($k != 0){
                $cate = $node->filter('td.has-bg span', 0)->text();
                $title = $node->filter('h4.item-bds-title > a', 0)->text();
                $price = $node->filter('.item-bds-more span b', 0)->last();
                $price = $price->text();
                $address = $node->filter('.item-bds-more > span >b')->first();
                $address = explode(',', $address->text());
                $address = $address[0];
                $content = $node->filter('.bds-item-content-high p', 0)->text();
                $created = $node->filter('.item-bds > td.text-center', 0)->first()->text();
                $created = preg_match('/([0-9]+\/+[0-9]+\/[0-9]+)/u', $created, $createed);
                $created = $createed[0];
                $k = $k + $i;
                $newcr = strtotime(str_replace('/', '-', "$created +$k seconds"));
                $created = date('Y-m-d H:i:s', $newcr);
                if ($idcate != 21){
                    $phone = $node->filter('table.sub-table tbody tr', 1)->each(function($pho){
                        preg_match('/([0-9]+)/mi', $pho->text(), $phones);
                        if (!empty($phones)){
                            return $phones[0];
                        }
                    })[1];
                    $adddetail = $node->filter('table.sub-table tbody tr', 0)->first();
                    $adddetail = $adddetail->filter('td')->last()->text();
                }else{
                    $phone = '';
                    $adddetail = '';
                }

                $cate = $this->GetidCate($cate);
                $idcatego = 4;
                if ($idcate == 3){
                    $idcatego = str_replace(3, 1, $idcate);
                }elseif ($idcate == 12){
                    $idcatego = str_replace( 12, 2, $idcate);
                }elseif ($idcate == 21){
                    $idcatego = str_replace( 21, 3, $idcate);
                }else{
                    $idcatego = 4;
                }
                if ($idcate == 21 || $idcate == 22){
                    $listcate = $idcatego;
                }else{
                    $listcate[] = $idcatego;
                    $listcate[] = $cate;
                }
                $data = [
                    'name' => $title,
                    'slug' => Helper::create_slug($title),
                    'content' => $content,
                    'province_id' => $user['province'],
                    'district_id' => $this->GetDistID(trim($address)),
                    'address' =>$adddetail,
                    'price' => $this->ConvertMoney($price),
                    'contact_phone' => $phone,
                    'status' => 1,
                    'created_at' => $created
                ];
                var_dump($this->InsertData($data, $listcate));
                var_dump($title,$created);
            }
        });
    }
    protected function InsertData($data = null, $listcate = null){
        if (empty($data)){
            return false;
        }
        $new = Product::where('slug', $data['slug'])->first();
        if (!empty($new)){
            return false;
        }
        $pro = Product::create($data);
        if (is_array($listcate)){
            foreach ($listcate as $cate){
                Product_cate_product::create([
                    'product_cat_id' => $cate,
                    'product_id' => $pro->id
                ]);
            }
        }else{
            Product_cate_product::create([
                'product_id' => $pro->id,
                'product_cat_id' => $listcate
            ]);
        }
        return "done";
    }
    protected function GetDistID($distric = null){
        $citys = District::all();
        foreach ($citys as $city){
            preg_match("/($distric)/u", $city->name, $newtp);
            if (!empty($newtp)){
                return $city->code;
            }
        }
    }

    protected function GetidCate($cate = null){
        $cate = trim($cate);
        $categories = Product_categories::all();
        foreach ($categories as $category){
            preg_match("/($cate)/u", $category->name, $newcate);
            if (!empty($newcate)){
                return $category->id;
            }
        }
        return 0;
    }
    protected function ConvertMoney($money = null){
        if (empty($money)){
            return "0";
        }
        if (trim($money) == 'Liên hệ.' || trim($money) == 'Liên hệ'){
            return "0";
        }
        $type = preg_split('/([0-9]*\.?[0-9]+)/m', $money);
        if (!is_array($type)){
            $val = 0;
        }else{
            if (count($type) > 1){
                if (preg_match('/(tỷ)/u', trim($type[1]))){
                    $val = 1000000000;
                }elseif(preg_match('/(triệu)/u', trim($type[1]))){
                    $val = 1000000;
                }else{
                    $val = 1;
                }
            }
        }
        preg_match('/([0-9]*\.?[0-9]+)/m', $money, $price);
        if (!empty($price)){
            return $price[0] * $val;
        }
        return "0";
    }
}
