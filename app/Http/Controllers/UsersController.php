<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\shift_master_models;
use App\User;
use App\Product;

class UsersController extends Controller
{
    public function add_agency(Request $rq)
    {
        $product = Product::find($rq->id);
        if (empty($product)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Tin không tồn tại hoặc đã bị xóa'
            ]);
        }

        $user_id = \Auth::user()->id;

        $user = User::find($user_id);
        if (empty($user)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Chưa đăng nhập',
            ]);
        }

        $agency = $user['agency'];

        $arr = array();
        if (empty($agency)) {
            $arr[] = $rq->id;
        } else {
            $arr = json_decode($agency, true);
            $ck = 0;
            foreach ($arr as $key => $val) :
                if ($val == $rq->id) :
                    $ck = 1;
                    return response()->json([
                        'errors' => true,
                        'msg' => 'Tin môi giới đã được thêm'
                    ]);
                endif;
            endforeach;
            if ($ck == 0) :
                $arr[] = $rq->id;
            endif;
        }

        $user->agency = json_encode($arr);
        
        $user->save();

        return response()->json([
            'success' => true,
            'msg' => 'Thêm thành công'
        ]);
    }
}
