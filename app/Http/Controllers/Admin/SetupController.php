<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Option;
use App\Http\Requests\SetupPost;
class SetupController extends Controller
{
    protected function getSetup($key = null)
    {
        return Option::where('opt_key', $key)->first();
    }
    public function index(){
        $title = $this->getSetup('title');
        $logo = $this->getSetup('logo');
        $address = $this->getSetup('address');
        $phone = $this->getSetup('phone');
        $about_img = $this->getSetup('about-img');
        $about_content = $this->getSetup('about-content');
        $link_facebook = $this->getSetup('link-facebook');
        $link_youtube = $this->getSetup('link-youtube');
        $banner_title = $this->getSetup('banner-title');
        $banner_content = $this->getSetup('banner-content');
        $banner_img = $this->getSetup('banner-img');
        $amount = $this->getSetup('amount');
        $text_home = $this->getSetup('text_home');
        return view('server.setup.index',[
            'title' => $title,
            'address' => $address,
            'logo' => $logo,
            'phone' => $phone,
            'about_img' => $about_img,
            'about_content' => $about_content,
            'link_facebook' => $link_facebook,
            'link_youtube' => $link_youtube,
            'amount' => $amount,
            'text_home' => $text_home
        ]);
    }
    public function PostIndex(Request $req){
        $req->validate([
           'title' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:255',
            'about_content' => 'required',
            'about_img' => 'required|max:255',
            'logo' => 'required|max:255',
            'amount' => 'required|numeric'
        ],[
            'required' => 'Không để trống :attribute',
            'max' => ':attribute Không vượt quá 255 ký tự',
            'numeric' => 'Không đúng định dạng số'
        ],[
            'title' => 'Tiêu đề trang',
            'address' => 'Địa chỉ'
        ]);

        Option::where('opt_key', 'title')->update(['opt_value' => $req->title]);
        Option::where('opt_key', 'logo')->update(['opt_value' => $req->logo]);
        Option::where('opt_key', 'address')->update(['opt_value' => $req->address]);
        Option::where('opt_key', 'phone')->update(['opt_value' => $req->phone]);
        Option::where('opt_key', 'about-content')->update(['opt_value' => $req->about_content]);
        Option::where('opt_key', 'about-img')->update(['opt_value' => $req->about_img]);
        Option::where('opt_key', 'amount')->update(['opt_value' => $req->amount]);
        Option::where('opt_key', 'text_home')->update(['opt_value' => $req->text_home]);

        return redirect()->back()->with('success', 'Cập nhật thành công');
    }

    public function banner(){
        $banner_title = $this->getSetup('banner-title');
        $banner_content = $this->getSetup('banner-content');
        $banner_img = $this->getSetup('banner-img');

        return view('server.setup.banner',[
            'banner_title' => $banner_title,
            'banner_content' => $banner_content,
            'banner_img' => $banner_img,
        ]);
    }

    public function Postbanner(Request $req){
        $req->validate([
           'banner_title' => 'required|max:255',
           'banner_content' => 'required',
           'banner_img' => 'required|max:255',
        ],[
            'required' => 'Không được để trống :attribute',
            'max' => ':attribute Không vượt quá 255 ký tự'
        ],[
            'banner_title' => 'Tiêu đề Banner',
            'banner_content' => 'Nội dung Banner',
            'banner_img' => 'Ảnh Banner'
        ]);
        Option::where('opt_key', 'banner-title')->update(['opt_value' => $req->banner_title]);
        Option::where('opt_key', 'banner-content')->update(['opt_value' => $req->banner_content]);
        Option::where('opt_key', 'banner-img')->update(['opt_value' => $req->banner_img]);
        return redirect()->back()->with('success', 'Cập nhật thành công');
    }

    public function post(){
        $new_title = $this->getSetup('new_title');
        $new_content = $this->getSetup('new_content');
        $new_image = $this->getSetup('new_image');

        return view('server.setup.postnew',[
           'new_title' =>$new_title,
           'new_content' =>$new_content,
           'new_image' =>$new_image,
        ]);
    }

    public function Postpost(Request $req){

        $req->validate([
           'new_title' => 'required|max:255',
           'new_content' => 'required',
           'new_image' => 'required|max:255',
        ],[
            'required' => 'Không được để trống :attribute',
            'max' => ':attribute Không vượt quá 255 ký tự'
        ],[
            'new_title' => 'Tiêu đề Tin Mới',
            'new_content' =>'Nội dung Tin Mới',
            'new_image' => 'Ảnh Tin Mới'
        ]);
        Option::where('opt_key', 'new_title')->update(['opt_value' => $req->new_title]);
        Option::where('opt_key', 'new_content')->update(['opt_value' => $req->new_content]);
        Option::where('opt_key', 'new_image')->update(['opt_value' => $req->new_image]);

        return redirect()->back()->with('success', 'Cập nhật thành công');

    }
}
