<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use App\Page;
use App\Helper;

class PageController extends Controller
{
    public function index(){
        $lists = Page::orderBy('id', 'desc')->get();
    	return view('server.page.index', ['listpage'=> $lists]);
    }

    public function create(){
    	return view('server.page.create');
    }

    public function Postcreate(Request $req){
    	$vali = Validator::make($req->all(), [
    		'title' => 'required|max:255',
    		'content' => 'required',
    		'seo_desc' => 'max:255'
    	],[
    		'title.required' => 'Không được để trống tiêu đề',
    		'title.max' => 'Tiêu đề không vượt quá 255 ký tự',
    		'content.required' => 'Không được để trống nội dung',
    		'seo_desc.max' => 'Mô tả seo không vượt quá 255 ký tự'
    	]);
    	if ($vali->fails()) {
    		return response()->json([
    			'errors'=> true,
    			'msg' => $vali->errors()
    		]);
    	}
		$seoimg = $req->seo_image;
    	Page::create([
    		'name' => $req->input('title'),
    		'content' => $req->input('content'),
    		'status' => $req->input('status'),
    		'seo_title' => ($req->seo_title) ? $req->seo_title : $req->title,
    		'seo_desc' => ($req->seo_desc) ? $req->seo_desc : strip_tags(str_limit($req->content, $limit = 100, $end =".")),
    		'seo_image' => $seoimg
    	]);
    	return response()->json([
    		'errors'=> false,
    		'msg' => 'Thêm trang thành công'
    	]);
    }

    public function PostDelete(Request $req){
        $vali = Validator::make($req->all(), [
            'id' => 'required|numeric'
        ],[
            'id.required' => 'Không để trống id trang',
            'id.numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $page = Page::where('id', $req->id)->delete();
        if (empty($page)) {
            $data = [
                'errors' => true,
                'msg' => 'Id trang không tồn tại'
            ];
            return response()->json($data);
        }
        $data = [
            'errors' => false,
            'msg' => 'Xóa trang thành công'
        ];
        return response()->json($data);
    }

    public function update($id = null){
        $page =  Page::where('id', $id)->first();
        if (empty($page)) {
            return redirect()->back();
        }
        return view('server.page.update', ['page'=>$page]);
    }

    public function postUpdate(Request $req, $id = null){
        $vali = Validator::make($req->all(), [
            'title' => 'required|max:255',
            'content' => 'required',
            'seo_title' => 'max:255',
            'seo_desc' => 'max:255',
            'slug' => 'required|max:255',
        ],[
            'title.required' => 'Không được để trống tiêu đề',
            'title.max' => 'Tiêu đề không vượt quá 255 ký tự',
            'content.required' => 'Không được để trống nội dung',
            'seo_title.required' => 'Không được để trống tiêu đề seo',
            'seo_title.max' => 'Tiêu đề seo không vượt quá 255 ký tự',
            'seo_image.required' => 'Không được để trống hình ảnh seo',
            'seo_desc.required' => 'Không được để trống mô tả seo',
            'seo_desc.max' => 'Mô tả seo không vượt quá 255 ký tự',
            'slug.required' => 'Không được để trống đường dẫn',
            'slug.max' => 'đường dẫn không vượt quá 255 ký tự',

        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $page = Page::where('id', $id)->first();
        if (empty($page)) {
            return response()->json([
                'errors'=> true,
                'msg' => 'Không tồn tại trang'
            ]);
        }
        $page->name = $req->title;
        $page->content = $req->content;
        $page->seo_title = ($req->seo_title) ? $req->seo_title : $req->title;
        $page->seo_image = $req->seo_image;
        $page->seo_desc = ($req->seo_desc) ? $req->seo_desc : strip_tags(str_limit($req->content, $limit = 100, $end ="."));
        $page->status = $req->status;
        $page->slug = Helper::create_slug($req->slug);
        if ($page->save()) {
            return response()->json([
                'errors'=> false,
                'msg' => 'Cập nhật thành công'
            ]);
        }
    }
}
