<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product_categories;
use App\Helper;
use Validator;
class ProCategorieController extends Controller
{
    public function index(){
    	$parent = Product_categories::all();
    	return view('server.pro_category.index', ['parents' => $parent]);
    }

    public function data()
    {
    	$parents = Product_categories::orderBy('id', 'desc')->get();
    	$result = [];
    	$id = 0;
    	foreach ($parents as $k => $parent) {
    		if ($parent->status) {
    			$id += 1;
    			$temp = '{
	    			"id" : "'.$id.'",
	    			"name" : "'.$parent->name.'",
	    			"slug" : "'.$parent->slug.'",
	    			"seo_desc" : "'.$parent->seo_desc.'",
                    "parent" : "'.$parent->parent().'",
	    			"idcate":"'.$parent->id.'"
	    		}';
    			$result[] = json_decode($temp);
    		}
    	}
    	return response()->json(['data' => $result]);
    }


    public function postCreate(Request $req){
    	$vali = Validator::make($req->all(), [
    		'name'=> 'required|max:255',
    		'description' => 'max:255',
    		'parent_id' => 'required|numeric',
    	],[
    		'name.required'=> 'Không được để trống tên danh mục',
    		'name.max'=>'Tên danhm mục không vượt quá 255 ký tự',
    		'parent_id.required' =>'Không được để trống chuyên mục',
    		'parent_id.numeric'=>'Chuyên mục giá trị phải là number',
    	]);

    	if ($vali->fails()) {
    		return response()->json([
    			'errors'=> true,
    			'msg'=> $vali->errors()
    		]);
    	}
    	$data = [
    		'name' => $req->input('name'),
    		'description' => ($req->description) ? $req->input('description') : $req->input('name'),
    		'parent_id' => $req->input('parent_id'),
    		'seo_title' => $req->input('name'),
    		'seo_image' => ($req->seo_image) ? $req->input('seo_image') : null,
    		'seo_desc' => ($req->description) ? $req->input('description') : $req->input('name'),
    		'status'=> 1
    	];
    	Product_categories::create($data);
    	return response()->json([
    		'errors' => false,
    		'msg' => 'Thêm danh mục thành công'
    	]);
    }

    public function postDelete(Request $req){
    	$vali = Validator::make($req->all(), [
    		'id'=> 'required|numeric'
    	],[
    		'id.required' => 'Không được để trống id',
    		'id.numeric' => 'Id Không đúng định dạng'
    	]);

    	if ($vali->fails()) {
    		return response()->json([
    			'errors'=> true,
    			'msg' => $vali->errors()
    		]);
    	}
    	$cate = Product_categories::where('id', $req->id)->first();
    	if (empty($cate)) {
    		return response()->json([
    			'errors'=> true,
    			'msg' => 'Không có id danh mục này'
    		]);
    	}
    	$cate->status =0;
    	$cate->save();
    	return response()->json([
			'errors'=> false,
			'msg' => 'Xóa danh mục thành công'
		]);
    }

    public function update($id = null){
    	$cate = Product_categories::where('id', $id)->first(); 
        if (empty($cate)) {
            return "";
        }
        $html = '<div class="form-group">
                <label>Tên danh mục</label>
                <input name="id" value="'.$cate->id.'" hidden="">
                <input type="text" name="nameup" id="nameup" value="'.$cate->name.'" class="form-control">
                <span id="errupname" class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Đường dẫn tĩnh</label>
                <input type="text" name="slugup" value="'.$cate->slug.'" class="form-control">
                <span id="errupslug" class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Mô tả</label>
                <input type="text" name="descup" value="'.$cate->description.'" class="form-control">
                <span id="errupdesc" class="help-block"></span>
            </div>
            <div class="form-group">
                <label for="seo_image">Ảnh đại diện</label>
                 <div class="input-group">
                   <span class="input-group-btn">
                     <a id="lffm" data-input="seo_images" data-preview="holders" class="btn btn-primary">
                       <i class="fa fa-picture-o"></i> Chọn
                     </a>
                   </span>
                   <input id="seo_images" value="'.$cate->seo_image.'" name="seo_images" class="form-control" type="text">
                    <span id="errupimg" class="help-block"></span>
                 </div>
                <span id="errimg" class="help-block"></span>
                <img id="holders" src="'.$cate->seo_image.'" style="margin-top:15px;max-height:100px;">
            </div>';
        return $html;
    }

    public function postUpdate(Request $req){
    	$vali = Validator::make($req->all(),[
            'id' => 'required|numeric',
            'nameup'=> 'required|max:255',
            'slugup' => 'required',
            'descup' => 'required',
            'seo_images' => 'required'
        ],[
            'id.required' => 'Không được để trống id',
            'id.numeric' => 'Không đúng định id',
            'nameup.required' => 'Không được để trống tiêu đề',
            'slugup.required' => 'Không được để trống đường dẫn tĩnh',
            'descup.required' => 'Không được để trống mô tả ',
            'seo_images.required' => 'Không được để trống ảnh đại diện',

        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }

        $cate = Product_categories::where('id', $req->id)->first();
        if (empty($cate)) {
            return response()->json([
                'errors'=> true,
                'msg' => 'Id Không tồn tại'
            ]);
        }

        $cate->name = $req->input('nameup');
        $cate->seo_title = $req->input('nameup');
        $cate->slug = Helper::create_slug($req->slugup);
        $cate->description = $req->input('descup');
        $cate->seo_desc = $req->input('descup');
        $cate->seo_image = $req->input('seo_images');
        $cate->save();
        return response()->json([
            'errors' => false,
            'msg'=> 'Cập nhật thành công'
        ]);
    }
}
