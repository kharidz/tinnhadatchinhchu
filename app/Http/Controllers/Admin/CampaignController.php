<?php

namespace App\Http\Controllers\Admin;

use App\Campaign;
use App\Send_Campaign;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Subscribe;
use DB;
class CampaignController extends Controller
{
    public function index(){
        return view('server.campaign.index');
    }
    public function dataCampaign(Request $req){
        $columns = [
            'id', 'name', 'subject', 'content', 'created_at'
        ];
        $limit = $req->input('length');
        $start = $req->input('start');
        $order = $columns[$req->input('order.0.column')];
        $dir = $req->input('order.0.dir');

        $totalData = $totalFiltered = Campaign::count();

        if (!empty($req->input('search.value'))) {
            $campaigns = Campaign::offset($start)->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $req->input('search.value');
            $campaigns = Campaign::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('subject', 'LIKE', "%{$search}%")
                ->orWhere('content', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        }

        $json_data = array(
            "draw"            => intval($req->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $campaigns
        );
        echo json_encode($json_data);
    }

    public function deleteCamp($id = null){
        $campaign = Campaign::find($id);
        if (empty($campaign)){
            return response([
                'errors' => true,
                'msg' =>'id không tồn tại trên hệ thống'
            ]);
        }
        Send_Campaign::where('id_campaign', $campaign->id)->delete();
        $campaign->delete();
        return response([
            'errors' => false,
            'msg' => 'Xóa chiến dịch thành công'
        ]);
    }

    public function create(){
        $users = DB::table('users')
            ->select('email');
        $subscribes = DB::table('subscribes')
            ->select('email');
        $emails = $subscribes;
        $subscribes = $subscribes->count();
        $emails = $emails->union($users)->get();
        return view('server.campaign.create', compact('emails','users', 'subscribes'));
    }
    public function data(Request $req){
        $limit = ($req->input('length')) ? $req->input('length') : 10;
        $start = ($req->input('start')) ? $req->input('start') * 10 : 1;
        $first = DB::table('users')
            ->select('email');
        $users = DB::table('subscribes')
            ->select('email')
            ->union($first)->offset($start)->limit($limit)->orderBy('email', 'desc')->get();
        return json_decode($users->toJson());
    }

    public function postCreate(Request $req){
        $vali = Validator::make($req->all(),[
            'name' => 'required|max:255',
            'subject' => 'required|max:255',
            'type' => 'required|numeric',
            'email.*' => 'email|max:40',
            'content' => 'required'
        ],[
            'required' => ':attribute không được để trống',
            'max' => ':attribute không vượt quá 255 ký tự',
            'numeric' => ':attribute không đúng định dạng số',
            'email' => ':attribute không đúng định dạng email'
        ]);
        if ($vali->fails()){
            return response([
                'errors' => true,
                'msg' => $vali->errors()
            ]);
        }
        $campaign = Campaign::create([
           'name' => $req->input('name'),
           'subject' => $req->input('subject'),
           'type' => $req->input('type'),
            'content' => $req->input('content')
        ]);
        if ($req->input('type') == 4){
            foreach($req->input('email') as $email){
                Send_Campaign::create([
                   'email' => $email,
                   'id_campaign' => $campaign->id
                ]);
            }
        }
        return response([
            'errors' => false,
            'msg' => 'Thêm chiến dịch thành công'
        ]);
    }
}
