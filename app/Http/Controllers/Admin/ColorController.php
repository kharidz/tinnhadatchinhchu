<?php

namespace App\Http\Controllers\Admin;

use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    public function index(){
        $bgcolor = Option::where('opt_key', 'bgbanner')->first();
        $customcss = Option::where('opt_key', 'customcss')->first();
        return view('server.setup.color', compact('bgcolor', 'customcss'));
    }

    public function store(Request $req){
        $bg = Option::where('opt_key', 'bgbanner')->first();
        $cuscss = Option::where('opt_key', 'customcss')->first();
        $bg->opt_value = empty($req->bgbanner) ? "" : $req->bgbanner;
        $bg->save();
        $cuscss->opt_value = empty($req->customcss) ? "" : $req->customcss;
        $cuscss->save();
        return redirect()->back()->with('succesi', 'Cập nhật thành công');
    }
}
