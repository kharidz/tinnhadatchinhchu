<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Banner;
class BannerController extends Controller
{
    public function banner(){
        return view('server.setup.banner');
    }
    public function DataBanner(){
        $banners = Banner::orderby('id', 'desc')->get();
//        dd($banners);
        $result = [];
        foreach ($banners as $k => $banner){
            $k += 1;
            $content = str_replace(array("\n","\r", "\t"),"",strip_tags($banner->content));
            $tmp = '{
                "id" : "'.$k.'",
                "title": "'.$banner->title.'",
                "content": "'.$content.'",
                "images" : "'.$banner->images.'",
                "idbanner" : "'.$banner->id.'"
            }';
            $tmp = json_decode($tmp);
//            $tmp = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tmp), true );
            if ($tmp != null){
                $result[] = $tmp;
            }
        }
        return ($result) ? $result : null;
    }

    public function idBanner($id = null){
        $banner = Banner::where('id',$id)->first();
        if (empty($banner)){
            return 0;
        }
        $html = '
        <div class="form-group">
            <label for="ed-title">Tiêu đề</label>
            <input type="number" value="'.$banner->id.'" name="ed_id" id="ed-id" hidden="">
            <input type="text" value="'.$banner->title.'" name="ed_title" id="ed-title" class="form-control">
            <span id="errtitle" class="help-block"></span>
        </div>
          <div class="form-group">
                <label for="seo_image">Ảnh đại diện</label>
                 <div class="input-group">
                   <span class="input-group-btn">
                     <a id="lffm" data-input="ed_image" data-preview="holders" class="btn btn-primary">
                       <i class="fa fa-picture-o"></i> Chọn
                     </a>
                   </span>
                   <input id="ed_image" value="'.$banner->images.'" name="ed_image" class="form-control" type="text">
                    <span id="errupimg" class="help-block"></span>
                 </div>
                <img id="holders" src="'.$banner->images.'" style="margin-top:15px;max-height:100px;">
            </div>
          <div class="form-group">
              <label for="ed-content">Nội dung</label>
              <textarea name="ed_content" class="form-control" id="ed_content" cols="20" rows="5">'.$banner->content.'</textarea>
              <span id="errcont" class="help-block"></span>
          </div>
        ';
        return $html;
    }

    public function UpdateBanner(Request $req){
        $vali = Validator::make($req->all(), [
            'ed_id' => 'required|numeric',
            'ed_title' => 'required|max:255',
            'ed_content' => 'required',
            'ed_image' => 'required|max:255'
        ],[
            'required' => 'Không được để trống :attribute',
            'max' => ':attribute Không vượt quá 255 ký tự',
            'numeric' => ':attribute Không đúng định dạng id'
        ],[
            'ed_title' => 'Tiêu đề Banner',
            'ed_content' => 'Nội dung Banner',
            'ed_image' => 'Ảnh Banner'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }

        $banner = Banner::where('id', $req->id)->first();
        if (empty($banner)){
            return response()->json([
                'errors'=> true,
                'msg' => 'Id Không tồn tại'
            ]);
        }

        $banner->title = $req->ed_title;
        $banner->content = $req->ed_content;
        $banner->images = $req->ed_image;
        $banner->save();
        return response()->json([
            'errors' => false,
            'msg'=> 'Cập nhật thành công'
        ]);
    }
    public function Postbanner(Request $req){
        $vali = Validator::make($req->all(),[
            'title' => 'required|max:255',
            'images' => 'required|max:255',
            'content' => 'required'
        ],[
            'required' => 'Không được để trống :attribute',
            'max' => ':attribute Không vượt quá 255 ký tự'
        ],[
            'title' => 'Tiêu đề Banner',
            'content' => 'Nội dung Banner',
            'images' => 'Ảnh Banner'
        ]);
        if ($vali->fails()){
            return response()->json([
                'errors' => true,
                'msg' => $vali->errors()
            ]);
        }
        Banner::create([
            'title' => $req->title,
            'images' => $req->images,
            'content' => $req->content
        ]);
        return response()->json([
           'errors'=> false,
           'msg' => 'Thêm Banner Thành Công'
        ]);
    }

    public function PostDelete($id = null){
        $banner = Banner::where('id', $id)->delete();
        if (empty($banner)){
            return response()->json([
                'errors' => true,
                'msg' => 'Khong ton tai id'
            ]);
        }
        return response()->json([
            'errors'=> false,
            'msg' => 'Xóa danh mục thành công'
        ]);
    }
}
