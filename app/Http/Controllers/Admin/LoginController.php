<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Auth;

class LoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:admin')->except('logout');
	}
    public function login()
    {
    	return view('server.login');
    }

    public function postLogin(Request $req)
    {
    	$req->validate([
    		'email' => 'required|email',
    		'password' => 'required|min:6'
    	],[
    		'email.required' => 'Không được để rỗng email',
    		'email.email' => 'Email không đúng định dạng',
    		'password.required' => 'Không được để rỗng mật khẩu',
    		'password.min' => 'Mật khẩu phải trên 6 ký tự'
    	]);
    	if (Auth::guard('admin')->attempt(['email' => $req->email, 'password' => $req->password], $req->remember)) {
    		return redirect()->route('admin.index');
    	}else{
    		return redirect()->back()->withInput()->withErrors(['errlogin'=> 'Sai thông tin tài khoản']);
    	}
    }
}
