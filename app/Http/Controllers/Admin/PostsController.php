<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Categorie;
use App\Categorie_Post;
use Validator;
use App\Helper;

class PostsController extends Controller
{
    public function index(){
    	$posts = Post::orderBy('id', 'desc')->get();
    	// foreach ($posts as $value) {
    	// 	dd($value->getcate);	
    	// }
    	return view('server.post.index', ['posts'=>$posts]);
    }

    public function create(){
    	$cate = Categorie::all();
    	return view('server.post.create', ['categories'=> $cate]);
    }

    public function PostCreate(Request $req){
    	$vali = Validator::make($req->all(), [
    		'name' => 'required|max:255',
    		'content' => 'required',
    		'seo_desc' => 'max:255',
    	], [
    		'name.required' => 'Không được để trống tiêu đề',
    		'name.max' => 'Tiêu đề không vượt quá 255 ký tự',
    		'content.required' => 'Không để trống nội dung bài đăng',
    		'seo_desc.max' => 'Mô tả seo không vượt quá 255 ký tự',
    	]);

    	if ($vali->fails()) {
    		return response()->json([
    			'error'=> true,
    			'msg' => $vali->errors()
    		]);
    	}
    	$data = [
			'name' => $req->name,
			'excerpt' => ($req->excerpt) ? $req->excerpt : Helper::desctoseo($req->input('content'), 255),
			'content' => $req->content,
			'seo_title' => ($req->seo_title) ? $req->seo_title : $req->name,
			'seo_desc' => ($req->seo_desc) ? $req->seo_desc : Helper::desctoseo($req->input('content'), 255),
			'seo_images' => $req->seo_image,
		];
		$post = Post::create($data);
        if (isset($req->catepost)) {
            foreach ($req->catepost as $k => $value) {
                Categorie_Post::create([
                    'post_id' => $post->id,
                    'cate_id' => $value
                ]);
            }
        }
    	return response()->json([
    		'error' => false,
    		'msg' => 'Thêm bài đăng thành công'
    	]);
    }

    public function postDelete(Request $req){
    	$vali = Validator::make($req->all(), [
            'id' => 'required|numeric'
        ],[
            'id.required' => 'Không để trống id trang',
            'id.numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $post = Post::where('id', $req->id)->delete();
        if (empty($post)) {
        	return response()->json([
        		'errors'=> true,
        		'msg' => 'Id bài viết không tồn tại'
        	]);
        }
        Categorie_Post::where('post_id', $req->id)->delete();
        return response()->json([
        	'errors' => false,
        	'msg' => 'Xóa bài viết thành công'
        ]);
    }

    public function update($id = null){
    	$post = Post::where('id', $id)->first();
    	if (empty($post)) {
    		return redirect()->route('admin.posts.index');
    	}
    	$cate = Categorie::all();
    	$catepost = Categorie_Post::where('post_id', $id)->get();
    	return view('server.post.update',['categories'=> $cate,'post'=>$post, 'catepost'=>$catepost]);
    }


    public function postUpdate(Request $req, $id = null){
    	$post = Post::where('id', $id)->first();
    	if (empty($post)) {
    		return redirect()->back();
    	}
    	$vali = Validator::make($req->all(), [
    		'name' => 'required|max:255',
    		'content' => 'required',
    		'excerpt' => 'required',
    		'seo_desc' => 'required|max:255',
    		'slug' => 'required|max:255'
    	],[
    		'name.required' => 'Không để trống tiêu đề',
    		'name.max' => 'Tiêu đề không vượt quá 255 ký tự',
    		'content.required' => 'Không để trống nội dung',
    		'excerpt.required' => 'Không để trống mô tả ngắn',
    		'seo_desc.required' => 'Không để trống mô tả seo',
    		'seo_desc.max' => 'Mô tả seo không vượt quá 255 ký tự',
			'slug.required' => 'Không để trống đường dẫn tĩnh',
    		'slug.max' => 'Đường dẫn tĩnh không vượt quá 255 ký tự',
    	]);
    	if ($vali->fails()) {
    		return response()->json([
    			'errors'=> true,
    			'msg'=> $vali->errors()
    		]);
    	}
    	$catep = Categorie_Post::where('post_id', $id)->delete();
        if (!empty($req->catepost)) {  
        	foreach ($req->catepost as $cate) {
        		Categorie_Post::create([
        			'post_id'=> $id,
        			'cate_id' => $cate
        		]);
        	}
        }
    	$post->name = $req->name;
    	$post->excerpt = $req->excerpt;
    	$post->content = $req->content;
    	$post->seo_title = ($req->seo_title) ? $req->seo_title : $req->name;
    	$post->seo_desc = ($req->seo_desc) ? $req->seo_desc : Helper::desctoseo($req->input('content'), 255) ;
    	$post->seo_images = ($req->seo_image) ? $req->seo_image : null;
    	$post->slug = Helper::create_slug($req->slug);
    	if ($post->save()) {
    		return response()->json([
    			'errors'=> false,
    			'msg' =>'Cập nhật bài viết thành công'
    		]);
    	}
    }
}
