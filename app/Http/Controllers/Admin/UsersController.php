<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use Validator;
use App\Mail\AccountAdmin;
use App\Mail\AccountCustom;

class UsersController extends Controller
{
    public function custom()
    {
        $users = User::orderBy('id', 'desc')->get();
        return view('server.users.custom', ['users'=>$users]);
        //return view('server.users.custom');
    }

    public function DataCustom()
    {
        $users = User::orderBy('id', 'desc')->get();
        $result = [];
        $id = 0;
        foreach ($users as $k => $user) {
            $id += 1;
            $temp = '{
                "id" : "'.$id.'",
                "username" : "'.$user->username.'",
                "phone" : "'.$user->phone.'",
                "email" : "'.$user->email.'",
                "fullname" : "'.$user->fullname.'",
                "status":"'.$user->status.'",
                "amount":"'.$user->amount.'",
                "iduser": "'.$user->id.'"
            }';
            $result[] = json_decode($temp);
        }
        return response()->json([
            'data' => $result,
            'listUsers'=>$users,
        ]);
    }

    public function postDelete(Request $req)
    {
        $vali = Validator::make($req->all(), [
            'id' => 'required|numeric'
        ], [
            'id.required'=> 'Không để trống id users',
            'id.numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=>true,
                'msg' => $vali->errors()
            ]);
        }
        $user = User::where('id', $req->input('id'))->delete();
        if (empty($user)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Id tài khoản không tồn tại trên hệ thống'
            ]);
        }
        return response()->json([
            'errors'=>false,
            'msg' => 'Xóa Tài Khoản Thành Công'
        ]);
    }

    public function admins()
    {
        return view('server.users.admins');
    }

    public function adminData()
    {
        $users = Admin::orderBy('id', 'desc')->get();
        $result = [];
        $id = 0;
        foreach ($users as $k => $user) {
            $id += 1;
            $temp = '{
                "id" : "'.$id.'",
                "username" : "'.$user->username.'",
                "phone" : "'.$user->phone.'",
                "email" : "'.$user->email.'",
                "fullname" : "'.$user->fullname.'",
                "status":"'.$user->role.'",
                "iduser": "'.$user->id.'"
            }';
            $result[] = json_decode($temp);
        }
        return response()->json([
            'data' => $result
        ]);
    }

    public function postAdmin(Request $req)
    {
        $vali = Validator::make($req->all(), [
            'id' => 'required|numeric'
        ], [
            'id.required'=> 'Không để trống id users',
            'id.numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=>true,
                'msg' => $vali->errors()
            ]);
        }
        $user = Admin::where('id', $req->input('id'))->delete();
        if (empty($user)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Id tài khoản không tồn tại trên hệ thống'
            ]);
        }
        return response()->json([
            'errors'=>false,
            'msg' => 'Xóa Tài Khoản Thành Công'
        ]);
    }


    public function CreateAdmin(Request $req)
    {
        $vali = Validator::make($req->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'phone' => 'required|max:13|unique:admins'
        ], [
            'name.required' => 'Không được để trống tên',
            'name.string' => 'Không đúng định dạng họ tên',
            'name.max' => 'Không vượt quá 255 ký tự',
            'email.required' => 'Không được để trống email',
            'email.email' => 'Không đúng định dạng email',
            'email.max' => 'Không vượt quá 255 ký tự',
            'phone.required' => "Không để trống số điện thoại",
            'phone.max' => "Không đúng định dạng số điện thoại",
            'phone.unique' => "Số điện thoại đã tồn tại trên hệ thống",
            'email.unique' => "Email tồn tại trên hệ thống",
        ]);

        if ($vali->fails()) {
            return response()->json([
                'errors' => true,
                'msg' => $vali->errors()
            ]);
        }
        $password = bin2hex(random_bytes(5));
        $user =  Admin::create([
            'username' => $req->email,
            'phone' => $req->phone,
            'email' => $req->email,
            'fullname' => $req->name,
            'password' => bcrypt($password),
            'role' => 1,
            'token_hash' => sha1(time())
        ]);
        \Mail::to($user->email)->send(new AccountAdmin($user, $password));
        return response()->json([
                'errors' => false,
                'msg' => 'Tạo tài khoản thành công'
            ]);
    }


    public function CreateCutom(Request $req)
    {
        $vali = Validator::make($req->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:13|unique:users'
        ], [
            'name.required' => 'Không được để trống tên',
            'name.string' => 'Không đúng định dạng họ tên',
            'name.max' => 'Không vượt quá 255 ký tự',
            'email.required' => 'Không được để trống email',
            'email.email' => 'Không đúng định dạng email',
            'email.max' => 'Không vượt quá 255 ký tự',
            'phone.required' => "Không để trống số điện thoại",
            'phone.max' => "Không đúng định dạng số điện thoại",
            'phone.unique' => "Số điện thoại đã tồn tại trên hệ thống",
            'email.unique' => "Email tồn tại trên hệ thống",
        ]);

        if ($vali->fails()) {
            return response()->json([
                'errors' => true,
                'msg' => $vali->errors()
            ]);
        }
        $password = bin2hex(random_bytes(5));
        $user =  User::create([
            'username' => $req->email,
            'phone' => $req->phone,
            'email' => $req->email,
            'fullname' => $req->name,
            'password' => bcrypt($password),
            'status' => 1,
            'hash_code' => sha1(time())
        ]);
        \Mail::to($user->email)->send(new AccountCustom($user, $password));
        return response()->json([
                'errors' => false,
                'msg' => 'Tạo tài khoản thành công'
            ]);
    }
}
