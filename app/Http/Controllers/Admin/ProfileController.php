<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Auth;
class ProfileController extends Controller
{	

    public function index(){
    	$id = \Auth::guard('admin')->user()->id;
    	$user = Admin::find($id);
    	if (empty($user)) {
    		auth()->logout();
    		return redirect()->route('login');
    	}
    	return view('server.profile.index', ['user'=> $user]);
    }


    public function postUpdate(Request $req){
    	$req->validate([
    		'fullname' => 'required|max:255',
    		'phone' => 'required|numeric',
    	],[
            'fullname.required' => 'Không để trống họ tên',
            'phone.required' => 'Không để trống số điện thoại',
            'phone.numeric' => 'Không đúng định dạng sdt',
            'fullname.max' => 'Họ tên không vượt quá 255 ký tự',
    	]);
        $id = \Auth::guard('admin')->user()->id;
        $user = Admin::find($id);
        if (empty($user)) {
            return redirect()->back()->with('errupdate', 'Không tồn tại tài khoản trên');
        }
        $user->fullname = $req->fullname;
        $user->phone = $req->phone;
        $user->save();
        return redirect()->back()->with('success', 'Cập nhật thành công');
    }

    public function postPassword(Request $req){
        $req->validate([
            'oldpassword'=> 'required|min:8',
            'newpassword'=>'required|min:8|confirmed'
        ],[
            'oldpassword.required' => 'Không được để trống mật khẩu cũ',
            'newpassword.required' => 'Không được để trống mật khẩu mới',
            'oldpassword.min' => 'Mật khẩu lớn hơn 6 ký tự',
            'newpassword.min' => 'Mật khẩu lớn hơn 6 ký tự',
            'newpassword.confirmed' => 'Mật khẩu mới không khớp',
        ]);
        $id = \Auth::guard('admin')->user()->id;
        $user = Admin::find($id);
        if (empty($user)) {
            return redirect()->route('admin.profile.index');
        }
        if (\Hash::check($req->oldpassword, $user->password)) {
            $user->password = bcrypt($req->newpassword);
            $user->save();
            return redirect()->back()->with('succpass', 'Đổi mật khẩu thành công');
        }else{
            return redirect()->back()->with('errpass', 'Mật khẩu cũ không đúng');
        }
    }
}
