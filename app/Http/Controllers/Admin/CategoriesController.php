<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categorie;
use Validator;
use App\Helper;

class CategoriesController extends Controller
{
    public function index()
    {
    	$categories = Categorie::orderBy('id', 'desc')->get();
    	return view('server.categories.index', ['categories' => $categories]);
    }

    public function create(){
    	return view('server.categories.create', ['categories'=> Categorie::all()]);
    }

    public function Postcreate(Request $req){
    	$vali = Validator::make($req->all(),[
    		'name' => 'required|max:255',
    		'description' => 'required',
    	],[
    		'name.required' => 'Không được để trống tên danh mục',
    		'name.max' => 'Tên danh mục không vượt quá 255 ký tự',
    		'description.required' => 'Không được để trống tên danh mục',
    	]);

    	if ($vali->fails()) {
    		return response()->json([
    			'error'=> true,
    			'msg' => $vali->errors()
    		]);
    	}
    	Categorie::create([
    		'name' => $req->input('name'),
    		'parent_id' => $req->input('parent_id'),
    		'description' => $req->input('description'),
    		'seo_title' => ($req->seo_title) ? $req->seo_title : $req->input('name'),
    		'seo_desc' => ($req->seo_desc) ? $req->seo_desc : Helper::desctoseo($req->input('description'), 255),
    		'seo_image' => ($req->input('seo_image')) ? $req->input('seo_image') : null,
    		'status' => 2,
    	]);
    	return response()->json([
    		'error'=> false,
    		'msg' => 'Thêm danh mục bài viết thành công'
    	]);
    }


    public function update($id = null)
    {
        $listcate = Categorie::all();
        $categorie = Categorie::where('id',$id)->first();
        if (empty($categorie)) {
            return redirect()->back();
        }
    	return view('server.categories.update', ['categories'=> $listcate, 'cate'=>$categorie]);
    }

    public function postUpdate(Request $req, $id = null)
    {
    	$vali = Validator::make($req->all(),[
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'description' => 'required',
            'seo_image' => 'required',
            'seo_title' => 'required',
            'seo_desc' => 'required',
        ],[
            'name.required' => 'Không được để trống Đường dẫn',
            'name.max' => 'Đường dẫn không vượt quá 255 ký tự',
            'slug.required' => 'Không được để trống tên danh mục',
            'slug.max' => 'Tên danh mục không vượt quá 255 ký tự',
            'description.required' => 'Không được để trống tên danh mục',
            'seo_image.required' => 'Không được để trống tên danh mục',
        ]);

        if ($vali->fails()) {
            return response()->json([
                'error'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $cate = Categorie::where('id', $id)->first();
        if (empty($cate)) {
            return response()->json([
                'errors'=> true,
                'msg' => 'Không tồn tại trang'
            ]);
        }
        if ($cate->id == $req->parent_id) {
            return response()->json([
                'errors'=> true,
                'msg' => 'Nó không thế là bố nó!'
            ]);
        }
        $cate->name = $req->name;
        $cate->seo_title = $req->seo_title;
        $cate->slug = Helper::create_slug($req->slug);
        $cate->description = $req->description;
        $cate->parent_id = $req->parent_id;
        $cate->seo_image = $req->seo_image;
        $cate->seo_desc = $req->seo_desc;
        $cate->save();
        return response()->json([
           'errors' => false,
           'msg' => 'Cập nhật thành công'
        ]);
    }

    public function PostDelete(Request $req){
    	$vali = Validator::make($req->all(), [
            'id' => 'required|numeric'
        ],[
            'id.required' => 'Không để trống id trang',
            'id.numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $categorie = Categorie::where('id', $req->id)->first();
        if (empty($categorie)) {
            $data = [
                'errors' => true,
                'msg' => 'Id trang không tồn tại'
            ];
            return response()->json($data);
        }
        $categorie->status = 0;
        $categorie->save();
        $data = [
            'errors' => false,
            'msg' => 'Xóa trang thành công'
        ];
        return response()->json($data);
    }
}
