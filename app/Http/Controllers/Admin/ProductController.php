<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use App\District;
use App\Ward;
use App\Product;
use App\Product_categories;
use App\Product_cate_product;
use Validator;
use Yajra\Datatables\Datatables;
class ProductController extends Controller
{
    public function index()
    {
        return view('server.products.index');
    }
    public function data(Request $req){
        $columns = [
            'id', 'name', 'address', 'price', 'contact_phone', 'area', 'cate', 'slug', 'created_at'
        ];
        $limit = $req->input('length');
        $start = $req->input('start');
        $order = $columns[$req->input('order.0.column')];
        $dir = $req->input('order.0.dir');

        $totalData = $totalFiltered = Product::count();
        if (!empty($req->input('search.value'))) {
            $products = Product::offset($start)->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $req->input('search.value');
            $products = Product::where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('address', 'LIKE', "%{$search}%")
                ->orWhere('price', 'LIKE', "%{$search}%")
                ->orWhere('contact_phone', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        }
        $data = [];
        foreach ($products as $k => $product) {
            $data[] = [
                'id' => $k,
                'name' => $product->name,
                'address' => $product->address,
                'area' => $product->area,
                'price' => $product->price,
                'cate' => $product->getcate(),
                'contact_phone' => $product->contact_phone,
                'created_at' => $product->created_at,
            ];
        }
        $json_data = array(
            "draw"            => intval($req->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
    public function create(){
        $cate = Product_categories::all();
        if (empty($cate)) {
            return redirect()->route('admin.pro_category.index');
        }
        $province = Province::all();
        $districts = District::where('province_code', '01')->get();
        $ward = Ward::where('district_code', '001')->get();
        return view('server.products.create',
            ['provinces' => $province,
                'districts' => $districts,
                'wards'=>$ward,
                'cates' => $cate
            ]);
    }

    public function postCreate(Request $req){
        $vali = Validator::make($req->all(),[
            'name' => 'required|max:255',
            'area' => 'required|numeric',
            'price' => 'required|numeric',
            'contact_phone' => 'required|max:255',
            'content' => 'required',
            'district_id' => 'required|numeric',
            'province_id' => 'required|numeric',
            'ward_id' => 'required',
        ],[
            'name.required' => 'Không được để trống tiêu đề',
            'area.required' => 'Không được để trống diện tích',
            'price.required' => 'Không được để trống giá',
            'contact_phone.required' => 'Không được để trống số điện thoại',
            'content.required' => 'Không được để trống nội dung',
            'district_id.required' => 'Không được để trống tỉnh thành',
            'province_id.required' => 'Không được để trống quận huyện',
            'ward_id.required' => 'Không được để trống phường xã',
            'name.max' => 'Tiêu đề không vượt quá 255',
            'district_id.numeric' =>'Không đúng định dạng số',
            'province_id.numeric' =>'Không đúng định dạng số',
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors' => true,
                'msg' => $vali->errors()
            ]);
        }
        $data = [
            'name' =>$req->input('name'),
            'content' => $req->input('content'),
            'province_id' => $req->input('province_id'),
            'district_id' => $req->input('district_id'),
            'content' => $req->input('content'),
            'address' => trim($req->input('ward_id')),
            'price' => $req->input('price'),
            'area' => $req->input('area'),
            'contact_phone' => trim($req->input('contact_phone')),
            'status' => 1,
        ];
        $product = Product::create($data);
        if (!empty($req->input('cate_id'))) {
            foreach ($req->input('cate_id') as $cate) {
                Product_cate_product::create([
                    'product_cat_id' => $cate,
                    'product_id' => $product->id
                ]);
            }
        }
        return response()->json([
            'errors' => false,
            'msg' => 'Thêm sản phẩm thành công'
        ]);

    }
    public function update($id = null){
        $product = Product::where('id', $id)->first();
        if (empty($product)) {
            return redirect()->route('admin.index');
        }
        $cate = Product_categories::all();
        if (empty($cate)) {
            return redirect()->route('admin.pro_category.index');
        }
        $province = Province::all();
        $districts = District::where('province_code', $product->province_id)->get();
        $ward = Ward::where('district_code', $product->district_id)->get();
        return view('server.products.update',
            ['provinces' => $province,
                'districts' => $districts,
                'wards'=>$ward,
                'cates' => $cate,
                'product' => $product
            ]);
    }

    public function postUpdate(Request $req, $id = null){
        $product = Product::where('id', $id)->first();
        if (empty($product)) {
            return response()->json([
                'errors'=> true,
                'msg'=> 'Id sản phẩm không tồn tại'
            ]);
        }
        $vali = Validator::make($req->all(),[
            'name' => 'required|max:255',
            'area' => 'required|numeric',
            'price' => 'required|numeric',
            'contact_phone' => 'required|max:255',
            'content' => 'required',
            'district_id' => 'required|numeric',
            'province_id' => 'required|numeric',
        ],[
            'name.required' => 'Không được để trống tiêu đề',
            'area.required' => 'Không được để trống diện tích',
            'price.required' => 'Không được để trống giá',
            'contact_phone.required' => 'Không được để trống số điện thoại',
            'content.required' => 'Không được để trống nội dung',
            'district_id.required' => 'Không được để trống tỉnh thành',
            'province_id.required' => 'Không được để trống quận huyện',
            'name.max' => 'Tiêu đề không vượt quá 255',
            'district_id.numeric' =>'Không đúng định dạng số',
            'province_id.numeric' =>'Không đúng định dạng số',
        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' =>$vali->errors()
            ]);
        }
        $product->name = $req->input('name');
        $product->content = $req->input('content');
        $product->province_id = $req->input('province_id');
        $product->district_id = $req->input('district_id');
        $product->price = $req->input('price');
        $product->area = $req->input('area');
        $product->contact_phone = $req->input('contact_phone');
        if ($req->input('ward_id')) {
            $product->address = $req->input('ward_id');
        }
        $product->save();
        if ($req->input('cate_id')) {
            Product_cate_product::where('product_id', $product->id)->delete();
            foreach ($req->input('cate_id') as $value) {
                Product_cate_product::create([
                    'product_id' => $product->id,
                    'product_cat_id' => $value
                ]);
            }
        }
        return response()->json([
            'errors' =>false,
            'msg' => 'Cập nhật sản phẩm thành công'
        ]);
    }
    public function postDelete(Request $req)
    {
        $vali = Validator::make($req->all(),[
            'id' => 'required|numeric'
        ],[
            'id.required' => 'Không để trống id',
            'id.numeric' => 'Không đúng định dạng id',
        ]);

        if($vali->fails()){
            return response()->json([
                'errors'=> true,
                'msg' => 'Id để trống hoặc không đúng ký tự'
            ]);
        }
        $pro = Product::where('id', $req->id)->first();
        if (empty($pro)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Id sản phẩm không tồn tại'
            ]);
        }
        $pro->status = 0;
        $pro->save();
        return response()->json([
            'errors' => false,
            'msg' => 'Xóa Sản phẩm thành công'
        ]);
    }
}
