<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\User_Payment;

class PaymentController extends Controller
{
	public function history(){

		$payment = User_Payment::orderBy('id', 'desc')->get();
		// dd($payment);
		return view('server.payment.history', ['historys'=> $payment]);
	}

    public function postMoney(Request $req){
    	$vali = Validator($req->all(), [
    		'iduser' => 'required|numeric',
    		'contentpay' => 'max:255',
    		'amount' => 'required|numeric'
    	],[
    		'iduser.required' =>'Không được để trống Id Tài khoản',
    		'id.numeric' => 'Không đúng định dạng id',
    		'contentpay.max' => 'Không vượt quá 255 ký tự',
    		'amount.required' => 'Không được để trống số tiền',
    		'amount.numeric' => 'không đúng định dạng số'
    	]);
    	if ($vali->fails()) {
    		return response()->json([
    			'errors' => true,
    			'msg' => $vali->errors()
    		]);
    	}

    	$user = User::where('id', $req->iduser)->first();
    	if (empty($user)) {
    		return response()->json([
    			'errors' => true,
    			'msg' => 'User Không tồn tại trên hệ thống'
    		]);
    	}
    	$user->amount += $req->amount;
    	$user->save();
    	$content = ($req->contentpay) ? $req->contentpay : "Thêm $req->amount Vào tài khoản $user->name";
    	User_Payment::create([
    		'user_id' => $user->id,
    		'amount' => $req->amount,
    		'content' => $content
    	]);
    	return response()->json([
    		'errors' => false,
    		'msg' => "Thêm ".number_format($req->amount)." VNĐ vào $user->email Thành công"
    	]);
    }
}
