<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
class PageController extends Controller
{
    public function detail($slug = null){
    	$page = Page::where('slug', $slug)->first();
    	if (empty($page)) {
    		return redirect()->route('client.home');
    	}
    	return view('client.page', ['page'=> $page]);
    }
}
