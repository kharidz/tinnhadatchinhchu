<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use App\Product;
use App\District;
use App\Province;
use App\Product_categories;
use App\User_Products;
use App\User;
use App\View_products;
use Validator;
use Carbon\Carbon;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public function index(Request $req)
    {
        $id = \Auth::user()->id;
        $wheres = [];
        $cate = ($req->input('cate')) ? $req->input('cate') : null;
        $cate_id = Product_categories::where('id', $cate)->first();
        $type_gia = 0;
        if (isset($cate)) {
            if ($cate == 1 || $cate_id->parent_id == 1) {
                $type_gia = 0;
            } elseif ($cate == 2 || $cate_id->parent_id == 2) {
                $type_gia = 1;
            }
        }
        $city_id = User::where('id', $id)->first();
        $city = ($city_id->province_id)? $city_id->province_id : null;
        if (!$city) {
            return redirect()->route('client.profile.RegisterCity');
        }
//        $wheres[] = ['province_id', '=', $city];
        if (!empty($req->input('district'))) {
            $wheres[] = ['district_id', '=', $req->input('district')];
        }
        if (!empty($req->input('phone'))) {
            $phone = $req->input('phone');
            $wheres[] = ['contact_phone', 'like', "%$phone%"];
        }
        if (!empty($req->input('price'))) {
            switch ($req->input('price')) {
                case 1:
                    $wheres[] = ['price', '<=', ($type_gia) ? 1000000 : 10000000];
                    break;
                case 2:
                    $wheres[] = ['price', '>=', ($type_gia) ? 1000000 : 10000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 3000000 : 100000000];
                    break;
                case 3:
                    $wheres[] = ['price', '>=', ($type_gia) ? 3000000 : 100000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 5000000 : 1000000000];
                    break;
                case 4:
                    $wheres[] = ['price', '>=', ($type_gia) ? 5000000 : 1000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 10000000 : 3000000000];
                    break;
                case 5:
                    $wheres[] = ['price', '>=', ($type_gia) ? 10000000 : 3000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 40000000 : 7000000000];
                    break;
                case 6:
                    $wheres[] = ['price', '>=', ($type_gia) ? 40000000 : 7000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 70000000 : 10000000000];
                    break;
                case 7:
                    $wheres[] = ['price', '>=', ($type_gia) ? 70000000 : 10000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 100000000 : 20000000000];
                    break;
                case 8:
                    $wheres[] = ['price', '>=', ($type_gia) ? 100000000 : 20000000000];
                    break;
                default:
                    $wheres[] = ['price', '>=', 0];
                    break;
            }
        }
        $whedate = [];
        if ($req->D1) {
            $whedate['to'] = new Carbon($req->D1);
            $whedate['form'] = ($req->D2) ? new Carbon($req->D2) : new Carbon(time());
        }
        $title = ($req->input('title')) ? $req->input('title'): null;
        $products = Product::whereIn('id', function ($q) use ($wheres, $cate, $whedate, $title, $city) {
            $q->select('product_id')->from('product_cat_product');
            if (isset($cate)) {
                $q->where('product_cat_id', $cate);
            }
                $q->where($wheres)->where(function($qq) use ($city){
                    $qq->where('province_id',$city);
                });
            if (!empty($whedate)) {
                $q->whereBetween('created_at', [$whedate['to']->format('Y-m-d')." 00:00:00", $whedate['form']->format('Y-m-d')." 23:59:59"]);
            }
            if ($title) {
                $q->where('name', 'like', "%$title%");
            }
        })->orderBy('created_at', 'desc')->paginate(20);
        $tmp_view = View_products::where('user_id', $id)->get();
        $views = [];
        foreach ($tmp_view as $view) {
            $views[] = $view->pro_id;
        }
        $tmp_book = User_Products::where('user_id', $id)->get();
        $books = [];
        foreach ($tmp_book as $book) {
            $books[] = $book->pro_id;
        }
        $products->withPath($req->fullUrl());
        $cate = Product_categories::all();
        $provinces = Province::where('code', $city)->first();
        $districts = District::where('province_code', $city)->get();
        $user_id = \Auth::user()->id;
        $agency = array();
        if (!empty($user_id)) {
            $user = User::find($user_id);
            if (!empty($user['agency'])) :
                $agency =  json_decode($user['agency'], true);
            endif;
        }

        $near_city = [
            1 => [
                30,31,22,33
            ],
            79 => [
                83,72,92
            ],
            2 => [
                33,30,31
            ]
        ];
        if (array_key_exists($city, $near_city)){
            $near_city = $near_city[$city];
        }else{
            $near_city = $near_city[2];
        }
        $citys = Province::where('code', $city);
        foreach($near_city as $city){
            $citys->orWhere('code', $city);
        }
        $near_city = $citys->get();
        return view('client.product.index', [
            'agency' => $agency,
            'products' => $products, 'districts' => $districts, 'cates'=>$cate , 'views' => $views,
            'type_gia' => $type_gia,
            'books'=> $books,
            'provinces' => $provinces->name,
            'near_city' => $near_city
        ]);
    }


    public function contentMore($slug = null)
    {
        $id = \Auth::user()->id;
        $product = Product::where('slug', $slug)->first();
        if (empty($product)) {
            return "";
        }
        $view = View_products::where('user_id', $id)->where('pro_id', $product->id)->first();
        if (empty($view)) {
            View_products::create([
                'user_id' => \Auth::user()->id,
                'pro_id' => $product->id
            ]);
        };
        $data = $product->content;
        $data .= $product->getprovince();
        /*
        $data .= "<table>
            <tbody>
                <tr>
                    <th>Địa chỉ</th>
                    <th>Quận / Huyện</th>
                    <th>Tỉnh/ Thành</th>
                </tr>
                <tr>
                    <td>".$product->address."</td>
                    <td>".$product->getdistrict()."</td>
                    <td>".$product->getprovince()."</td>
                </tr>
            </tbody>
        </table>";
        */
        return $data;
    }

    public function SaveItem($slug = null)
    {
        $id = \Auth::user()->id;
        $product = Product::where('slug', $slug)->first();
        if (empty($product)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Không tồi tại trên hệ thống'
            ]);
        }
        $user_pro = User_Products::where('user_id', $id)->where('pro_id', $product->id)->first();
        // dd($user_pro);
        if (!empty($user_pro)) {
            return response()->json([
                'errors' => true,
                'msg' => 'Sản phẩm này đã lưu trước đó'
            ]);
        }
        User_Products::create([
            'user_id' => $id,
            'pro_id' => $product->id
        ]);
        return response()->json([
            'errors' => false,
            'msg' => 'Lưu tin '.$product->name.' thành công'
        ]);
    }

    public function Export(Request $req)
    {
        $id = \Auth::user()->id;
        $wheres = [];
        $cate = ($req->input('cate')) ? $req->input('cate') : null;
        $cate_id = Product_categories::where('id', $cate)->first();
        $type_gia = 0;
        if (isset($cate)) {
            if ($cate == 1 || $cate_id->parent_id == 1) {
                $type_gia = 0;
            } elseif ($cate == 2 || $cate_id->parent_id == 2) {
                $type_gia = 1;
            }
        }
        $city_id = User::where('id', $id)->first();
        $amount = Option::where('opt_key', 'amount')->first();
        if ($city_id->amount < $amount->opt_value) {
            return redirect()->route('client.products.index');
        }
        $city = ($city_id->province_id)? $city_id->province_id : null;
        if (!$city) {
            return redirect()->route('client.profile.RegisterCity');
        }
        $wheres[] = ['province_id', '=', $city];
        if (!empty($req->input('district'))) {
            $wheres[] = ['district_id', '=', $req->input('district')];
        }
        if (!empty($req->input('phone'))) {
            $phone = $req->input('phone');
            $wheres[] = ['contact_phone', 'like', "%$phone%"];
        }
        if (!empty($req->input('price'))) {
            switch ($req->input('price')) {
                case 1:
                    $wheres[] = ['price', '<=', ($type_gia) ? 1000000 : 10000000];
                    break;
                case 2:
                    $wheres[] = ['price', '>=', ($type_gia) ? 1000000 : 10000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 3000000 : 100000000];
                    break;
                case 3:
                    $wheres[] = ['price', '>=', ($type_gia) ? 3000000 : 100000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 5000000 : 1000000000];
                    break;
                case 4:
                    $wheres[] = ['price', '>=', ($type_gia) ? 5000000 : 1000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 10000000 : 3000000000];
                    break;
                case 5:
                    $wheres[] = ['price', '>=', ($type_gia) ? 10000000 : 3000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 40000000 : 7000000000];
                    break;
                case 6:
                    $wheres[] = ['price', '>=', ($type_gia) ? 40000000 : 7000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 70000000 : 10000000000];
                    break;
                case 7:
                    $wheres[] = ['price', '>=', ($type_gia) ? 70000000 : 10000000000];
                    $wheres[] = ['price', '<=', ($type_gia) ? 100000000 : 20000000000];
                    break;
                case 8:
                    $wheres[] = ['price', '>=', ($type_gia) ? 100000000 : 20000000000];
                    break;
                default:
                    $wheres[] = ['price', '>=', 0];
                    break;
            }
        }
        $whedate = [];
        if ($req->D1) {
            $whedate['to'] = new Carbon($req->D1);
            $whedate['form'] = ($req->D2) ? new Carbon($req->D2) : new Carbon(time());
        }
        $title = ($req->input('title')) ? $req->input('title'): null;

        return Excel::download(new ProductExport($wheres, $cate, $whedate, $title), "Product-".time().".xlsx");
    }
}
