<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Option;
use App\History_Payment;
class PaymentController extends Controller
{
    public function subtract(){
    	$tien = Option::where('opt_key', 'amount')->first();
        $tien = $tien->opt_value;
    	$history = History_Payment::orderBy('id', 'desc')->first();
        $new = date('Y-m-d',time());
    	if (!empty($history)){
            $old = date('Y-m-d', strtotime($history->created_at));
            if($old == $new){
                return "Hôm nay đã thực hiện công việc chừ tiền vào lúc: $history->created_at";
            }
        }
    	$users = User::where('amount', '>=', $tien)->decrement('amount', $tien);
    	History_Payment::create([
    		'content' => 'Thực hiện công việc ngày'.$new,
    	]);
    	return "done";
    }

}
