<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Subscribe;
class SubscribeController extends Controller
{
    public function postSub(Request $req){
        $req->validate([
            'email' => 'required|email|max:40|unique:subscribes',
        ],[
            'required' => ':attribute không được để trống',
            'email' => ':attribute không đúng định dang email',
            'max' => ':attribute không vượt quá 40 ký tự',
            'unique' => ':attribute đã tồn tại trên hệ thống',
        ]);
        $user = new Subscribe();
        $user->email = $req->input('email');
        if (!empty($user->save())){
            return redirect()->back()->with('success', 'Đăng ký nhận tin thàng công');
        }
    }
}
