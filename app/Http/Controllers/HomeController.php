<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        return view('client.welcome');
    }

    public function gioithieu(){
        return view('client.gioithieu');
    }

    public function banggia(){
        return view('client.banggia');
    }
    public function lienhe(){
        return view('client.lienhe');
    }
    
}
