<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Categorie;
use App\Categorie_Post;
class PostController extends Controller
{
    public function index(){
    	$posts = Post::orderBy('id', 'desc')->paginate(8);
        $newpost = Post::orderBy('id', 'desc')->take(3)->get();
        $getcate = Categorie_Post::where('post_id', 1)->first();
        if (empty($getcate)) {
            $related = $newpost;
        }else{
            $related = Post::whereHas('cate', function($q) use ($getcate){
                $q->where('cate_id', $getcate->cate_id);
            })->get();
        }
    	return view('client.post', compact('posts', 'newpost', 'related'));
    }

    public function detail($slug = null){
    	$post = Post::where('slug', $slug)->first();
    	if (empty($post)) {
    		return redirect()->route('client.home');
    	}
        $newpost = Post::orderBy('id', 'desc')->take(3)->get();
        $getcate = Categorie_Post::where('post_id', $post->id)->first();
        if (empty($getcate)) {
            $related = $newpost;
        }else{
            $related = Post::whereHas('cate', function($q) use ($getcate){
                $q->where('cate_id', $getcate->cate_id);
            })->get();
        }
        // dd($related);
    	return view('client.postdetail', ['post'=>$post, 'newpost' => $newpost, 'related' => $related]);
    }

    public function category($slug = null){
        
    }
}
