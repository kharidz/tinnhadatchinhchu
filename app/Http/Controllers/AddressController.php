<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\District;
use App\Ward;
use Validator;

class AddressController extends Controller
{
    public function district(Request $req){
    	$district = District::where('province_code', $req->input('province_id'))->get();
    	return $district;
    }

    public function ward(Request $req){
    	$distr = (int)$req->input('district_id');
    	$ward = Ward::where('district_code', $distr)->get();
    	return $ward;
    }
}
