<?php

namespace App\Http\Controllers;

use App\Province;
use App\User_Products;
use Illuminate\Http\Request;
use App\User;
use App\User_Payment;
use App\Product;
use Validator;
use Excel;
use App\Exports\ExcelBookMark;
class ProfileController extends Controller
{
    public function index(){
    	$id = \Auth::user()->id;
    	// dd($id);
    	$user = User::find($id);
    	if (empty($user)) {
    		return redirect()->route('client.home');
    	}
    	return view('client.profile.index', ['user'=> $user]);
    }
    public function postUpdate(Request $req){
    	$req->validate([
    		'fullname' => 'required|max:255',
    		'phone' => 'required|numeric'
    	],[
    		'fullname.required' => 'Không để trống Họ tên ',
    		'phone.required' => 'Không để trống số điện thoại',

    	]);
    	$id = \Auth::user()->id;
    	// dd($id);
    	$user = User::find($id);
    	$user->fullname = $req->fullname;
    	$user->phone = $req->input('phone');
    	$user->save();
    	return redirect()->back()->with('success', 'Cập Nhật thành công');
    }


    public function postPassword(Request $req){
    	$req->validate([
    		'oldpassword' => 'required',
    		'newpassword' => 'required|min:8|confirmed',
    	],[
    		'oldpassword.required' => 'Không để trống mật khẩu cũ',
    		'newpassword.required' => 'Không để trống mật khẩu mới',
    		'newpassword.min' => 'Độ dài mật khẩu lớn hơn 8',
    		'newpassword.confirmed' => 'Mật khẩu mới không khớp'
    	]);
    	$id = \Auth::user()->id;
    	$user = User::find($id);
    	if (\Hash::check($req->oldpassword, $user->password)) {
    		$user->password = bcrypt($req->newpassword);
    		$user->save();
    		return redirect()->back()->with('passnoti', 'Cập nhật thành công');
    	}
    	return redirect()->back()->with('passnoti', 'Mật khẩu cũ không đúng');
    }
    public function Pay_History(){
    	$id = \Auth::user()->id;
    	$user = User::find($id);
    	if (empty($user)) {
    		return redirect()->route('client.home');
    	}
    	$payments = User_Payment::where('user_id', \Auth::user()->id)->get();
    	return view('client.profile.payhistory',['user'=>$user, 'payments'=>$payments]);
    }


    public function bookmarks(){
        $id = \Auth::user()->id;
        $user = User::find($id);
        if (empty($user)) {
            return redirect()->route('client.home');
        }
        $product = Product::whereHas('get_user', function($q) use($id){
            $q->where('user_id', $id);
        })->orderBy('id', 'desc')->paginate(10);
//         dd($product);
        return view('client.profile.bookmarks', ['user' => $user, 'products' => $product]);
    }

    public function Deletebookmarks(Request $req){
        $vali = Validator::make($req->all(),[
           'id' => 'required|numeric'
        ],[
            'required' => 'Không được để trống',
            'numeric' => 'Không đúng định dạng id'
        ]);
        if ($vali->fails()){
            return response()->json([
                'errors' => true,
                'msg' =>  $vali->errors()
            ]);
        }
        $save = User_Products::where('user_id', \Auth::user()->id)->where('pro_id', $req->id)->delete();
        if (!empty($save)){
            return response()->json([
               'errors' => false,
               'msg' =>  'Xóa thành công'
            ]);
        }
        return response()->json([
            'errors' => true,
            'msg' =>  "Không tồn tại bài viết"
        ]);
    }

    public function Export(){
        return Excel::download(new ExcelBookMark(), 'Product-'.time().".xlsx");
    }

    public function RegisterCity(){
        $id = \Auth::user()->id;
        $user = User::find($id);
        if (empty($user) || $user->province_id) {
            return redirect()->route('client.home');
        }
        $province = Province::all();
        return view('client.profile.upcity', ['citys' => $province]);
    }

    public function PostRegisterCity(Request $req){
        $req->validate([
            'City' => 'required|numeric'
        ],[
            'City.required' => 'Không được để trống',
            'City.numeric' => 'Không đúng định dạng'
        ]);
        $id = \Auth::user()->id;
        $user = User::find($id);
        if (empty($user) || $user->province_id) {
            return redirect()->route('client.home');
        }
        $user->province_id = $req->City;
        $user->save();
        return redirect()->route('client.products.index');
    }
}
