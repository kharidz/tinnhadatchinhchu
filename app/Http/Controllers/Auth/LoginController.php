<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        if (! $user->status) {
            auth()->logout();
            return back()->with('errlogin', 'Lam On Xac Nhan Tai Khoan');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function loginPost(Request $req){
        $req->validate([
            'LoginEmail' => 'required|email',
            'LoginPassword' => 'required|min:6'
        ], [
            'LoginEmail.required'=> 'Hãy điền địa chỉ hộp thư điện tử!',
            'LoginEmail.email' => 'Sai khuôn dạng địa chỉ hộp thư điện tử!',
            'LoginPassword.required' => 'Hãy điền mật khẩu!'
        ]);

        if (Auth::attempt(['email' => $req->input('LoginEmail'), 'password' => $req->input('LoginPassword')])) {
            $req->session()->regenerate();
            $previous_session = Auth::User()->session_id;
            if ($previous_session) {
                \Session::getHandler()->destroy($previous_session);
            }
            Auth::user()->session_id = \Session::getId();
            Auth::user()->save();
            $this->clearLoginAttempts($req);
            return $this->authenticated($req, auth()->user());
        }else{
            return redirect()->route('login')->with('errlogin', 'Thông Tin Tài Khoản Của Bạn Không Đúng');
        }

    }
}
