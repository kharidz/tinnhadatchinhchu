<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\SendActive;
use App\SmsModel;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'string', 'max:255', 'unique:users'],
            'LoginPassword' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
//    protected function create(array $data)
//    {
//        return User::create([
//            'fullname' => $data['name'],
//            'email' => $data['email'],
//            'phone' => $data['phone'],
//            'password' => Hash::make($data['LoginPassword']),
//        ]);
//    }

    public function PostRegist(Request $req){
        $vali = Validator::make($req->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
            'LoginPassword' => ['required', 'string', 'min:8'],
        ],[
            'name.required' => 'Không được để trống họ tên',
            'name.max'=>'Không vượt quá 255',
            'email.required' => 'Không được để trống email',
            'email.max' => 'Không vượt quá 255',
            'email.email' => 'Không đúng định dạng',
            'email.unique' => 'Email Đã tồn tại trên hệ thống',
            'phone.required' => 'Không được để trống số điện thoại',
            'phone.max' => 'Không vượt quá 255',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'LoginPassword.required' => 'Không được để trống mật khẩu',

        ]);
        if ($vali->fails()) {
            return response()->json([
                'errors'=> true,
                'msg' => $vali->errors()
            ]);
        }
        $user = User::create([
            'username' => $req->input('email'),
            'phone' => $req->input('phone'),
            'email' => $req->input('email'),
            'fullname' => $req->input('name'),
            'password' => Hash::make($req->input('LoginPassword')),
            'status' => 0,
            'hash_code' => sha1(time())
        ]);
        \Mail::to($user->email)->send(new SendActive($user));
        $text = "Vui lòng truy cập hòm thư để kích hoạt tài khoản ".env('APP_URL');
        SmsModel::to($user->phone)->send($text);
        return response()->json([
            'errors' => false,
            'msg'=> 'Đăng ký thành công! Một thư kích hoạt tài khoản đã được gửi đến Email đăng ký của bạn. Vui lòng kiểm tra email và kích hoạt tài khoản.',
        ]);
    }

    public function active($code = null){
        $user = User::where('hash_code', $code)->first();
        if (empty($user)) {
            return redirect()->route('client.home');
        }
        $user->hash_code = sha1(time());
        $user->status = 1;
        $user->save();
        return redirect()->route('login')->with('success', 'Xác nhận tài khoản thành công');
    }
}
