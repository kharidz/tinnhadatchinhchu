<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Mail\PassForgot;
use App\User;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function Forgot(){
        return view('auth.passwords.email');
    }


    public function PostForgot(Request $req){
        $req->validate([
            'Email' => 'required|email'
        ],[
            'Email.required' => 'Không được để trống mail',
            'Email.email' => 'Không đúng định dạng mail'
        ]);
        $user = User::where('email', $req->input('Email'))->first();
        if (empty($user)) {
            return redirect()->back()->with('errs', 'Email Không tồn tại trên hệ thống');
        }
        \Mail::to($user->email)->send(new PassForgot($user));
        return redirect()->route('login')->with('notiforgot','Mội thư đặt lại mật khẩu đã được gửi tới hòm thư của bạn. Vui lòng truy cập vào hòm thư của bạn để đặt lại mật khẩu.');
    }

    public function ResetPass($token = null){
        $user = User::where('hash_code', $token)->first();
        if (empty($user)) {
            return redirect()->route('password.reset');
        }
        return view('auth.passwords.reset', ['user'=>$user]);
    }

    public function PostReset(Request $req){
        $req->validate([
            'password'=> 'required|min:6|confirmed'
        ],[
            'password.required' => 'Không được để trống mật khẩu',
            'password.min' => 'Mật khẩu ít nhất 6 ký tự',
            'password.confirmed' => 'Mật khẩu không khớp'
        ]);
        $user = User::where('hash_code', $req->input('token'))->first();
        if (empty($user)) {
            return redirect()->back()->with('errtoken', 'Token không tồn tại');
        }
        $user->password = Hash::make($req->input('password'));
        $user->hash_code = sha1(time());
        $user->save();
        return redirect()->route('login')->with('sucreset', 'Đặt lại mật khẩu thành công');
    }
}
