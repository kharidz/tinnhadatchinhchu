<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
	use Sluggable;
	
    protected $table = 'pages';

    protected $fillable = ['name', 'slug', 'content', 'status', 'templates', 'seo_title', 'seo_desc', 'seo_image'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
