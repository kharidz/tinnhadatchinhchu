<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $table = 'ward';

    protected $primaryKey = 'code';

    protected $fillable = ['name', 'type', 'district_id'];

    protected $timestamp = false;
}
