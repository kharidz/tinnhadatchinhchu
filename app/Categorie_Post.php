<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie_Post extends Model
{
    protected $table = 'category_post';

    protected $fillable = ['post_id', 'cate_id'];

    public $timestamps = false;

    public function relatedPost(){
    	return $this->belongsToMany('App\Post', 'pro_id');
    }
}
