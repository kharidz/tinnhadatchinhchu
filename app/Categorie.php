<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Categorie extends Model
{
    use Sluggable;
	protected $table = 'categories';

	protected $fillable = ['name', 'slug', 'parent_id','description', 'seo_title', 'seo_desc','seo_image', 'status'];

	/**
	 * [sluggable description]
	 * @return slug Categories
	 */
	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Categories Parent
     * @return parent categories
     */
    public function childs(){
        return $this->hasMany('App\Categorie', 'parent_id');
    }

    public function parent()
    {
        $parent = $this->belongsTo('App\Categorie', 'parent_id')->first();
        if (empty($parent)) {
            return 'Danh mục cha';
        }
        return $parent->name;
    }

}
