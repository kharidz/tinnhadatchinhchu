<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $passwd;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $passwd)
    {
        $this->user = $user;
        $this->passwd = $passwd;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.accountadmin')->subject('Thông tin tài khoản quản trị của bạn');
    }
}
