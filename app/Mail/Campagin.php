<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Campagin extends Mailable
{
    use Queueable, SerializesModels;
    protected $email, $campaign;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $campaign)
    {
        $this->email = $email;
        $this->campaign = $campaign;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->campaign->subject)->html($this->campaign->content);
    }
}
