<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('client.home');

Route::post('subscribes', 'SubscribeController@postSub')->name('client.subscribes.post');

/**
 * Page View
 */
Route::get('p/{slug?}', 'PageController@detail')->name('client.page');
Auth::routes();

Route::post('registers', 'Auth\RegisterController@PostRegist')->name('auth.registers');

Route::get('tintuc', 'PostController@index')->name('client.home.new');

/**
 * Product of the category
 */
Route::get('category/{slug?}', 'PostController@category')->name('client.category');

/**
 * Create Post Login
 */
Route::post('logins', 'Auth\LoginController@loginPost')->name('logins');

Route::get('logout', function () {
    \Auth::guard('admin')->logout();
    \Auth::logout();
    return redirect()->route('client.home');
})->name('logout');

Route::get('active/{code?}', 'Auth\RegisterController@active')->name('active');
Route::get('password/reset', 'Auth\ForgotPasswordController@Forgot')->name('password.forgot');
Route::post('password/reset', 'Auth\ForgotPasswordController@PostForgot')->name('password.forgot');
Route::get('password/reset/{token}', 'Auth\ForgotPasswordController@ResetPass')->name('password.forgot.reset');
Route::post('password/resets', 'Auth\ForgotPasswordController@PostReset')->name('password.forgot.resets');


Route::group(['prefix' => 'member', 'middleware'=> ['auth']], function () {
    Route::get('', 'ProfileController@index')->name('client.profile.index');
    Route::post('', 'ProfileController@postUpdate')->name('client.profile.update');
    Route::post('password', 'ProfileController@postPassword')->name('client.profile.password');

    Route::get('pay-history', 'ProfileController@Pay_History')->name('client.profile.payhistory');

    Route::get('bookmarks', 'ProfileController@bookmarks')->name('client.profile.bookmarks');
    Route::post('bookmarks', 'ProfileController@Deletebookmarks')->name('client.profile.bookmarks');

    Route::get('bookmarks/export', 'ProfileController@Export')->name('client.profile.bookmarks.export');
    Route::get('RegisterCity', 'ProfileController@RegisterCity')->name('client.profile.RegisterCity');
    Route::post('RegisterCity', 'ProfileController@PostRegisterCity')->name('client.profile.RegisterCity');
});

Route::group(['prefix' => 'product', 'middleware'=> ['auth']], function () {
    Route::get('', 'ProductController@index')->name('client.products.index');
    Route::get('export', 'ProductController@Export')->name('client.products.export');
    Route::get('{slug?}', 'ProductController@contentMore')->name('client.products.content');
    Route::get('b/{slug?}', 'ProductController@SaveItem')->name('client.products.save');
});

/**
 * Dashboard Page
 */
Route::get('dashboard/login', 'Admin\LoginController@login')->name('server.login');
Route::post('dashboard/login', 'Admin\LoginController@postLogin')->name('server.login');



Route::group(['prefix' => 'dashboard', 'namespace'=>'Admin', 'middleware'=>'admin'], function () {
    Route::get('/', function () {
        return view('server.index');
    })->name('admin.index');


    /**
     * Setup Controller
     */
    Route::group(['prefix' =>'setup'], function () {
        Route::get('/', 'SetupController@index')->name('admin.setup.index');
        Route::post('/', 'SetupController@PostIndex')->name('admin.setup.index');

        Route::get('/banner', 'BannerController@banner')->name('admin.setup.banner');
        Route::post('/banner', 'BannerController@Postbanner')->name('admin.setup.banner');
        Route::get('/banner/data', 'BannerController@DataBanner')->name('admin.setup.banner.data');
        Route::get('/banners/{id?}', 'BannerController@idBanner')->name('admin.setup.banner.modal');
        Route::post('/banners/{id?}', 'BannerController@UpdateBanner')->name('admin.setup.banner.modal');
        Route::get('/banner-del/{id?}', 'BannerController@PostDelete')->name('admin.setup.banner.delete');

        Route::get('/post', 'SetupController@post')->name('admin.setup.post');
        Route::post('/post', 'SetupController@Postpost')->name('admin.setup.post');

        Route::get('/color', 'ColorController@index')->name('admin.setup.color');
        Route::post('/color', 'ColorController@store')->name('admin.setup.color');
    });
    /**
     * Page Controller
     */
    Route::group(['prefix' => 'page'], function () {
        Route::get('/', 'PageController@index')->name('admin.page.index');
        Route::delete('/', 'PageController@PostDelete')->name('admin.page.index')->where('id', '[0-9]+');

        Route::get('create', 'PageController@create')->name('admin.page.create');
        Route::post('create', 'PageController@Postcreate')->name('admin.page.create');

        Route::get('update/{id?}', 'PageController@update')->name('admin.page.update')->where('id', '[0-9]+');
        Route::post('update/{id?}', 'PageController@postUpdate')->name('admin.page.update')->where('id', '[0-9]+');
    });

    /**
     * Categories Post Controller
     */
    Route::group(['prefix' => 'categories'], function () {
        Route::get('', 'CategoriesController@index')->name('admin.categories.index');
        Route::delete('/', 'CategoriesController@PostDelete')->name('admin.categories.index')->where('id', '[0-9]+');

        Route::get('create', 'CategoriesController@create')->name('admin.categories.create');
        Route::post('create', 'CategoriesController@Postcreate')->name('admin.categories.create');

        Route::get('update/{id?}', 'CategoriesController@update')->name('admin.categories.update')->where('id', '[0-9]+');
        Route::post('update/{id?}', 'CategoriesController@postUpdate')->name('admin.categories.update')->where('id', '[0-9]+');
    });

    /**
     * Posts Controller
     */
    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'PostsController@index')->name('admin.posts.index');
        Route::delete('', 'PostsController@postDelete')->name('admin.posts.index')->where('id', '[0-9]+');

        Route::get('create', 'PostsController@create')->name('admin.post.create');
        Route::post('create', 'PostsController@PostCreate')->name('admin.post.create');

        Route::get('update/{id?}', 'PostsController@update')->name('admin.post.update')->where('id', '[0-9]+');
        Route::post('update/{id?}', 'PostsController@postUpdate')->name('admin.post.update')->where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'pro_category'], function () {
        Route::get('', 'ProCategorieController@index')->name('admin.pro_category.index');
        Route::get('ajax', 'ProCategorieController@data')->name('admin.pro_category.data');
        Route::post('', 'ProCategorieController@postCreate')->name('admin.pro_category.index');
        Route::delete('', 'ProCategorieController@postDelete')->name('admin.pro_category.index');

        Route::get('update/{id?}', 'ProCategorieController@update')->name('admin.pro_category.update')->where('id', '[0-9]+');
        Route::post('update', 'ProCategorieController@postUpdate')->name('admin.pro_category.update');
    });


    Route::group(['prefix' => 'products'], function () {
        Route::get('', 'ProductController@index')->name('admin.product.index');
        Route::get('data', 'ProductController@data')->name('admin.product.data');
        Route::delete('', 'ProductController@postDelete')->name('admin.product.delete');

        Route::get('create', 'ProductController@create')->name('admin.product.create');
        Route::post('create', 'ProductController@postCreate')->name('admin.product.create');

        Route::get('update/{id?}', 'ProductController@update')->name('admin.product.update')->where('id', '[0-9]+');
        Route::post('update/{id?}', 'ProductController@postUpdate')->name('admin.product.update')->where('id', '[0-9]+');
    });


    Route::group(['prefix' => 'users' , 'middleware'=> 'fullrole'], function () {
        Route::get('custom/data', 'UsersController@DataCustom')->name('admin.custom.data');
        Route::get('custom', 'UsersController@custom')->name('admin.custom.index');
        Route::delete('custom', 'UsersController@postDelete')->name('admin.custom.index');

        Route::post('create-custom', 'UsersController@CreateCutom')->name('admin.custom.create');


        Route::get('admins', 'UsersController@admins')->name('admin.admin.index');
        Route::get('admins/data', 'UsersController@adminData')->name('admin.admin.data');
        Route::delete('admins', 'UsersController@postAdmin')->name('admin.admin.index');

        Route::post('create-admin', 'UsersController@CreateAdmin')->name('admin.admin.create');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::post('', 'PaymentController@postMoney')->name('admin.payment.addmoney');

        Route::get('history', 'PaymentController@history')->name('admin.payment.history');
    });

    /**
     * Profile
     */
    
    Route::group(['prefix' => 'profile'], function () {
        Route::get('', 'ProfileController@index')->name('admin.profile.index');
        Route::post('', 'ProfileController@postUpdate')->name('admin.profile.index');
        Route::post('password', 'ProfileController@postPassword')->name('admin.profile.password');
    });

    /*
     * Campaign
     */
    Route::group(['prefix' => 'campaign'], function(){
       Route::get('', 'CampaignController@index')->name('admin.campaign.index');
       Route::get('data_camp', 'CampaignController@dataCampaign')->name('admin.campaign.data_camp');
       Route::get('delete/{id?}', 'CampaignController@deleteCamp')->name('admin.campaign.delete');
       Route::get('data', 'CampaignController@data')->name('admin.campaign.data');
       Route::get('create', 'CampaignController@create')->name('admin.campaign.create');
       Route::post('create', 'CampaignController@postCreate');
    });
});

Route::group(['prefix' => 'addr'], function () {
    Route::post('district', 'AddressController@district')->name('district');
    Route::post('ward', 'AddressController@ward')->name('ward');
});

Route::post('add_agency', 'UsersController@add_agency')->name('user.add_agency');

Route::get('subtract-money', 'PaymentController@subtract');

Route::get('{slug?}', 'PostController@detail')->name('client.posts.detail');

Route::group(['prefix'=> 'crawl'], function () {
    Route::get('batdongsanchinhchu', 'CrawlController@batdongsanchinhchu');
    Route::get('old-batdongsanchinhchu', 'CrawlController@OldBatdongsanchinhchu');
    Route::get('hcm', 'CrawlController@bdschHCM');
});


