install:
	composer install --no-scripts

	@if [ ! -f .env ];\
		then cp .env.example .env;\
		echo "Copied from .env.example";\
		php artisan key:generate;\
	fi

	php artisan storage:link
	php artisan migrate
	php artisan import:db
	php artisan db:seed
	npm install
	npm run production
