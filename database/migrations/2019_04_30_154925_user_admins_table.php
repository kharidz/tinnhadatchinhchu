<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('fullname');
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->date('last_login')->nullable();
            $table->string('token_hash')->nullable();
            $table->integer('role');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
