<?php

use App\Product_categories;
use Illuminate\Database\Seeder;
use App\Helper;
class FakeCatePro extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product_categories::create([
            'name' => 'Bán nhà đất',
            'slug' => 'ban-nha-dat',
            'parent_id' => 0,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê nhà đất',
            'slug' => 'thue-nha-dat',
            'parent_id' => 0,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cần thuê',
            'slug' => Helper::create_slug('Cần thuê'),
            'parent_id' => 0,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cần mua',
            'slug' => Helper::create_slug('Cần mua'),
            'parent_id' => 0,
            'status' => 1
        ]);

        Product_categories::create([
            'name' => 'Bán Nhà Riêng, Trong Ngõ',
            'slug' => Helper::create_slug('Bán Nhà Riêng, Trong Ngõ'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán nhà mặt phố',
            'slug' => Helper::create_slug('Bán nhà mặt phố'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán Căn hộ, Chung cư',
            'slug' => Helper::create_slug('Bán Căn hộ, Chung cư'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán Đất ở, Đất Thổ Cư',
            'slug' => Helper::create_slug('Bán Đất ở, Đất Thổ Cư'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán Đất ở, Đất Thổ Cư',
            'slug' => Helper::create_slug('Bán Đất ở, Đất Thổ Cư'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán đất nền dự án',
            'slug' => Helper::create_slug('Bán đất nền dự án'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán Biệt thự, liền kề, phân lô',
            'slug' => Helper::create_slug('Bán Biệt thự, liền kề, phân lô'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán kho, xưởng',
            'slug' => Helper::create_slug('Bán kho, xưởng'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán văn phòng, mặt bằng kinh doanh',
            'slug' => Helper::create_slug('Bán văn phòng, mặt bằng kinh doanh'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Bán Nhà tập thể',
            'slug' => Helper::create_slug('Bán Nhà tập thể'),
            'parent_id' => 1,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho Thuê Căn hộ, chung cư',
            'slug' => Helper::create_slug('Cho Thuê Căn hộ, chung cư'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho Thuê Nhà Riêng, Trong Ngõ',
            'slug' => Helper::create_slug('Cho Thuê Nhà Riêng, Trong Ngõ'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê nhà mặt phố',
            'slug' => Helper::create_slug('Cho thuê nhà mặt phố'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê phòng trọ',
            'slug' => Helper::create_slug('Cho thuê phòng trọ'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê văn phòng',
            'slug' => Helper::create_slug('Cho thuê văn phòng'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho Thuê Cửa Hàng - Kiot',
            'slug' => Helper::create_slug('Cho Thuê Cửa Hàng - Kiot'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê kho, xưởng',
            'slug' => Helper::create_slug('Cho thuê kho, xưởng'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê BĐS khác',
            'slug' => Helper::create_slug('Cho thuê BĐS khác'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho Thuê Biệt thự, liền kề, phân lô',
            'slug' => Helper::create_slug('Cho Thuê Biệt thự, liền kề, phân lô'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê khách sạn, căn hộ dịch vụ',
            'slug' => Helper::create_slug('Cho thuê khách sạn, căn hộ dịch vụ'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho Thuê Nhà tập thể',
            'slug' => Helper::create_slug('Cho Thuê Nhà tập thể'),
            'parent_id' => 2,
            'status' => 1
        ]);
        Product_categories::create([
            'name' => 'Cho thuê văn phòng, mặt bằng kinh doanh',
            'slug' => Helper::create_slug('Cho thuê văn phòng, mặt bằng kinh doanh'),
            'parent_id' => 2,
            'status' => 1
        ]);

    }
}
