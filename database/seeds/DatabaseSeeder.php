<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(FakeUser::class);
         $this->call(FakeCatePro::class);
//         $this->call(FakeProduct::class);
    }
}
