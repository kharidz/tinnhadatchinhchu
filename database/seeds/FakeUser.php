<?php

use Illuminate\Database\Seeder;

class FakeUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\Admin::create([
        	'username' => 'admin',
        	'password' => bcrypt('admin123'),
        	'phone' => '0123657485',
        	'email' => 'admin@sotaychinhchu.com',
        	'fullname' => 'Administrator',
        	'token_hash' => sha1('admin123'),
            'role' => 9
        ]);

        $member = App\User::create([
            'username' => 'admin',
            'password' => bcrypt('admin123'),
            'phone' => '0123657485',
            'email' => 'admin@sotaychinhchu.com',
            'fullname' => 'Administrator',
            'hash_code' => sha1('admin123'),
            'status' => 1
        ]);
    }
}
