const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/admin/js/app.js', 'public/admin/js')
	.copyDirectory('node_modules/admin-lte/dist/img', 'public/dist/img')
	.copyDirectory([
		'node_modules/admin-lte/bower_components/datatables.net'
		], 'public/admin/plugin/datatables.net')
	.copyDirectory([
		'node_modules/admin-lte/bower_components/datatables.net-bs'
		], 'public/admin/plugin/datatables.net-bs')
	.copyDirectory([
		'node_modules/admin-lte/plugins/bootstrap-wysihtml5'
		], 'public/admin/plugin/bootstrap-wysihtml5')
	.copyDirectory([
		'node_modules/admin-lte/plugins/iCheck'
		], 'public/admin/plugin/iCheck')
	.copyDirectory([
		'node_modules/ckeditor'
		], 'public/admin/plugin/ckeditor')
    .copyDirectory([
        'node_modules/dropify'
    ], 'public/admin/plugin/dropify')
	.copyDirectory([
		'node_modules/dropify'
		], 'public/admin/plugin/dropify')
   .sass('resources/admin/sass/app.scss', 'public/admin/css');
