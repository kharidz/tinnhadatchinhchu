jQuery('.slide-home-bn').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    dots:true,
    animateOut: 'fadeOut',
    smartSpeed:600,
    autoplay:true,
    autoplayTimeout:6000,
    items:1
})

$('.slide-about').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    items:1
})
$('.slide-video').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    autoplay:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    items:1
})

jQuery(document).ready(function() {
    jQuery('.toggle-item .toggle-title h4').click(function() {
        if (jQuery(this).hasClass('active')) {
            jQuery(this).parents('.toggle-item').find('.toggle-content').slideDown();
            jQuery(this).removeClass('active')
        }
        else{
             jQuery(this).parents('.box-toggle').find('.toggle-content').slideUp();
             
             jQuery(this).parents('.toggle-item').find('.toggle-content').slideDown();
             jQuery(this).addClass('active');

        }
    });  
});

jQuery(document).ready(function () {
    jQuery('.menu-box .main-menu>li.menu-item-has-children>a').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
    jQuery('.menu-box .main-menu .sub-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-right"></i>');

    jQuery('.menu-site .btn-show-menu').click(function () {
        jQuery(this).parents('.menu-site').find('.menu-box').css('width','100%');
    });
    jQuery('.menu-box .btn-hide-menu, .menu-box .bg-menu').click(function () {
        jQuery(this).parents('.menu-box ').css('width','0');
    });

    if($(window).width()<992){
        jQuery('.main-menu li.menu-item-has-children>a>i').click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().children('.sub-menu').slideToggle('fast');
        });
    }
});