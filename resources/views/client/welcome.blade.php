<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{($title) ? $title: env('APP_NAME')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<header>
    <div class="container">
        <div class="top-head">
            <div class="topbar-left-content">
                <ul id="top_menu" class="top-menu">
                    @auth
                        <li>
                            <span class="sprite login">&nbsp;</span>
                            <a href="{{ route('logout') }}" title="Thoát">Thoát</a>
                        </li>
                        <li><span class="icon-member">&nbsp;</span> <a href="/member">Trang cá nhân</a></li>
                        <li>
                            <span class="sprite register">&nbsp;</span><a href="javascript:ShowProfile()"
                                                                          title="Thay đổi thông tin cá nhân">{{Auth::user()->fullname}}</a>
                        </li>

                    @else
                        <li><span class="sprite register">&nbsp;</span><a href="{{ route('register') }}"
                                                                          title="Đăng ký tài khoản">Đăng ký</a></li>
                        <li><span class="sprite login">&nbsp;</span><a href="/login" title="Đăng nhập hệ thống">Đăng
                                nhập</a></li>
                    @endauth
                </ul>
            </div>
            <div class="topbar-right-content">
                <ul class="social-icons clearfix">
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li> <a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                    <li class="google"><a href="#" title="Google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>

                    <li class="youtube"><a href="#" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <li class="instagram"><a href="#" title="Instgram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                    <li class="pinterest"> <a href="#" title="Pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <div class="row">
                <div class="col-md-4 col-xs-6 col-sm-6">
                    <div class="logo">
                        <a title="{{$title}}" href="/"><img src="{{ $logo }}" alt="{{$title}}"></a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-6 col-sm-6">
                    <div class="menu-site">
                        <button class="btn btn-show-menu hidden-md hidden-lg"><i class="fa fa-bars"></i></button>
                        <div class="menu-box">
                            <div class="bg-menu hidden-md hidden-lg"></div>
                            <ul class="main-menu">
                                <li><a href="{{URL::to('p/gioi-thieu')}}" title="Giới thiệu">Giới thiệu</a></li>
                                <li><a href="{{URL::to('p/san-pham-dich-vu')}}" title="Tin rao">Sản phẩm - Dịch vụ</a></li>
                                <li><a href="{{URL::to('product')}}" title="Tin rao">Nhà đất chính chủ</a></li>
                                <li><a href="{{URL::to('p/bang-gia')}}" title="Tin rao">Bảng giá</a></li>
                                <li><a href="{{URL::to('tintuc')}}" title="Tin tức">Tin tức</a></li>
                                <li><a href="{{URL::to('p/lien-he')}}" title="Liên hệ">Liên hệ</a></li>
                            </ul>
                            <button class="btn btn-hide-menu hidden-md hidden-lg"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header><!-- /header -->
<div class="home">
    <div class="bn-home">
        <div class="owl-carousel owl-theme slide-home-bn">
            @foreach($banners as $banner)
            <div class="item">
                <img src="{{$banner->images}}" alt="{{$banner->title}}">
            </div>
            @endforeach
        </div>
    </div>
    <div class="content-box">
        <div class="container">
            <div class="title-home text-center">
                <h2>Tin mới nhất</h2>
                <p>{{$new_title}}</p>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <img src="{{$new_img}}" alt="{{$new_title}}">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info">
                        <h3>{{$new_title}}</h3>
                        <p>{!! $new_content !!}</p>
                    </div>
                </div>
            </div>
        </div>
{{--    </div>--}}
{{--    <div class="out-app">--}}
{{--        <div class="container">--}}
{{--            <div class="title-home text-center">--}}
{{--                <h2>Our Apps</h2>--}}
{{--                <p>Check out our fantastic apps below and follow the links to learn more!</p>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                    <div class="pro-item">--}}
{{--                        <div class="appboxtop" style="background-image: url('img/appboxtop01.png')"></div>--}}
{{--                        <div class="appboxmid">--}}
{{--                            <p class="shortdesc">QuiverVision apps deliver truly magical experiences enjoyed by kids, parents, and educators alike. Our premier App is called “Quiver” and is available on iOS, Android and Fire OS.</p>--}}
{{--                            <a href="#">Learn More</a>--}}
{{--                        </div>--}}
{{--                        <div class="appboxbtm">--}}
{{--                            <label>Available in</label>--}}
{{--                            <ul>--}}
{{--                                <li><a class="app01" href="#"></a></li>--}}
{{--                                <li><a class="app02" href="#"></a></li>--}}
{{--                                <li><a class="app03" href="#"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                    <div class="pro-item">--}}
{{--                        <div class="appboxtop" style="background-image: url('img/appboxtop02.png')"></div>--}}
{{--                        <div class="appboxmid">--}}
{{--                            <p class="shortdesc">QuiverVision apps deliver truly magical experiences enjoyed by kids, parents, and educators alike. Our premier App is called “Quiver” and is available on iOS, Android and Fire OS.</p>--}}
{{--                            <a href="#">Learn More</a>--}}
{{--                        </div>--}}
{{--                        <div class="appboxbtm">--}}
{{--                            <label>Available in</label>--}}
{{--                            <ul>--}}
{{--                                <li><a class="app01" href="#"></a></li>--}}
{{--                                <li><a class="app02" href="#"></a></li>--}}
{{--                                <li><a class="app03" href="#"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                    <div class="pro-item">--}}
{{--                        <div class="appboxtop" style="background-image: url('img/appboxtop03.png')"></div>--}}
{{--                        <div class="appboxmid">--}}
{{--                            <p class="shortdesc">QuiverVision apps deliver truly magical experiences enjoyed by kids, parents, and educators alike. Our premier App is called “Quiver” and is available on iOS, Android and Fire OS.</p>--}}
{{--                            <a href="#">Learn More</a>--}}
{{--                        </div>--}}
{{--                        <div class="appboxbtm">--}}
{{--                            <label>Available in</label>--}}
{{--                            <ul>--}}
{{--                                <li><a class="app01" href="#"></a></li>--}}
{{--                                <li><a class="app02" href="#"></a></li>--}}
{{--                                <li><a class="app03" href="#"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                    <div class="pro-item">--}}
{{--                        <div class="appboxtop" style="background-image: url('img/appboxtop04.png')"></div>--}}
{{--                        <div class="appboxmid">--}}
{{--                            <p class="shortdesc">QuiverVision apps deliver truly magical experiences enjoyed by kids, parents, and educators alike. Our premier App is called “Quiver” and is available on iOS, Android and Fire OS.</p>--}}
{{--                            <a href="#">Learn More</a>--}}
{{--                        </div>--}}
{{--                        <div class="appboxbtm">--}}
{{--                            <label>Available in</label>--}}
{{--                            <ul>--}}
{{--                                <li><a class="app01" href="#"></a></li>--}}
{{--                                <li><a class="app02" href="#"></a></li>--}}
{{--                                <li><a class="app03" href="#"></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="features">
        <div class="container">
            <div class="title-home text-center">
                <h2>Packed full of features</h2>
                <p>Simple, effective and easy to use, at the<br> the touch of a button</p>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 col-left">
                    <div class="features-item">
                        <h3>Print directly</h3>
                        <p>Print directly from the Apps or our website!</p>
                    </div>
                    <div class="features-item">
                        <h3>Real-time coloring</h3>
                        <p>Watch the 3D scene being colored in real time, as you color the printed page.</p>
                    </div>
                    <div class="features-item">
                        <h3>Proximity features</h3>
                        <p>Change your 3D experience by moving your device closer to the page.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <img src="img/appprev.jpg" alt="">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 col-right">
                    <div class="features-item">
                        <h3>Page association</h3>
                        <p>Watch as colored elements from one page magically make an appearance on a second page.</p>
                    </div>
                    <div class="features-item">
                        <h3>Simple Games</h3>
                        <p>Play a digital game using your colored creations!</p>
                    </div>
                    <div class="features-item">
                        <h3>Educational Quizzes</h3>
                        <p>Fully interactive and engaging educational Quizzes based on the content!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="email-info">
        <div class="container">
            <div class="title-home text-center">
                <h2>Stay tuned</h2>
                <p>Sign up to receive updates. You won’t want to miss out on any of our exciting new products.</p>
            </div>
            <form action="{{route('client.subscribes.post')}}" method="post">
                @csrf
                <input type="email" name="email" class="form-control" placeholder="Your email address">
                @if($errors->has('email'))
                    <span style="color:red;font-weight: 900;">{{$errors->first('email')}}</span>
                @endif
                @if(session()->has('success'))
                    <span style="color:#ff9e00;font-weight: 900;">{{session()->get('success')}}</span>
                @endif
                <button type="submit" class="btn"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </form>
            <ul class="social-ft">
                <li><a href="#"><img src="{{asset('img/twitter.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/facebook.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/googleplus.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/instagram.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/youtube.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/pinterest.png')}}" alt=""></a></li>
            </ul>
        </div>
    </div>
    <div class="ft">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="copyright-text copyright-left">
                        <strong>2016 © {{$title}}.</strong>
                    </div>

                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="footer_menu">
                        <ul>
                            <li><a href="{{URL::to('p/gioi-thieu')}}" title="Giới thiệu">Giới thiệu</a></li>
                            <li><a href="{{URL::to('p/san-pham-dich-vu')}}" title="Tin rao">Sản phẩm - Dịch vụ</a></li>
                            <li><a href="{{URL::to('product')}}" title="Tin rao">Nhà đất chính chủ</a></li>
                            <li><a href="{{URL::to('p/bang-gia')}}" title="Tin rao">Bảng giá</a></li>
                            <li><a href="{{URL::to('tintuc')}}" title="Tin tức">Tin tức</a></li>
                            <li><a href="{{URL::to('p/lien-he')}}" title="Liên hệ">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
</body>
</html>
