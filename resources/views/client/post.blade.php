@extends('layouts.client')
@section('title', 'Tin tức')

@section('content')
    <div class="new-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    @foreach ($posts as $post)
                        <div class="new-item">
                            <div class="img-new">
                                <a href="#"><img src="{{$post->seo_images}}" alt=""></a>
                            </div>
                            <div class="post-meta-data style2">
                                <span class="post-meta-cats"><a href="#">News</a></span>
                                <span class="post-meta-date">10.06.19</span>
                            </div>
                            <h2><a href="{{ route('client.posts.detail', $post->slug) }}">{{$post->name}}</a></h2>
                            <p>{{str_limit($post->seo_desc, $limit = 150, $end = "[…]")}} </p>
                            <a href="{{ route('client.posts.detail', $post->slug) }}">Read More</a>
                        </div>
                    @endforeach
                    <div class="wp-pagenavi" role="navigation">
                        {{-- <span class="pages">Trang 1 trên 5</span>
                        <span aria-current="page" class="current">1</span> --}}
                        {{$posts->onEachSide(1)->links()}}
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="sidebar">
                        <form>
                            <input type="text" name="" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                        <div class="recent-posts">
                            <h4>Tin tức & Sự kiện</h4>
                            <ul>
                                @foreach($newpost as $new)
                                    <li><a href="{{ route('client.posts.detail', $new->slug) }}">{{$new->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="recent-posts">
                            <h4>Bài viết liên quan</h4>
                            <ul>
                                @foreach($related as $relate)
                                    <li><a href="{{ route('client.posts.detail', $relate->slug) }}">{{$relate->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
