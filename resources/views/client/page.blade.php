@extends('layouts.client')
@section('title', $page->name)

@section('content')
	<div class="container" style="min-height:400px">
    <div class="row">
        <h2 class="title mgt10">
            <span>{{$page->name}}</span></h2>
        <div class="row three-cols about-us">
            {!! $page->content !!}
        </div>
    </div>
</div>
@endsection
