@extends('layouts.client')
@section('title', 'Thông Tin Liên Hệ')

@section('content')
	<div class="main-content " style="min-height:400px">


    <div class="rows three-cols">
        <div class="cols-2">
            <div class="width-680">
                <h3>Bản đồ</h3>
                <div class="map-block clearfix">
                    <img width="680" height="394" alt="" src="images/1.jpg">
                </div>
            </div>
        </div>
        <div class="cols">
            <h3>Thông tin</h3>
            <table class="contact-info">
                <tbody>
                    <tr>
                        <th>
                            <h4 class="email">Email</h4>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Hỗ trợ thông tin?</h5>
                            <p><a href="mailto:hotro@rnet.vn " data-email-type="info"
                                    data-role="protect-email">rnetcompany@gmail.com</a></p>
                            <h5>Cơ hội việc làm?</h5>
                            <p><a href="mailto:rnetcompany@gmail.com" data-email-type="job"
                                    data-role="protect-email">rnetcompany@gmail.com</a></p>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <h4 class="phone">Điện thoại</h4>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <strong>0911.889.682 </strong>
                            </p>
                            <p>
                                <strong>0964.685.166</strong>
                            </p>
                            <p><strong>0968.010.122</strong></p>
                            <p><strong>0966.826.212</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <h4 class="home-link">Trụ sở chính</h4>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p>Địa chỉ: Lô 5.1 khu dự án báo An Ninh, Phố Trần Bình, Cầu Giấy – Hà Nội</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection