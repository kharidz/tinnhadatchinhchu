@extends('layouts.client')


@section('title', 'Danh sách sản phẩm')

@section('content')

<div class="container-fluid padding-lg" style="min-height:450px">
    <div class="row">
        <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel panel-heading">
                <h4 class="panel-title">Loại bất động sản</h4>
            </div>
            <div class="panel-body" id="divLeftCat">
                <ul class="nav nav-pills nav-stacked">
                    @foreach ($cates as $cate)
                        @if ($cate->childs()->count() > 0)
                        <li><a href="{{ route('client.products.index') }}?cate={{$cate->id}}">{{$cate->name}}</a></li>
                            <ul class="nav">
                                @foreach ($cate->childs as $childs)
                                    <li><a href="{{ route('client.products.index') }}?cate={{$childs->id}}">&nbsp;&nbsp;&nbsp;--{{$childs->name}}</a></li>
                                @endforeach
                            </ul>
                        @elseif($cate->parent_id == 0)
                            <li><a href="{{ route('client.products.index') }}?cate={{$cate->id}}">{{$cate->name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Tỉnh Thành</h4>
            </div>
            <div class="cat-list" id="divLeftCity">
                <ul class="nav">
                    <li class=""><a id="city2" href="#" class="child pl10 mgl10 act">{{$provinces}}</a></li>

                    <li class="sub" id="show-all"><a title="tất cả" href="javascript:ShowCityLink(1);"> Tất cả [ <font
                                style="color:#f87310;float:none;">+</font> ]</a></li>
                    <li class="sub hide" id="hide-all"><a title="thu gọn" href="javascript:ShowCityLink(0);"> Thu gọn [
                            <font style="color:#f87310;float:none;">-</font> ]</a></li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <script type="text/javascript">
            function ShowCityLink(showid) {
                if (showid == 1) {
                    $(".sub").removeClass("hide");
                    $("#show-all").addClass("hide");
                } else {
                    $(".sub").addClass("hide");
                    $("#show-all").removeClass("hide");
                }
            }
        </script>
        <div class="clear"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Thông tin tài khoản</h4>
            </div>
            <div class="panel-body">
                <ul class="nav">
                    <li><a href="{{route('client.profile.bookmarks')}}" class="child pl10 mgl10">Tin rao đã lưu</a></li>
                    <li><a href="#" class="child pl10 mgl10">Số dư: <span class="red bold">{{(auth()->user()->amount) ? number_format(auth()->user()->amount) : 0}}</span> (VNĐ)</a></li>
                    <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">{{ (!auth()->user()->amount) ? "Hết Hạn" : round(auth()->user()->amount / 15000)." Ngày"}}</span></a></li>
                    <li><a href="{{ route('client.profile.payhistory') }}" class="child pl10 mgl10">Lịch sử giao dịch</a></li>
                    <li><a href="{{ route('client.profile.index') }}" title="Thay đổi thông tin cá nhân"
                            class="child pl10 mgl10">Thông tin tài khoản</a></li>

                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
        <div class="col-md-9">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Tin môi giới</h4>
            </div>
            <div class="panel-body">
                <div class="row padding-md">
                    <form action="{{ route('client.products.index') }}" id="seachbox" method="get">
                        <input type="text" name="title"
                               title="Nhập thông tin tìm kiếm" class="form-control" placeholder="Nhập thông tin tìm kiếm">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" id="l-cat">
                                    <label for="Cat">Phân mục</label>
                                    <select class="form-control" id="cate" name="cate">
                                        @foreach ($cates as $cate)
                                            @if($cate->childs()->count() > 0)
                                                <option
                                                    @if(Request::get('cate') == $cate->id)
                                                    selected
                                                    @endif
                                                    value="{{$cate->id}}">{{$cate->name}}</option>
                                                @foreach ($cate->childs as $childs)
                                                    <option
                                                        @if(Request::get('cate') == $childs->id)
                                                        selected
                                                        @endif
                                                        data-parent="{{$childs->parent_id}}"
                                                        value="{{$childs->id}}">--{{$childs->name}}</option>
                                                @endforeach
                                            @elseif($cate->parent_id == 0)
                                                <option
                                                    @if(Request::get('cate') == $cate->id)
                                                    selected
                                                    @endif
                                                    value="{{$cate->id}}">{{$cate->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="l-city">
                                    <label for="city">Tỉnh/thành</label>
                                    <select class="form-control" id="city" name="city">
                                            <option @if(Request::get('city') == auth()->user()->province_id)
                                                    selected
                                                    @endif value="">{{$provinces}}</option>

{{--                                        @foreach($near_city as $city)--}}
{{--                                            <option @if(Request::get('city') == auth()->user()->province_id)--}}
{{--                                                    selected--}}
{{--                                                    @endif value="{{$city->code}}">{{$city->name}}</option>--}}
{{--                                        @endforeach--}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="l" id="l-dist">
                                    <label for="district">Quận/huyện</label>
                                </div>
                                <div id="Dists">
                                    <select id="district" name="district" class="form-control">
                                        <option value="">Tất cả</option>
                                        @foreach ($districts as $district)
                                            <option
                                                @if(Request::get('district') == $district->code)
                                                selected
                                                @endif
                                                value="{{$district->code}}">{{$district->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="l" id="l-price">
                                    <label for="Price">Giá Bán/Cho thuê</label>
                                </div>
                                <div>
                                    <select id="price" name="price" class="form-control">
                                        @if (!$type_gia)
                                            <option selected="selected" value="0">--Giá bán--</option>
                                            <option value="1">&lt; 10 triệu</option>
                                            <option value="2">10 triệu - 100 triệu</option>
                                            <option value="3">100 triệu - 1 tỷ</option>
                                            <option value="4">1 tỷ - 3 tỷ</option>
                                            <option value="5">3 - 7 tỷ</option>
                                            <option value="6">7 - 10 tỷ</option>
                                            <option value="7">10 - 20 tỷ</option>
                                            <option value="9">&gt; 20 tỷ</option>
                                        @else
                                            <option selected="selected" value="0">--Giá cho thuê--</option>
                                            <option value="1">&lt; 1 triệu</option>
                                            <option value="2">1 - 3 triệu</option>
                                            <option value="3">3 - 5 triệu</option>
                                            <option value="4">5 - 10 triệu</option>
                                            <option value="5">10 - 40 triệu</option>
                                            <option value="6">40 - 70 triệu</option>
                                            <option value="7">70 - 100 triệu</option>
                                            <option value="8">&gt; 100 triệu</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="l" id="l-d1">
                                    <label for="D1">Từ</label>
                                </div>
                                <div>
                                    <input class="form-control" type="date" data-date="" data-date-format="DD MMMM YYYY" value="" id="D1" name="D1">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="l" id="l-d2">
                                    <label for="D2">Đến</label>
                                </div>
                                <div>
                                    <input class="form-control" type="date" data-date="" data-date-format="DD MMMM YYYY" value="" id="D2" name="D2">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="l" id="l-k">
                                    <label for="phone">Điện thoại</label>
                                </div>
                                <div>
                                    <input id="phone" name="phone" type="text" value="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="padding-top-lg">
                                    <button id="btnFilter" class="btn btn-primary" type="submit" class="pr-search">Tìm Kiếm</button>
                                    <button type="button" class="btn btn-success" id="export-data">Xuất Excel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="m g" id="FR">
                    <div class="validation-summary-valid" data-valmsg-summary="true">
                        <ul>
                            <li style="display:none"></li>
                        </ul>
                    </div>

                    <div class="clear"></div>
                    <div class="validation-summary-errors" data-valmsg-summary="true">
                        <ul>
                            @if (session()->has('notib'))
                                <p style="color:red">{{session()->get('notib')}}</p>
                            @endif
                            <li style="display:none"></li>
                        </ul>
                    </div>
                    @if (auth()->user()->amount < $amount)
                        <p style="text-align: center">
                            <span style="padding: 2px;">Để gia hạn sử dụng hoặc xem thêm những tin mới nhất, Quý Khách vui lòng thanh toán phí <b>nâng cấp tài khoản không giới hạn chức năng</b> chỉ với giá <b style="color:Red">{{number_format($amount)}}đ/ngày! và nạp tối thiểu 75,000 VNĐ</b>. Liên hệ hotline <span style="color:red;">0903.402.702</span> để nhận được nhiều ưu đãi và hỗ trợ hơn nữa!</span><br>
                        </p>
                    @else
                        <div class="box-pr">

                            <div class="clear"></div>
                            <div style="text-align: center;">
                                <div class="pagination-container">
                                    <ul class="pagination">
                                        {{$products->onEachSide(1)->links()}}
                                    </ul>
                                </div>
                            </div>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <th width="40px">
                                        Ngày
                                    </th>
                                    <th width="80px">
                                        Loại
                                    </th>
                                    <th>
                                        {{$products->total()}} Tin rao
                                    </th>
                                    <th width="70px">
                                        Lưu tin
                                    </th>
                                    <th>Môi giới</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($products as $product)
                                    @if (in_array($product->id, $books) || in_array($product->id,$agency))
                                        @continue
                                    @endif
                                    <tr>
                                        <td>
                                            <p>
                                                {{$product->created_at->format('d/m/Y')}}
                                            </p>
                                        </td>
                                        <td align="center" @if (in_array($product->id, $views))
                                        style="background-image:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZlcnNpb249JzEuMScgd2lkdGg9JzE2MHB4JyBoZWlnaHQ9JzU4cHgnPg0KPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzE4MCcgaGVpZ2h0PSc3MCcgc3R5bGU9J3N0cm9rZTp3aGl0ZTsgZmlsbDpsaWdodEJsdWU7IHN0cm9rZS1vcGFjaXR5OiAwLjM7IHN0cm9rZS13aWR0aDogM3B4OyBmaWxsLW9wYWNpdHk6IDAuOTsgc3Ryb2tlLWRhc2hhcnJheTogMTAgNTsgc3Ryb2tlLWxpbmVjYXA9cm91bmQ7ICcvPg0KPHRleHQgeD0nODUnIHk9JzMwJyBzdHlsZT0nZmlsbDp3aGl0ZTsgdGV4dC1hbmNob3I6IG1pZGRsZScgZm9udC1zaXplPScxNicgdHJhbnNmb3JtPSdyb3RhdGUoLTE1LDEwMCwyNSknPlhlbSAxNS8wNS8yMDE5PC90ZXh0Pjwvc3ZnPg==)"
                                            @endif >
                                <span class="v">
                                    {{$product->getcate()}}
                                </span>
                                        </td>
                                        <td data-slug="{{$product->slug}}">
                                            @if (in_array($product->id, $views))
                                                <p class="pdt">
                                                    <a href="javascript:;" class="pro-tits">{{$product->name}}</a>
                                                </p>
                                                <div class="buido"style="text-align:justify;line-height:22px;max-width:441px; overflow:hidden;">
                                                    {{strip_tags(html_entity_decode($product->content))}}
                                                </div>
                                            @else
                                                <p class="pdt">
                                                    <a href="javascript:;" data-slug="{{$product->slug}}" class="pro-tit">{{$product->name}}</a>
                                                </p>
                                                <div id="div{{$product->slug}}" class="buido" style="text-align:justify;line-height:22px;max-width:441px; overflow:hidden;">
                                                    {{str_limit(strip_tags(html_entity_decode($product->content)), $limit = 50, $end = '...')}}
                                                </div>
                                            @endif

                                            <div class="clear"></div>

                                            <br>
                                            <div class="pt10">

                                                <b>KV:</b> <span class="v">{!!$product->getdistrict()!!}</span><b class="pl10">LH:</b> <span
                                                    class="v">{!!$product->contact_phone!!}</span><span></span>
                                                <br>
                                                <b>Giá:</b> <span class="v">{{($product->price) ? number_format($product->price). "VNĐ": "Liên hệ"}}</span>
                                                <b class="pl10">DT:</b> <span class="v">{{$product->area}} m 2</span>
                                                <span class="clear">
                                    </span></div>
                                        </td>
                                        <td class="v">
                                            <div class="clear mgt5">
                                                <button data-slug="{{ $product->slug }}" class="save-item btn btn-danger btn-xs">
                                                    Lưu tin
                                                </button>
                                            </div>
                                        </td>
                                        <td class="agency" style="text-align: center;">
                                <span style="cursor: pointer;" data-id="{{$product->id}}" class="on">
                                    <button class="btn btn-primary btn-xs">Môi giới</button>
                                </span>
                                        </td>
                                    </tr>
                                @empty
                                    Không có sản phẩm
                                @endforelse
                                </tbody>
                            </table>

                            <div class="clear"></div>
                            <div style="text-align: center;">
                                <div class="pagination-container">
                                    <ul class="pagination">
                                        {{$products->onEachSide(1)->links()}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript">
    $('form#seachbox').submit(function() {
        $(':input', this).each(function() {
            this.disabled = !($(this).val());
        });
    });
    $('#export-data').on('click', function(){
        var params = window.location.search;
        window.open(`{{route('client.products.export')}}/${params}`);
    });
    $('.save-item').on('click', function(){
        var slug = $(this).attr('data-slug');
        $.ajax({
            url: `{{route('client.products.save')}}/${slug}`,
            method: 'get'
        }).done(function(result){
            alert(result.msg);
        });
    });
    $('.pro-tit').on('click', function(){
        var slug = $(this).attr('data-slug');
        $.get(`{{ route('client.products.content') }}/${slug}`, {}, function (res) {
            $(`#div${slug}`).html(res);
        });
        $(this).off('click');
    });
    $('#city').on('change', function(e){
        var province_id = $(this).children("option:selected").val();
        // console.log(province_id);
        $.ajax({
            url: '{{ route('district') }}',
            method: 'post',
            data: {
                province_id: province_id,
                _token: '{{csrf_token()}}'
            }
        }).done(function(result){
            let temp = '';
            $.each(result, function(k, val){
                var code = val.code;
                var name = val.name;
                temp += `<option value="${code}">${name}</option>`;
            });
            $('#district').html(temp);
        });
    });

    $('#cate').on('change', function(){
       var parent = $('option:selected', this).attr('data-parent');
       if($(this).val() == 1 || parent == 1 || $(this).val() == 4){
           $('#price').html('<option selected="selected" value="0">--Giá bán--</option>\n' +
            '       <option value="1">&lt; 10 triệu</option>\n' +
            '       <option value="2">10 triệu - 100 triệu</option>\n' +
            '       <option value="3">100 triệu - 1 tỷ</option>\n' +
            '       <option value="4">1 tỷ - 3 tỷ</option>\n' +
            '       <option value="5">3 - 7 tỷ</option>\n' +
            '       <option value="6">7 - 10 tỷ</option>\n' +
            '       <option value="7">10 - 20 tỷ</option>\n' +
            '       <option value="9">&gt; 20 tỷ</option>');
       }else{
           $('#price').html('<option selected="selected" value="0">--Giá cho thuê--</option>\n' +
               '                                        <option value="1">&lt; 1 triệu</option>\n' +
               '                                        <option value="2">1 - 3 triệu</option>\n' +
               '                                        <option value="3">3 - 5 triệu</option>\n' +
               '                                        <option value="4">5 - 10 triệu</option>\n' +
               '                                        <option value="5">10 - 40 triệu</option>\n' +
               '                                        <option value="6">40 - 70 triệu</option>\n' +
               '                                        <option value="7">70 - 100 triệu</option>\n' +
               '                                        <option value="8">&gt; 100 triệu</option>');
       }
    });
    jQuery('body').on('click','.tblprice td span.on',function(){
        if(confirm('Đánh dấu tin này là môi giới. Tin môi giới sẽ không hiển thị sau này nữa'))
        {
            var curr = jQuery(this);
            var id = jQuery(this).data('id');
            var DataForm = new FormData();
            DataForm.append('id',id);
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('user.add_agency')}}',
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: DataForm
            }).done(function(res){
                if(res.success == true)
                {
                    curr.parents('tr').remove();
                }
                console.log(res.msg);
            });
        }

  });
</script>
@endsection
