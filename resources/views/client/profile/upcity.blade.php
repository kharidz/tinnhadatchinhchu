@extends('layouts.client')
@section('title','Chọn tỉnh thành môi giới (bắt buộc)')

@section('content')
<div class="container padding-lg" style="min-height:450px">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="title mgt10">
                    <span>@yield('title')</span>
                </h2>
            </div>
            <div class="panel-body">
                <form action="{{route('client.profile.RegisterCity')}}" id="RegisterCityFrm" method="post">
                    @csrf
                    <div class="validation-summary-errors" data-valmsg-summary="true"><ul>
                            @if ($errors->has('City'))
                                <li>{{$errors->first('City')}}</li>
                            @endif
                            <li style="display:none"></li>
                        </ul></div>            <table class="tblprice">
                        <tr>
                            <th width="110px" style="text-align: left">
                                Tỉnh/thành môi giới
                            </th>
                            <td>
                                <select data-val="true" id="City" name="City" class="form-control" style="width:300px;">
                                    <option selected="selected" value="">--KXĐ--</option>
                                    @foreach ($citys as $city)
                                        <option value="{{$city->code}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #fff;"></td>
                            <td style="border: 1px solid #fff;">
                                <input id="btnRegisterCity" class="btn btn-primary" type="submit" value="Xác nhận & đăng ký" />
                            </td>
                        </tr>
                    </table>
                    <input id="Id" name="Id" type="hidden" value="W+Wz57uOvTQJREdj59SrIg9SA3864r0yyePjjP+II1z7fVZRbZlKNrm0v4x5eYXx3UBAVSCuVwICMgrJ3SYUrA==" /><input id="ReturnUrl" name="ReturnUrl" type="hidden" value="/Product" /></form>
                <p style="text-align: justify; line-height: 22px; width: 500px;">
                    <b style="color: Red">Chú ý: </b>Vui lòng đăng ký <b style="color: Blue">chính xác </b>tỉnh thành môi giới để đảm bảo quyền lợi của quý khách.
                    <br />
                    VD: Anh Nguyễn Văn A chuyên môi giới tại Hà Nội sẽ chọn tỉnh thành môi giới là <b>Hà Nội</b>.
                </p>
                <p style="text-align: justify; line-height: 22px; width: 500px;">
                    Nếu gặp bất kỳ khó khăn gì trong quá trình sử dụng website nói chung, Quý vị hãy liên hệ ngay với chúng tôi theo số đt: 0976.512.695
                    hoặc email: hotro@rnet.vn để được trợ giúp!
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
