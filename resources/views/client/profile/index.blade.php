@extends('layouts.client')
@section('title','Trang Cá Nhân')

@section('content')
<div class="container padding-lg" style="min-height:450px">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Trang cá nhân</h4>
                </div>
                <div class="panel-body">
                    <ul class="nav">
                        <li><a href="{{route('client.profile.bookmarks')}}" class="child pl10 mgl10">Tin rao đã lưu</a></li>
                        <li><a href="#" class="child pl10 mgl10">Số dư: <span class="red bold">{{number_format($user->amount)}}</span> (VNĐ)</a></li>
                        <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">{{ (!auth()->user()->amount) ? "Hết Hạn" : round(auth()->user()->amount / 15000)." Ngày"}}</span></a></li>                    <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">01/01/0001</span></a></li>
                        <li><a href="{{ route('client.profile.payhistory') }}" class="child pl10 mgl10">Lịch sử giao dịch</a></li>
                        <li><a href="{{ route('client.profile.index') }}" title="Thay đổi thông tin cá nhân" class="child pl10 mgl10">Thông tin tài khoản</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Thông tin</h4>
                </div>
                <div class="panel-body">
                    <form class="box-pr" action="#" method="post">
                        <div class="validation-summary-errors" data-valmsg-summary="true">
                            <ul>
                                <li style="display:none"></li>
                                @if (session()->has('success'))
                                    <li>{{session()->get('success')}}</li>
                                @endif
                            </ul>
                        </div>
                        <div class="box-pr-tit">Thông tin tài khoản cá nhân</div>
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-2"><label for="email">Email</label></div>
                                <div class="col-md-10">{{$user->email}}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2"><label for="fullname">Họ tên</label></div>
                                <div class="col-md-10">
                                    <input class="form-control" data-val="true" data-val-required="Hãy điền họ và tên!" id="fullname" name="fullname" placeholder="Nhập họ và tên" style="width: 300px;" type="text" value="{{$user->fullname}}">
                                    @if ($errors->has('fullname'))
                                        <span>{{$errors->first('fullname')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2"><label for="phone">Điện thoại</label></div>
                                <div class="col-md-10">
                                    <input class="form-control" data-val="true" data-val-required="Hãy điền số điện thoại!" id="phone" name="phone" placeholder="Nhập số điện thoại" style="width: 300px;" type="number" value="{{$user->phone}}">
                                    @if ($errors->has('phone'))
                                        <span>{{$errors->first('phone')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group"><button type="submit" class="btn btn-primary">Cập nhật</button></div>
                        </table>
                    </form>
                    <form class="box-pr" action="{{ route('client.profile.password') }}" method="post">
                        <div class="validation-summary-errors" data-valmsg-summary="true">
                            <ul>
                                <li style="display:none"></li>
                                @if (session()->has('passnoti'))
                                    <li>{{session()->get('passnoti')}}</li>
                                @endif
                            </ul>
                        </div>
                        <div class="box-pr-tit">Đổi mật khẩu</div>
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="oldpassword">Mật khẩu cũ</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control" id="oldpassword" name="oldpassword" placeholder="Nhập mật khẩu cũ" style="width: 300px;" type="password">
                                    @if ($errors->has('oldpassword'))
                                        <span>{{$errors->first('oldpassword')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="newpassword">Mật khẩu mới</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control" id="newpassword" name="newpassword" placeholder="Nhập mật khẩu mới" style="width: 300px;" type="password">
                                    @if ($errors->has('newpassword'))
                                        <span>{{$errors->first('newpassword')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="newpassword_confirmation">Nhập lại mật khẩu mới</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control" id="newpassword_confirmation" name="newpassword_confirmation" placeholder="Nhập lại mật khẩu mới" style="width: 300px;" type="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
