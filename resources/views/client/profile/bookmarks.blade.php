@extends('layouts.client')
@section('title','Sản phẩm đã được lưu')

@section('content')
<div class="container padding-lg" style="min-height:450px">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Trang cá nhân</h4>
                </div>
                <div class="panel-body">
                    <ul class="nav">
                        <li><a href="{{route('client.profile.bookmarks')}}" class="child pl10 mgl10">Tin rao đã lưu</a></li>
                        <li><a href="#" class="child pl10 mgl10">Số dư: <span class="red bold">{{number_format($user->amount)}}</span> (VNĐ)</a></li>
                        <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">{{ (!auth()->user()->amount) ? "Hết Hạn" : round(auth()->user()->amount / 15000)." Ngày"}}</span></a></li>                    <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">01/01/0001</span></a></li>
                        <li><a href="{{ route('client.profile.payhistory') }}" class="child pl10 mgl10">Lịch sử giao dịch</a></li>
                        <li><a href="{{ route('client.profile.index') }}" title="Thay đổi thông tin cá nhân" class="child pl10 mgl10">Thông tin tài khoản</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-danger">
                @if (session()->has('notib'))
                    <p style="color:red">{{session()->get('notib')}}</p>
                @endif
                <div class="panel-heading">Thông tin @yield('title')
                    <a target="_blank" href="{{route('client.profile.bookmarks.export')}}" class="btn-bookmark">Lưu Excel</a>
                </div>
                <table class="table table-hover" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>Loại</th>
                        <th>Nội dung</th>
                        <th>Địa chỉ</th>
                        <th>Hành động</th>
                    </tr>

                    @foreach ($products as $k => $product)
                        <tr>
                            <td>{{$k+1}}</td>
                            <td>{{$product->getcate()}}</td>
                            <td><a href="javascript:;" class="ShowMore" data-slug="{{$product->slug}}">{{$product->name}}</a>
                                <div id="div{{$product->slug}}">
                                    {!!str_limit(html_entity_decode($product->content), $limit = 50, $end = "....")!!}
                                </div></td>
                            <td>{{$product->address.",".$product->getdistrict().",".$product->getprovince()}}</td>
                            <td><button class="btn btn-xs btn-primary btn-delete" data-id="{{$product->id}}">Xóa</button></td>
                        </tr>
                    @endforeach
                </table>
                <div class="clear"></div>
                <div style="text-align: center;">
                    <div class="pagination-container">
                        <ul class="pagination">
                            {{$products->onEachSide(1)->links()}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		$('.ShowMore').on('click', function(){
			var slug = $(this).attr('data-slug');
			$.get(`{{ route('client.products.content') }}/${slug}`, {}, function (res) {
				$(`#div${slug}`).html(res);
			});
			$(this).off('click');
		});

		$('.btn-delete').on('click', function(){
		   var id = $(this).attr('data-id');
		   $.ajax({
               url: '{{route('client.profile.bookmarks')}}',
               method: 'post',
               data: {
                   id: id,
                   _token: '{{csrf_token()}}'
               }
           }).done(function(result){
               if (!result.errors){
                   window.location.reload();
               }
           });
        });
	</script>
@endsection
