@extends('layouts.client')
@section('title','Lịch sử nạp tiền')

@section('content')
<div class="container padding-lg" style="min-height:450px">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Trang cá nhân</h4>
                </div>
                <div class="panel-body">
                    <ul class="nav">
                        <li><a href="{{route('client.profile.bookmarks')}}" class="child pl10 mgl10">Tin rao đã lưu</a></li>
                        <li><a href="#" class="child pl10 mgl10">Số dư: <span class="red bold">{{number_format($user->amount)}}</span> (VNĐ)</a></li>
                        <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">{{ (!auth()->user()->amount) ? "Hết Hạn" : round(auth()->user()->amount / 15000)." Ngày"}}</span></a></li>                    <li><a href="#" class="child pl10 mgl10">Hạn dùng: <span class="red bold">01/01/0001</span></a></li>
                        <li><a href="{{ route('client.profile.payhistory') }}" class="child pl10 mgl10">Lịch sử giao dịch</a></li>
                        <li><a href="{{ route('client.profile.index') }}" title="Thay đổi thông tin cá nhân" class="child pl10 mgl10">Thông tin tài khoản</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="panel-title">Thông tin @yield('title')</h4>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>Số tiền</th>
                            <th>Nội dung</th>
                            <th>Ngày nạp tiền</th>
                        </tr>

                        @foreach ($payments as $k => $payment)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{number_format($payment->amount)}} VNĐ</td>
                                <td>{{str_limit($payment->content, $limit = 150, $end = "....")}}</td>
                                <td>{{$payment->created_at}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
@endsection
