@extends('layouts.client')
@section('title', $post->seo_title)

@section('content')
    <div class="single padding-lg">
        <div class="new-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="single-content">
                            <h2>{{$post->name}}</h2>
                            <img src="{{$post->seo_images}}" alt="">
                            <p>{!!$post->content!!}</p>
                        </div>
                        <div class="share">
                            <span>Share:</span>
                            <div class="social-share">
                                <a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#" title="Google"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                <a href="#" title="Pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="sidebar">
                            <form>
                                <input type="text" name="" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                            <div class="recent-posts">
                                <h4>Tin tức & Sự kiện</h4>
                                <ul>
                                    @foreach($newpost as $new)
                                        <li><a href="{{ route('client.posts.detail', $new->slug) }}">{{$new->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="recent-posts">
                                <h4 class="title">Bài viết liên quan</h4>
                                <ul>
                                    <ul>
                                        @foreach($related as $relate)
                                            <li><a href="{{ route('client.posts.detail', $relate->slug) }}">{{$relate->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
