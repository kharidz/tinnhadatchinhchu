@extends('layouts.client')
@section('title', 'BÁO GIÁ DỊCH VỤ KHAI THÁC TIN RAO NHÀ ĐẤT CHÍNH CHỦ')

@section('content')
	<div class="main-content " style="min-height:400px">
    <div class="intro">
        <h2 class="title mgt10">
            <span>Báo giá dịch vụ khai thác tin rao nhà đất chính chủ</span></h2>
        <div class="rows three-cols about-us">
            <p style="text-align: justify">
                Phí sử dụng dịch vụ <b>15.000 vnđ/ngày</b>. Khách hàng mua tối thiểu 5 ngày trở lên. Mức chiết khấu áp
                dụng cho Khách hàng khi mua gói dịch vụ có thời gian sử dụng từ 1 tháng trở lên như sau:</p>
            <div style="text-align: center; display: block;">
                <table class="tblprice" >
                    <tbody>
                        <tr>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Khuyến mại cộng thêm
                            </th>
                        </tr>
                        <tr>
                            <td>
                                Từ 01 tháng đến dưới 03 tháng
                            </td>
                            <td align="center">
                                10%
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Từ 3 tháng đến dưới 6 tháng
                            </td>
                            <td align="center">
                                20%
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Từ 6 tháng đến 1 năm
                            </td>
                            <td align="center">
                                30%
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p style="text-align: justify; line-height: 22px;">
            <strong>Liên hệ:</strong><br>
            Mobile:
            <strong>0911.889.682 - 0964.685.166 - 0968.010.122 - 0966.826.212</strong><br>
            Email: <a href="mailto:rnetcompany@gmail.com">rnetcompany@gmail.com</a>&nbsp;<br>
            Website:&nbsp;<a href="http://nhachinhchu.com.vn/">http://nhachinhchu.com.vn</a>
        </p>
    </div>
</div>
@endsection