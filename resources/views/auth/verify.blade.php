@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
        <br>
        <br>
        <p style="text-align: justify; line-height: 22px; width: 500px;">
            Nếu gặp bất kỳ khó khăn gì trong việc đăng ký, đăng nhập, hay trong việc sử dụng
            website nói chung, Quý Khách hãy liên hệ ngay với chúng tôi theo số Hotline hoặc email: <b
                style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
        </p>
        <ul>
            @foreach($phone as $phon)
            <li><a href="tel:{{preg_replace('/[^0-9]/','',$phon)}}"><b style="color: Blue">{{$phon}}</b></a>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
