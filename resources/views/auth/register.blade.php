@extends('layouts.client')
@section('title', 'Đăng Ký Tài Khoản')
@section('content')
    <div class="container padding-top-lg" style="min-height:400px">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h2 class="title mgt10">
                <span>@yield('title')</span>
            </h2>
        </div>
            <div class="panel-body">
                <form action="#" id="RegisterForm" method="post">
                    @csrf
                    <div class="@if (session('errlogin'))
                        validation-summary-errors
                    @else
                        validation-summary-errors
                    @endif
                    " data-valmsg-summary="true">
                        <ul id="errors">
                            @if (session('errlogin'))
                                <li>{!! session('errlogin') !!}</li>
                            @endif
                            <li style="display:none"></li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" data-val="true"
                               data-val-regex="Sai khuôn dạng địa chỉ hộp thư điện tử!"
                               data-val-regex-pattern="\s*([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+[.]*@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}\s*"
                               data-val-required="Hãy điền địa chỉ hộp thư điện tử!" id="email"
                               name="email" placeholder="Nhập địa chỉ hộp thư điện tử"
                               style="width: 300px;" type="text" value="" />
                    </div>
                    <div class="form-group">
                        <label for="name">Họ tên</label>
                        <input class="form-control" data-val="true" data-val-required="Hãy điền Họ tên!"
                               id="name" name="name" placeholder="Nhập họ tên"
                               style="width: 300px;" type="text" />
                    </div>
                    <div class="form-group">
                        <label for="phone">Số điện thoại</label>
                        <input class="form-control" data-val="true" data-val-required="Hãy điền số điện thoại!"
                               id="phone" name="phone" placeholder="Nhập số điện thoại"
                               style="width: 300px;" type="text" />
                    </div>
                    <div class="form-group">
                        <label for="LoginPassword">Mật khẩu</label>
                        <input class="form-control" data-val="true" data-val-required="Hãy điền mật khẩu!"
                               id="LoginPassword" name="LoginPassword" placeholder="Nhập mật khẩu"
                               style="width: 300px;" type="password" />
                    </div>
                    <div class="form-group">
                        <button id="submit" type="submit" class="btn btn-primary">Đăng ký</button>
                    </div>
                </form>
                <br>
                <div class="margin-md">
                    <p style="text-align: justify; line-height: 22px; width: 500px;">
                        Nếu gặp bất kỳ khó khăn gì trong việc đăng ký, đăng nhập, hay trong việc sử dụng
                        website nói chung, Quý Khách hãy liên hệ ngay với chúng tôi theo số Hotline hoặc email: <b
                            style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
                    </p>
                    <ul>
                        @foreach($phone as $phon)
                            <li><a href="tel:{{preg_replace('/[^0-9]/','',$phon)}}"><b style="color: Blue">{{$phon}}</b></a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
</div>
@endsection


@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(function(){

            $('input').on('focus', function(){
                $('#errors').html('');
            });
            $('#RegisterForm').on('submit', function(e){
                $('#submit').attr('disabled', true);
                $('#submit').html('<i class="fa fa-spin fa-spinner"></i> Đang Khởi Tạo ');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{ route('auth.registers') }}',
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                })
                .done(function(result) {
                    let msg = result.msg;
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Đăng ký');
                    if (result.errors) {
                        (msg.hasOwnProperty('email')) ? $('#errors').append("<li>"+msg.email[0]+"</li>") : null;
                        (msg.hasOwnProperty('phone')) ? $('#errors').append("<li>"+msg.phone[0]+"</li>") : null;
                    }else{
                        swal({
                            title: "Thông Báo!!!",
                            text: msg,
                            icon:'success'
                        }).then(()=>{
                            $(location).attr('href', '{{ route('client.home') }}');
                        });
                    }
                })
                .fail(function() {
                    console.log("error");
                });
                
            });
        })
    </script>
@endsection
