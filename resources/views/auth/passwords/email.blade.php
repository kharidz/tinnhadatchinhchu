@extends('layouts.client')
@section('title', 'Quên mật khẩu tài khoản')
@section('content')
<div class="main-content " style="min-height:450px">
    <div class="intro">
        <h2 class="title mgt10"><span>@yield('title')</span></h2>
        <form action="{{ route('password.forgot') }}" method="post">
            @csrf
            <div class="@if (session('errs') || session('success'))
                            validation-summary-errors
                        @else
                            validation-summary-valid
                        @endif" data-valmsg-summary="true">
                <ul id="listerr">
                    @if (session('errs'))
                        <li>{{ session('errs') }}</li>
                    @endif
                    <li style="display:none"></li>
                </ul>
            </div>
            <table class="tblprice">
                <tr>
                    <th><label for="Email">Email</label></th>
                    <td>
                        <input class="txt" value="{{old('Email')}}" data-val="true" data-val-regex="Sai khuôn dạng địa chỉ hộp thư điện tử!" data-val-regex-pattern="\s*([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+[.]*@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}\s*" data-val-required="Hãy nhập địa chỉ hộp thư điện tử đã đăng ký!" id="Email" name="Email" placeholder="Nhập địa chỉ hộp thư điện tử đã đăng ký" style="width: 300px;" type="text" value="" />
                        <input id="Gửi" type="submit" value="Gửi" style="width:50px;" />
                    </td>
                </tr>
            </table>
            <p style="text-align:justify; line-height:22px; width:500px;">
                Nếu gặp bất kỳ khó khăn gì trong việc lấy lại mật khẩu, đăng nhập, hay trong việc sử dụng website nói chung,
                Quý vị hãy liên hệ ngay với chúng tôi theo số điện thoại: <b style="color: Red">0903.402.702</b> hoặc email:
                <b style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
            </p>
            <br>
            <p style="text-align: justify; line-height: 22px; width: 500px;">
                Nếu gặp bất kỳ khó khăn gì trong việc đăng ký, đăng nhập, hay trong việc sử dụng
                website nói chung, Quý Khách hãy liên hệ ngay với chúng tôi theo số Hotline hoặc email: <b
                    style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
            </p>
            <ul>
                @foreach($phone as $phon)
                <li><a href="tel:{{preg_replace('/[^0-9]/','',$phon)}}"><b style="color: Blue">{{$phon}}</b></a>
                </li>
                @endforeach
            </ul>
        </form>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $(()=>{
            $('input').on('focus', ()=>{
                $('#listerr').html('');
            });
        });
    </script>
@endsection
