@extends('layouts.client')
@section('title', 'Đặt lại mật khẩu tài khoản')
@section('content')
<div class="main-content " style="min-height:400px">
    <div class="intro">
        <h2 class="title mgt10">
            <span>@yield('title')</span>
        </h2>
        <div class="fl">
            <div class="rows three-cols about-us">
                <form action="{{ route('password.forgot.resets') }}" id="RegisterForm" method="post">
                    @csrf
                    <input type="text" name="token" value="{{$user->hash_code}}" hidden>
                    <div class="@if (session('errtoken') || $errors->has('password'))
                                validation-summary-errors
                            @endif" data-valmsg-summary="true">
                        <ul id="errors">
                            @if (session('errtoken'))
                                <li>{{session('errtoken')}}</li>
                            @endif
                            @if ($errors->has('password'))
                                <li>{{$errors->first('password')}}</li>
                            @endif
                           <li style="display:none"></li>
                        </ul>
                    </div>
                    <table class="tblprice">
                        <tr>
                            <th style="text-align: left">
                                <label for="password">Mật khẩu</label>
                            </th>
                            <td>
                                <input class="txt" data-val="true" data-val-required="Hãy điền mật khẩu!"
                                    id="password" name="password" placeholder="Nhập mật khẩu"
                                    style="width: 300px;" type="password" />
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left">
                                <label for="password_confirmation">Mật khẩu</label>
                            </th>
                            <td>
                                <input class="txt" data-val="true" data-val-required="Hãy điền mật khẩu!"
                                    id="password_confirmation" name="password_confirmation" placeholder="Nhập mật khẩu"
                                    style="width: 300px;" type="password" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; border: 1px solid #fff;">
                                <button type="submit" class="btn">Gửi</button>
                                </a>
                            </td>
                        </tr>
                    </table>
                </form>
                <br>
                <p style="text-align: justify; line-height: 22px; width: 500px;">
                    Nếu gặp bất kỳ khó khăn gì trong việc đăng ký, đăng nhập, hay trong việc sử dụng
                    website nói chung, Quý Khách hãy liên hệ ngay với chúng tôi theo số Hotline hoặc email: <b
                        style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
                </p>
                <ul>
                    @foreach($phone as $phon)
                    <li><a href="tel:{{preg_replace('/[^0-9]/','',$phon)}}"><b style="color: Blue">{{$phon}}</b></a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $(()=>{
            $('input').on('focus', ()=>{
                $('#listerr').html('');
            });
        });
    </script>
@endsection
