@extends('layouts.client')

@section('content')
<div class="container padding-top-lg">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2 class="card-title">
                        <span>Đăng nhập hệ thống</span>
                    </h2>
                </div>
                <div class="panel-body padding-sm">
                    <form action="{{ route('logins') }}" id="LoginFrm" method="post">
                        @csrf
                        <div class="@if (session('errlogin') || session('success') || session('sucreset'))
                            validation-summary-errors
                        @else
                            validation-summary-valid
                        @endif
                        " data-valmsg-summary="true">
                            <ul class="margin-md">
                                @if (session('success'))
                                    <li>{{ session('success') }}</li>
                                @endif
                                @if (session('errlogin'))
                                    <li>{{ session('errlogin') }}</li>
                                @endif
                                @if (session('sucreset'))
                                    <li>{{session('sucreset')}}</li>
                                @endif
                                @if (session('notiforgot'))
                                <div style="background: #DFF0D8; padding: 20px; color: green; width: 100%; margin-bottom: 20px; font-size: 14px; border-radius: 10px; ">
                                    <strong>Thành công!</strong> {{session('notiforgot')}}
                                </div>
                                    
                                @endif
                                <li style="display:none"></li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="LoginEmail">Email</label>
                            <input class="form-control" data-val="true"
                                   data-val-regex="Sai khuôn dạng địa chỉ hộp thư điện tử!"
                                   data-val-regex-pattern="\s*([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+[.]*@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}\s*"
                                   data-val-required="Hãy điền địa chỉ hộp thư điện tử!" id="LoginEmail"
                                   name="LoginEmail" placeholder="Nhập địa chỉ hộp thư điện tử"
                                   style="width: 300px;" type="text" value="" />
                        </div>
                        <div class="form-group">
                            <label for="LoginPassword">Mật khẩu</label>
                            <input class="form-control" data-val="true" data-val-required="Hãy điền mật khẩu!"
                                   id="LoginPassword" name="LoginPassword" placeholder="Nhập mật khẩu"
                                   style="width: 300px;" type="password" />
                        </div>
                        <div class="form-group">
                            <input id="btnLogin" class="btn btn-primary" type="submit" value="Đăng nhập" />&nbsp; <a
                                href="/password/reset">
                                Quên mật khẩu?
                            </a>
                        </div>
                    </form>
                    <div class="margin-md">
                        <p style="text-align: justify; line-height: 22px; width: 500px;">
                            <b style="color: Red">Chú ý: </b>Vui lòng <b style="color: Blue">
                                không chia sẻ
                            </b>và bảo mật tài khoản để đảm bảo quyền lợi của Quý Khách. Nếu không đăng nhập
                            được do sự cố (mất điện, tắt máy...), vui lòng chờ và đăng nhập lại sau <b style="color: Blue">
                                5 phút
                            </b>.
                        </p>
                        <p style="text-align: justify; line-height: 22px; width: 500px;">
                            Nếu gặp bất kỳ khó khăn gì trong việc đăng ký, đăng nhập, hay trong việc sử dụng
                            website nói chung, Quý Khách hãy liên hệ ngay với chúng tôi theo số Hotline hoặc email: <b
                                style="color: Blue">sotaychinhchu@gmail.com</b> để được trợ giúp!
                        </p>
                        <ul class="margin-xs">
                            @foreach($phone as $phon)
                                <li><a href="tel:{{preg_replace('/[^0-9]/','',$phon)}}"><b style="color: Blue">{{$phon}}</b></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
