@extends('layouts.server')

@section('title','Danh mục sản phẩm')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
	<div class="row">
		<form class="col-md-4" id="create-cate">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Thêm danh mục</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="name">Tên danh mục</label>
						<input type="text" name="name" id="name" class="form-control">
						<span id="errname" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="parent_id">Chuyên mục cha</label>
						<select name="parent_id" id="parent_id" class="form-control">
							<option value="0">Trống</option>
							@foreach ($parents as $parent)
								@if ($parent->childs()->count() > 0)
								<option value="{{$parent->id}}">{{$parent->name}}</option>
									@foreach ($parent->childs as $childs)
										<option value="{{$childs->id}}">--{{$childs->name}}</option>
									@endforeach
								@elseif($parent->parent_id == 0)
									<option value="{{$parent->id}}">{{$parent->name}}</option>
								@endif
							@endforeach
						</select>
						<span id="errparent" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="description">Mô tả</label>
						<textarea id="description" name="description" class="form-control"></textarea>
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_image">Ảnh đại diện</label>
						 <div class="input-group">
						   <span class="input-group-btn">
						     <a id="lfm" data-input="seo_image" data-preview="holder" class="btn btn-primary">
						       <i class="fa fa-picture-o"></i> Chọn
						     </a>
						   </span>
						   <input id="seo_image" name="seo_image" class="form-control" type="text">
						 </div>
						<span id="errimg" class="help-block"></span>
						<img id="holder" style="margin-top:15px;max-height:100px;">
					</div>
					<div class="form-group">
						<button type="submit" id="submit" class="btn btn-primary">Đăng</button>
					</div>
				</div>
			</div>
		</form>
		<div class="col-md-8">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Danh sách danh mục</h3>
				</div>
				<div class="box-body">
					<table id="datacate" class="table table-bordered table-striped">
						
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Default bootstrap modal example -->
<div class="modal fade" id="editCate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <form action="{{ route('admin.pro_category.update') }}" class="modal-content" method="post" id="update-cate">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cập nhật danh mục</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			/**
			 * Setup file manager to seo_image
			 */
			$('#lfm').filemanager('image');
			/**
			 * Remove class & noti errors after forcus input & textarea & seo_image
			 */
			
			var CallEvent = function(){

				$('input, textarea').on('focus', function(){
					$(this).parent().removeClass('has-warning');
					$(this).next().text('');
				});
				$('#seo_image, #seo_images').on('focus', function(){
					$(this).parent().parent().removeClass('has-warning');
					$(this).parent().next().text('');
				});

				$('#editCate').on('show.bs.modal', function(e){
					var id = $(e.relatedTarget).data('edit');
					$.ajax({
						url: `{{ route('admin.pro_category.update') }}/${id}`,
						method: 'get',
						processData: false
					}).done(function(result){
						if (!result) {
							swal({
								title: 'Thông Báo!!!',
								text: 'Không tìm thấy danh mục này',
								icon: 'warning'
							}).then(will=>{
								window.location.reload();
							});
							$('#editCate').modal('hide');
						}
						$('.modal-body').html(result);
						table.ajax.reload(CallEvent);
						$('#lffm').filemanager('image');

					});
				});

				$('#update-cate').on('submit', function(c){
					c.preventDefault();
					var formUp = new FormData($(this)[0]);
					formUp.set('_token', '{{csrf_token()}}');
					$.ajax({
						url: '{{ route('admin.pro_category.update') }}',
						type: 'POST',
						processData:false,
						contentType: false,
						data: formUp,
					})
					.done(function(result) {
						if (result.errors) {
							var msg = result.msg;
							if ($.type(msg) == 'string') {
								swal({
									title: "Thông Báo",
									text: result.msg,
									icon: "warning",
									button: "Oke!",
								});
							}else{
								if (msg.hasOwnProperty('seo_desc')) {
									$('#errupdesc').parent().addClass('has-warning');
									$('#errupdesc').text(msg.seo_desc[0]);
								}
								if (msg.hasOwnProperty('nameup')) {
									$('#errupname').parent().addClass('has-warning');
									$('#errupname').text(msg.nameup[0]);
								}
								if (msg.hasOwnProperty('seo_images')) {
									$('#errupimg').parent().addClass('has-warning');
									$('#errupimg').text(msg.seo_images[0]);
								}
								if (msg.hasOwnProperty('slugup')) {
									$('#errupslug').parent().addClass('has-warning');
									$('#errupslug').text(msg.slugup[0]);
								}
							}
						}else{
							swal({
									title: "Thông Báo",
									text: result.msg,
									icon: "success",
									button: "Oke!",
								}).then(will=>{
									$('#editCate').modal('hide');
									table.ajax.reload(CallEvent);
								});
						}
					})
					.fail(function() {
						swal({
							icon: 'warning',
							title: 'Thông Báo!!!',
							text: 'Kết nối bị dám đoạn',
						}).then(will=>{
							window.location.reload();
						});
					});
					
				});
				$('.btn-delete').on('click', function(){
					var id = $(this).attr('data-id');
					swal({
						title: "Bạn có chắc không?",
						text: "Sau khi xóa, bạn sẽ không thể khôi phục tệp tưởng tượng này!",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
					.then((willDelete) => {
						if (willDelete) {
							$.ajax({
								url: '{{ route('admin.pro_category.index') }}',
								method: 'delete',
								data: {
									id: id,
									_token: '{{csrf_token()}}'
								}
							}).done(function(result){
								if (result.errors) {
									swal({
										title: 'Thông Báo!',
										text: ($.type(result.msg) == 'string') ? result.msg : result.msg.id[0],
										icon: 'warning'
									}).then(will=>{
										window.location.reload();
									});
								}else{
									swal({
										title: "Thông Báo!",
										text: result.msg,
										icon: "success"
									}).then(wil=>{
										table.ajax.reload(CallEvent);
									});
								}
							}).fail(function(){
								swal({
									title: "Thông Báo!",
									text: "Quá trình bị dám đoạn",
									icon: "warning"
								}).then(wil=>{
									window.location.reload();
								});
							});
						} else {
							swal("Bạn đã hủy xóa thành công!");
  						}
					});
				});
			};
			let table = $('#datacate').DataTable({
				responsive: true,
				ajax: '{{ route('admin.pro_category.data') }}',
				deferRender: true,
				columnDefs: [
					{
						'targets': [0],
						'searchable': true,
						'orderable': true,
						'className': 'hi'
					},
					{
						'targets': [1],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [2],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [3],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [4],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [5],
						'searchable': true,
						'orderable': true
					}
				],
				columns: [
					{data: 'id', 'title': 'ID', 'autoWidth':false },
					{data: 'name', 'title': 'Tiêu dề', 'autoWidth':false },
					{data: 'seo_desc', 'title': 'Mô tả', 'autoWidth':false },
					{data: 'slug', 'title': 'Đường dẫn tĩnh', 'autoWidth':false },
					{data: 'parent', 'title': 'Danh mục cha', 'autoWidth':false },
					{data: 'idcate', 'title': 'Hành động', 'autoWidth':false, render: function(data){
						var temp = `<a data-edit="${data}" data-remote="false" data-toggle="modal" data-target="#editCate" class="btn btn-primary btn-edit btn-xs"><i class="fa fa-fw fa-edit"></i></a>
										<button data-id="${data}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-remove"></i></button>`;
						return `${temp}`;
					}}
				], 
				rowID: 'id',
				'language': {
					'search': 'Tìm kiếm:',
					'paginate': {
						'first': 'Đầu',
						'last': 'Cuối',
						'next': 'Sau',
						'previous': 'Trước'
					},
					'info': 'Hiển thị _START_ đến _END_ trong _TOTAL_ kết quả',
					'infoEmpty': 'Hiển thị 0 đến 0 trong 0 kết quả',
					'zeroRecords': 'không có dữ liệu trong bảng'
				},
				initComplete: CallEvent
			});
			/**
			 * Submit Form Create Product Categories
			 */
			$('#create-cate').on('submit', function(e){
				e.preventDefault();
				var Formdata = new FormData($(this)[0]);
				Formdata.set('_token', '{{csrf_token()}}');
				$.ajax({
					url: '{{ route('admin.pro_category.index') }}',
					type: 'POST',
					contentType: false,
					processData: false,
					data: Formdata,
				})
				.done(function(result) {
					var msg = result.msg;
					if (result.errors) {
						if (msg.hasOwnProperty('name')) {
							$('#errname').parent().addClass('has-warning');
							$('#errname').text(msg.name[0]);
						}
						if (msg.hasOwnProperty('parent_id')) {
							$('#errparent').parent().addClass('has-warning');
							$('#errparent').text(msg.parent_id[0]);
						}
						if (msg.hasOwnProperty('description')) {
							$('#errdesc').parent().addClass('has-warning');
							$('#errdesc').text(msg.description[0]);
						}
						if (msg.hasOwnProperty('seo_image')) {
							$('#errimg').parent().addClass('has-warning');
							$('#errimg').text(msg.seo_image[0]);
						}
					}else{
						swal({
							title: 'Thông Báo!',
							icon: 'success',
							text: msg,
							button: 'Oke!'
						}).then( willci =>{
							$('input, textarea').val('');
							window.location.reload();
						});
					}
				})
				.fail(function() {
					swal({
							title: 'Thông Báo!',
							icon: 'success',
							text: 'Quá trình bị dám đoạn',
							button: 'Oke!'
						}).then( willci =>{
							window.location.reload();
						});
				});
				
			});
		});
	</script>
@endsection
