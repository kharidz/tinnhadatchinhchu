@extends('layouts.server')

@section('title', 'Danh sách chiến dịch')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">@yield('title')</h3>
                </div>
                <div class="box-body">
                    <table class="table" id="campaign_list"></table>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(function(){
           var table = $('#campaign_list').DataTable({
               serverSide: true,
               processing: true,
               ajax: '{{route('admin.campaign.data_camp')}}',
               columns:[
                   {data: 'id', title: '#ID', autoWidth: true},
                   {data:'name', title: 'Tên chiến dịch'},
                   {data:'subject', title: 'Tiêu đề chiến dịch'},
                   {data: 'created_at', title: 'Ngày tạo', render: function(created_at){
                           let date = moment();
                           return date.locale('vi').utc(created_at).format('LL');
                       }},
                   {data: 'status', title: 'Trạng thái', render: function(status){
                       return (status) ? "Hoàn Tất" : 'Đang sử lý';
                       }},
                   {data: 'id', title: 'Hành động', render: function (data) {
                           let html = `<button data-remove='${data}' class="btn btn-remove btn-primary btn-xs"><i class="fa fa-times"></i></button>`;
                           return html;
                       }}
               ],
               rowID: 'id',
               initComplete: function(){
                   $('#campaign_list').on('click', '.btn-remove', function(){
                       let id = $(this).data('remove');
                       swal({
                           title: "Bạn có chắc không?",
                           text: "Sau khi xóa, bạn sẽ không thể khôi phục tệp tưởng tượng này!",
                           icon: "warning",
                           buttons: true,
                           dangerMode: true,
                       })
                           .then((willDelete) => {
                               if (willDelete) {
                                   $.ajax({
                                        url: `{{route('admin.campaign.delete')}}/${id}`,
                                       method: 'get',
                                   }).done(result=>{
                                      swal({
                                          title: 'Thông báo!!!',
                                          icon: (result.errors) ? 'warning' : 'success',
                                          text: result.msg
                                      }).then( ()=>{
                                          table.ajax.reload();
                                      });
                                   });
                               } else {
                                   swal("Tệp tưởng tượng của bạn an toàn!");
                               }
                           });
                   });
               }
           });
        });
    </script>
@endsection
