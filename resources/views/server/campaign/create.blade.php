@extends('layouts.server')

@section('title', 'Thêm Chiến Dịch')

@section('css')
@endsection

@section('content')
<form class="row" method="post" action="#" id="up_campaign">
    @csrf
    <div class="col-md-7">
        <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">@yield('title')</h3>
        </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Tên chiến dịch</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" name="subject" class="form-control">
                </div>
                <div class="form-group">
                    <textarea name="content" id="content" cols="30" rows="10"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Inbox</h3>

{{--                <div class="box-tools pull-right">--}}
{{--                    <div class="has-feedback">--}}
{{--                        <input type="text" class="form-control input-sm" placeholder="Search Mail">--}}
{{--                        <span class="glyphicon glyphicon-search form-control-feedback"></span>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="email">Email</label>
                    <select id="type" name="type" class="form-control">
                        <option value="1">Danh sách khách hàng ({{$users->count()}})</option>
                        <option value="2">Danh sách theo dõi ({{$subscribes}})</option>
                        <option value="3">Tổng ({{$emails->count()}})</option>
                        <option value="4">Tùy chọn</option>
                    </select>
                </div>
                <div class="table-responsive mailbox-messages" id="custom"></div>
                <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /. box -->
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </div>
</form>
@endsection

@section('js')
    <script src="{{asset('admin/plugin/ckeditor/ckeditor.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(async function(){
            $('input').on('focus', function(){
               $(this).parent().removeClass('has-warning');
            });
            var content = CKEDITOR.replace('content');
            var Callback_mail = function(id = null){
                $.ajax({
                    url: '{{route('admin.campaign.data')}}',
                    method: 'get',
                    dataType: 'json',
                    data: {
                        start: id
                    }
                }).done(result=> {
                    if (!result.length){
                        return false;
                    }
                    result.forEach(item =>{
                       $('#list_email tbody').append(`
                        <tr>
                            <td><input type="checkbox" value="${item.email}" name="email[]"></td>
                            <td class="mailbox-name">${item.email}</td>
                        </tr>
                       `);
                    });
                    // Icheckbox to input checkbox
                    $('#list_email input[type="checkbox"]').iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue'
                    });
                    //Enable check and uncheck all functionality
                    $(".checkbox-toggle").click(function () {
                        var clicks = $(this).data('clicks');
                        if (clicks) {
                            //Uncheck all checkboxes
                            $("#list_email input[type='checkbox']").iCheck("uncheck");
                            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                        } else {
                            //Check all checkboxes
                            $("#list_email input[type='checkbox']").iCheck("check");
                            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                        }
                        $(this).data("clicks", !clicks);
                    });
                });
            }
            $('#type').on('change', function(e){
                let vale = $(this).val();
                let html = `<table class="table table-hover table-striped" id="list_email">
                        <thead>
                        <th>#</th>
                        <th>Email</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="mailbox-controls">
                        <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                        </button>
                        <div class="pull-right">
                        <button type="button" class="btn btn-default btn-sm btn-more" data-id="1"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                        </div>
                        </div>`;
                if (vale == 4){
                    $('#custom').html(html);
                    Callback_mail();
                }else{
                    $('#custom').html('');
                }
            });
            var id = 1;
            $('#custom').on('click', '.btn-more', function(){
                let mail = Callback_mail(id);
                id +=1;
            });

            $('#up_campaign').on('submit', function(e){
                e.preventDefault();
                let formdata = new FormData($(this)[0]);
                formdata.set('content', content.getData());
                $.ajax({
                    url: window.location.href,
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: formdata
                }).done(result =>{
                    let msg = result.msg;
                    if (result.errors){
                        if (msg.hasOwnProperty('name')){
                            $('input[name="name"]').parent().addClass('has-warning');
                        }
                        if (msg.hasOwnProperty('subject')){
                            $('input[name="subject"]').parent().addClass('has-warning');
                        }
                    } else{
                        swal({
                            title: 'Thông báo!!!',
                            icon: 'success',
                            text: msg
                        }).then(()=>{
                            window.location.reload();
                        });
                    }
                });
            });
        });
    </script>
@endsection
