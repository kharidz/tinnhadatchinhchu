@extends('layouts.server')
@section('title', 'Trang Cá Nhân')

@section('css')

@endsection


@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="box box-primary">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="/dist/img/user2-160x160.jpg" alt="">
					<h3 class="profile-username text-center">{{$user->fullname}}</h3>
					<p class="text-muted text-center">
					@if ($user->role > 5)
						Quản Trị
					@else
						Nhân Viên
					@endif
					</p>
					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Chức Vụ:</b>
							<span class="pull-right">
								@if ($user->role > 5)
									Quản Trị
								@else
									Nhân Viên
								@endif
							</span>
						</li>
						<li class="list-group-item">
							<b>Số diện thoại:</b>
							<span class="pull-right">
								{{($user->phone) ? $user->phone: "Không có"}}
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="@if (session()->has('errpass') || session()->has('succpass') || $errors->has('oldpassword')|| $errors->has('newpassword'))
						@else
						active
					@endif"><a data-toggle="tab" href="#update-info">Cập Nhật Thông tin</a></li>
					<li class="@if (session()->has('errpass') || session()->has('succpass') || $errors->has('oldpassword')|| $errors->has('newpassword'))
						active
					@endif"><a data-toggle="tab" href="#update-password">Đổi mật khẩu</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane @if (session()->has('errpass') || session()->has('succpass') || $errors->has('oldpassword')|| $errors->has('newpassword'))
						@else
						active
					@endif" id="update-info">
						<h4 class="box-title">Cập Nhật Thông tin</h4>
						@if (session()->has('errupdate'))
							<div class="alert alert-warning">
								<span>{{session()->get('errupdate')}}</span>
							</div>
						@endif
						@if (session()->has('success'))
							<div class="alert alert-success">
								<span>{{session()->get('success')}}</span>
							</div>
						@endif
						<form action="{{ route('admin.profile.index') }}" method="post">
							@csrf
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group {{$errors->has('fullname') ? "has-warning": null}}">
										<label for="fullname">Họ Tên</label>
										<input id="fullname" name="fullname" type="text" value="{{$user->fullname}}" class="form-control">
										@if ($errors->has('fullname'))
										<span class="help-block">{{$errors->first('fullname')}}</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group {{$errors->has('phone') ? "has-warning": null}}">
										<label for="phone">Số diện thoại</label>
										<input id="phone" name="phone" type="number" value="{{$user->phone}}" class="form-control">
										@if ($errors->has('phone'))
										<span class="help-block">{{$errors->first('phone')}}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="from-group">
								<button type="submit" class="btn btn-primary">Cập nhật</button>
							</div>
						</form>
					</div>
					<div class="tab-pane 
					@if (session()->has('errpass') || session()->has('succpass') || $errors->has('oldpassword')|| $errors->has('newpassword'))
						active
					@endif
					" id="update-password">
					
						<h4 class="box-title">Đổi mật khẩu</h4>
						<form action="{{ route('admin.profile.password') }}" method="post">
							<div class="form-group {{(session()->has('errpass') || $errors->has('oldpassword')) ? "has-warning": null}}">
					@csrf
					@if (session()->has('succpass'))
						<div class="alert alert-success">
							<span>{{session()->get('succpass')}}</span>
						</div>
					@endif
								<label for="oldpassword">Mật khẩu cũ</label>
								<input type="password" name="oldpassword" id="oldpassword" class="form-control">
								@if ($errors->has('oldpassword'))
									<span class="help-block">{{$errors->first('oldpassword')}}</span>
								@endif
								@if (session()->has('errpass'))
									<span class="help-block">{{session()->get('errpass')}}</span>
								@endif
							</div>
							<div class="form-group {{$errors->has('newpassword') ? "has-warning": null}}">
								<label for="oldpassword">Mật khẩu mới</label>
								<input type="password" name="newpassword" id="newpassword" class="form-control">
								@if ($errors->has('newpassword'))
									<span class="help-block">{{$errors->first('newpassword')}}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="newpassword_confirmation">Nhập lại mật khẩu mới</label>
								<input type="password" name="newpassword_confirmation" id="newpassword_confirmation" class="form-control">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Cập nhật</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript">
		$('input').on('focus', function(){
			$(this).parent().removeClass('has-warning');
			$(this).next().text('');
		});
	</script>
@endsection