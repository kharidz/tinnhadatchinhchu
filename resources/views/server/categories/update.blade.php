@extends('layouts.server')
@section('title', 'Cập nhật danh mục bài viết')
@section('css')

@endsection

@section('content')
	<form action="#" method="post" id="upload-form" class="row">
		@csrf
		<div class="col-md-8">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="name">Tên danh mục</label>
						<input type="text" name="name" id="name" value="{{$cate->name}}" class="form-control">
						<span id="errname" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="description">Mô tả danh mục</label>
						<textarea id="description" name="description">{!!$cate->description!!}</textarea>
						<span id="errdesc" class="help-block"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Đăng</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<div class="form-group">
							<label for="slug">Đường dẫn tĩnh</label>
							<input id="slug" name="slug" value="{{$cate->slug}}" class="form-control" type="text">
							Xem thử: <a id="slugview" href="{{ route('client.category', $cate->slug) }}">{{ route('client.category', $cate->slug) }}</a>
							<span id="errslug" class="help-block"></span>
						</div>
						<label for="parent">Danh mục cha</label>
						<select name="parent_id" id="parent_id" class="form-control">
							@if ($cate->parent_id == 0)
								<option value="0">Gốc</option>
							@else
								@foreach ($categories as $categorie)
								@if ($categorie->id == $cate->id)
									@continue
								@endif
									<option @if ($categorie->id == $cate->parent_id)
									selected 
								@endif value="{{$categorie->id}}">{{$categorie->name}}</option>
								@endforeach
							@endif
							
						</select>
					</div>
					<div class="form-group">
						<label for="seo_image">Ảnh đại diện</label>
						 <div class="input-group">
						   <span class="input-group-btn">
						     <a id="lfm" data-input="seo_image" data-preview="holder" class="btn btn-primary">
						       <i class="fa fa-picture-o"></i> Chọn
						     </a>
						   </span>
						   <input id="seo_image" name="seo_image" value="{{$cate->seo_image}}" class="form-control" type="text" >
						 </div>
						<span id="errimg" class="help-block"></span>
						<img id="holder" src="{{ asset($cate->seo_image) }}" style="margin-top:15px;max-height:100px;">
					</div>
					<div class="form-group">
						<label for="seo_desc">Tiêu Đề seo</label>
						<input type="text" class="form-control" id="seo_title" value="{{$cate->seo_title}}" placeholder="Tiêu Đề seo..." name="seo_title" />
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_desc">Mô tả seo</label>
						<textarea class="form-control" id="seo_desc" placeholder="Mô tả seo..." name="seo_desc">{{$cate->seo_desc}}</textarea>
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<button type="submit" id="submit" class="btn btn-primary">Đăng</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('input').focus(function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
				$('#slugview').text(`{{ route('client.category', $cate->slug) }}`);
			});
			$('#seo_image').focus(function(){
				$(this).parent().parent().removeClass('has-warning');
				$(this).parent().next().text('');
			});
			var options = {
			    filebrowserImageBrowseUrl: '/filemanager?type=Images',
			    filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
			    filebrowserBrowseUrl: '/filemanager?type=Files',
			    filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
			  };
			var editor = CKEDITOR.replace('description', options);
			$('#lfm').filemanager('image');

			$('#upload-form').on('submit', function(e){
				e.preventDefault();
				$.ajaxSetup({
					headers:{
						'X-CSRF-TOKEN': $('input[name="_token"]').val()
					}
				});
				var formData = new FormData($(this)[0]);
				formData.set('description', editor.getData());
				$.ajax({
					url: '{{ route('admin.categories.update', $cate->id) }}',
					method: 'post',
					processData: false,
					contentType: false,
					data: formData
				}).done(function(result){
					if (result.error) {
						var msg = result.msg;
						if (msg.hasOwnProperty('name')) {
							$('#errname').parent().addClass('has-warning');
							$('#errname').text(msg.name[0]);
						}
						if (msg.hasOwnProperty('description')) {
							$('#errdesc').text(msg.description[0]);
						}
						if (msg.hasOwnProperty('seo_image')) {
							$('#errimg').parent().addClass('has-warning');
							$('#errimg').text(msg.seo_image[0]);
						}
					}else{
						swal({
							title: "Thông Báo",
							text: result.msg,
							icon: "success",
							button: "Oke!",
						}).then((willDelete) =>{
							window.location.reload();
						});
					}
				});
			});
			$('#slug').on('input', function(){
				$('#slugview').text(`{{ route('client.category') }}/${$(this).val()}`);
			});
		});
	</script>
@endsection