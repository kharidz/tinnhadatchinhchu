@extends('layouts.server')

@section('title', 'Danh mục bài viết')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
				</div>
				<div class="box-body">
					<table id="datacate" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Tên danh mục</th>
								<th>Danh mục cha</th>
								<th>Tên danh mục seo</th>
								<th>Ngày tạo</th>
								<th>Hành động</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($categories as $categorie)
						@if (!$categorie->status)
							@continue
						@endif
							<tr>
								<td>{{$categorie->name}}</td>
								<td>{{$categorie->parent()}}</td>
								<td>{{$categorie->seo_title}}</td>
								<td>{{$categorie->created_at}}</td>
								<td>
										<a href="{{ route('client.category', $categorie->slug) }}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-eye"></i></a>
										<a href="{{ route('admin.categories.update', $categorie->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-edit"></i></a>
										<button data-id="{{$categorie->id}}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-remove"></i></button>
									</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

	<script type="text/javascript">
		$(function(){
			$('#datacate').DataTable({
				'language': {
					'search': 'Tìm kiếm:',
					'paginate': {
						'first': 'Đầu',
						'last': 'Cuối',
						'next': 'Sau',
						'previous': 'Trước'
					},
					'info': 'Hiển thị _START_ đến _END_ trong _TOTAL_ kết quả',
					'infoEmpty': 'Hiển thị 0 đến 0 trong 0 kết quả',
					'zeroRecords': 'không có dữ liệu trong bảng'
				}
			});

			$('.btn-delete').on('click',function(){
				var id = $(this).attr('data-id');
				swal({
					title: "Bạn có chắc không?",
					text: "Sau khi xóa, bạn sẽ không thể khôi phục tệp tưởng tượng này!",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						$.ajax({
							url: '{{ route('admin.categories.index') }}',
							type: 'DELETE',
							data: {
								id: id,
								_token: '{{ csrf_token() }}'
							},
						})
						.done(function(result) {
							if (!result.errors) {
								swal({
									title: 'Thông báo!',
									icon: 'success',
									text: 'Xóa danh mục thành công'
								}).then((willDone) =>{
									window.location.reload();
								});
							}else{
								swal({
									title: 'Thông báo!',
									icon: 'error',
									text: `${($.type(result.msg) =='string') ? result.msg : result.msg.id[0]}`
								});
							}
						})
						.fail(function() {
							swal({
								text: "Quá trình xóa bị dám đoạn",
								icon: "warning"
							});
						});
						
					} else {
						swal("Bạn đã hủy xóa thành công!");
					}
				});
			});
		});
	</script>
@endsection