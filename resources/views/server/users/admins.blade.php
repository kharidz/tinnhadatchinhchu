@extends('layouts.server')
@section('title', 'Quản lý tài khoản quản trị ')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
					<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#themuser">Thêm Tài Khoản</button>
				</div>
				<div class="box-body">
					<table id="listusers" class="table table-bordered table-hover">
						
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="themuser">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Thêm tài khoản quản trị</h4>
          </div>
          <div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Họ tên</label>
							<input type="text" name="name" id="name" class="form-control">
							<span id="errname" class="help-block"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Số điện thoại</label>
							<input type="number" name="phone" id="phone" placeholder="03869574258" class="form-control">
							<span id="errsdt" class="help-block"></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" id="email" class="form-control">
					<span id="errmail" class="help-block"></span>
				</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" id="create-user" class="btn btn-primary">Thêm tài khoản</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			var CallBack = function(){
				$('.btn-delete').on('click', function(){
					let id = $(this).attr('data-id');
					$.ajax({
						url: '{{ route('admin.admin.index') }}',
						type: 'DELETE',
						data: {id: id, _token: '{{csrf_token()}}'},
					})
					.done(function(result) {
						let msg = result.msg;
						if (result.errors) {
							swal({
								title: "Thông Báo!!!",
								icon: "warning",
								text: msg
							}).then(function(){
								table.ajax.reload(CallBack)
							});
						}else{
							swal({
								title: "Thông Báo!!!",
								icon: "success",
								text: msg
							}).then(function(){
								table.ajax.reload(CallBack)
							});
						}
					})
					.fail(function() {
						swal({
								title: "Thông Báo!!!",
								icon: "warning",
								text: 'Kết nối bị dám đoạn'
							}).then(function(){
								window.location.reload();
							});
					})
					.always(function() {
						console.log("complete");
					});
					
				});
			};

			var table = $('#listusers').DataTable({
				responsive: true,
				ajax: '{{ route('admin.admin.data') }}',
				deferRender: true,
				columnsDef: [
					{
						'targets': [0],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [1],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [2],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [3],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [4],
						'searchable': true,
						'orderable': true
					},
					{
						'targets': [5],
						'searchable': false,
						'orderable': false
					}
				],
				columns:[
					{data: 'username', title: 'User Name', autoWidth: false},
					{data: 'email', title: 'Email', autoWidth: false},
					{data: 'fullname', title: 'Họ Tên', autoWidth: false},
					{data: 'phone', title: 'Số Điện Thoại', autoWidth: false},
					{data: 'status', title: 'Trạng Thái', autoWidth: false, render:function(status){
						return (status > 5) ? "Quản trị" : "Nhân viên";
					}},
					{data: 'iduser', 'title': 'Hành động', 'autoWidth':false, render: function(data){
						var temp = `<button data-id="${data}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-remove"></i></button>`;
						return `${temp}`;
					}}
				],
				rowID: 'id',
				initComplete: CallBack
			});
			$('input').on('focus', function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
			});
			$('#create-user').on('click', function(){
				$(this).html('<i class="fa fa-spin fa-spinner"></i> Đang Khởi Tạo ');
				$(this).attr('disabled', true);
				let name = $('#name').val();
				let email = $('#email').val();
				let phone = $('#phone').val();
				(!name) ? $('#name').parent().addClass('has-warning'): null;
				(!email) ? $('#email').parent().addClass('has-warning'): null;
				(!phone) ? $('#phone').parent().addClass('has-warning'): null;
				$.ajax({
					url: '{{ route('admin.admin.create') }}',
					type: 'POST',
					data: {
						name: name,
						email: email,
						phone: phone,
						_token: '{{csrf_token()}}'
					},
				})
				.done(function(result) {
					let msg = result.msg;
					$('#create-user').html('Thêm tài khoản');
					$('#create-user').attr('disabled', false);
					if (result.errors) {
						if (msg.hasOwnProperty('name')) {
							$('#errname').parent().addClass('has-warning');
							$('#errname').text(msg.name[0]);
						}
						if (msg.hasOwnProperty('email')) {
							$('#errmail').parent().addClass('has-warning');
							$('#errmail').text(msg.email[0]);
						}
						if (msg.hasOwnProperty('phone')) {
							$('#errsdt').parent().addClass('has-warning');
							$('#errsdt').text(msg.phone[0]);
						}
					}else{
						swal({
							title: 'Thông Báo!!!',
							text: msg,
							icon: 'success'
						}).then(()=>{
							$('#themuser').modal('hide');
							table.ajax.reload(CallBack);
						});
					}
				})
				.fail(function() {
					swal({
						title: "Thông báo!!!",
						icon: 'warning',
						text: "Kết nối bị giám đoạn"
					}).then(()=>{
						window.location.reload();
					});
				});
				
			});
		});
	</script>
@endsection
