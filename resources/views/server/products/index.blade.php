@extends('layouts.server')

@section('title', 'Danh sách sản phẩm')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@yield('title')</h3>
                </div>
                <dib class="box-body">
                    <table id="listproduct" class="table table-bordered table-hover">

                    </table>
                </dib>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        var Callback = function(){
            $('.btn-delete').on('click', function(){
                let id = $(this).attr('data-id');
                $.ajax({
                    url: '{{ route('admin.product.delete') }}',
                    type: 'DELETE',
                    data: {
                        id: id,
                        _token: '{{csrf_token()}}'
                    },
                })
                    .done(function(result) {
                        if (result.errors) {
                            swal({
                                title: "Thông Báo!!!",
                                text: result.msg,
                                icon: 'warning'
                            }).then(()=>{
                                table.ajax.reload(Callback)
                            });
                        }else{
                            swal({
                                title: 'Thông Báo!!!',
                                icon: 'success',
                                text: result.msg
                            }).then(()=>{
                                table.ajax.reload(Callback)
                            });
                        }
                    })
                    .fail(function() {
                        swal({
                            title: "Thông Báo!!!",
                            text: "Kết nối bị dám đoạn"
                        }).then(()=>{
                            window.location.reload();
                        });
                    });
            });

            $('.btn-edit').on('click', function(){
                let id = $(this).attr('data-edit');
                console.log(id);
                $(location).attr('href', '{{ route('admin.product.update') }}/'+id);
            });
        };
        var table = $('#listproduct').DataTable({
            serverSide: true,
            ajax: '{{ route('admin.product.data') }}',
            processing: true,
            deferRender: true,
            responsive: true,
            "order": [[ 1, "desc" ]],
            columnDefs: [
                {
                    'targets': [0],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [1],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [2],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [3],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [4],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [5],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [6],
                    'searchable': true,
                    'orderable': true
                },
                {
                    'targets': [7],
                    'searchable': true,
                    'orderable': true
                }
            ],
            columns:[
                {data: 'id', 'title': 'ID', 'autoWidth':false },
                {data: 'name', 'title': 'Tên sản phẩm', 'autoWidth':false },
                {data: 'address', 'title': 'Địa chỉ', 'autoWidth':false },
                {data: 'area', 'title': 'Diện tích: m²', 'autoWidth':false },
                {data: 'price', 'title': 'Giá tiền: VNĐ', 'autoWidth':false },
                {data: 'contact_phone', 'title': 'Liên hệ', 'autoWidth':false },
                {data: 'cate', 'title': 'Danh mục', 'autoWidth':false },
                {data: 'idpro', 'title': 'Hành động', 'autoWidth':false, render: function(data){
                        var temp = `<a data-edit="${data}" data-remote="false" data-toggle="modal" data-target="#editCate" class="btn btn-primary btn-edit btn-xs"><i class="fa fa-fw fa-edit"></i></a>
										<button data-id="${data}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-remove"></i></button>`;
                        return `${temp}`;
                    }}
            ],
            rowID: 'id',
            'language': {
                'search': 'Tìm kiếm:',
                'paginate': {
                    'first': 'Đầu',
                    'last': 'Cuối',
                    'next': 'Sau',
                    'previous': 'Trước'
                },
                'info': 'Hiển thị _START_ đến _END_ trong _TOTAL_ kết quả',
                'infoEmpty': 'Hiển thị 0 đến 0 trong 0 kết quả',
                'zeroRecords': 'không có dữ liệu trong bảng',
                'processing': "Đang tải dữ liệu"
            },
            initComplete: Callback
        });
    </script>
@endsection
