@extends('layouts.server')

@section('title', 'Thêm sản phẩm')

@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
	<form class="row" action="#" id="upload-product">
		<div class="col-md-8">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="name">Tiêu đề sản phẩm</label>
						<input type="text" name="name" id="name" class="form-control">
						<span id="errname" class="help-block"></span>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="province_id">Tỉnh / Thành Phố</label>
								<select id="province_id" name="province_id" class="form-control">
									@foreach ($provinces as $province)
										<option value="{{$province->code}}">{{$province->type}} {{$province->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="district_id">Quận / Huyện</label>
								<select id="district_id" name="district_id" class="form-control">
									@foreach ($districts as $district)
										<option value="{{$district->code}}">{{$district->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Địa chỉ</label>
								<div class="input-group mb-4">
								<input type="text" id="address" class="form-control" placeholder="Số nhà ..." aria-label="" aria-describedby="basic-addon1">
								<div class="input-group-append" style="display: table-cell">
									<select id="ward_id" name="ward_id" class="form-control">
										@foreach ($wards as $ward)
											<option value="{{$ward->code}}">{{$ward->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="content">Nội dung sản phẩm</label>
						<textarea id="content" name="content" rows="3"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Đăng</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="price">Giá</label>
								<div class="input-group">
									<input type="number" name="price" id="price" step="any" class="form-control">
									<span class="input-group-addon">VNĐ</span>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="price">Diện tích</label>
								<div class="input-group">
									<input type="number" name="area" id="area" step="any" class="form-control">
									<span class="input-group-addon">m²</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="contact_phone">Số điện thoại</label>
						<input type="text" name="contact_phone" id="contact_phone" placeholder="0868085264 hoặc 0868085264|0865294654" class="form-control">
					</div>
					<div class="form-group">
						<label for="cate_id">Danh mục</label>
						<select id="cate_id" name="cate_id[]" multiple class="form-control select2">
							@foreach ($cates as $parent)
								@if ($parent->childs()->count() > 0)
									<option value="{{$parent->id}}">{{$parent->name}}</option>
									@foreach ($parent->childs as $childs)
										<option value="{{$childs->id}}">--{{$childs->name}}</option>
									@endforeach
								@elseif($parent->parent_id == 0)
									<option value="{{$parent->id}}">{{$parent->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Đăng</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('input, textarea, select').on('focus', function(){
				$(this).parent().removeClass('has-warning');
			});
			$('.select2').select2();
			/**
			 * Setup Ckeditor
			 */
			var options = {
				filebrowserImageBrowseUrl: '/filemanager?type=Images',
				filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
				filebrowserBrowseUrl: '/filemanager?type=Files',
				filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
			};
			var editor = CKEDITOR.replace('content', options);

			$('#province_id').on('change', function(e){
				var province_id = $(this).children("option:selected").val();
				// console.log(province_id);
				$.ajax({
					url: '{{ route('district') }}',
					method: 'post',
					data: {
						province_id: province_id,
						_token: '{{csrf_token()}}'
					}
				}).done(function(result){
					let temp = '';
					$.each(result, function(k, val){
						var code = val.code;
						var name = val.name;
						temp += `<option value="${code}">${name}</option>`;
					});
					$('#ward_id').html('<option>Trống rỗng</option>');
					$('#district_id').html(temp);
				});
			});

			$('#district_id').on('change', function(e){
				var district_code = $(this).children("option:selected").val();
				$.ajax({
					url: '{{ route('ward') }}',
					method: 'post',
					data: {
						district_id: district_code,
						_token: '{{csrf_token()}}'
					}
				}).done(function(result){
					let temp = '';
					$.each(result, function(k, val){
						var code = val.code;
						var name = val.name;
						temp += `<option value="${code}">${name}</option>`;
					});
					$('#ward_id').html(temp);
				});
			});

			$('#upload-product').on('submit', function(e){
				e.preventDefault();
				var Formdata = new FormData($(this)[0]);
				Formdata.set('content', editor.getData());
				Formdata.set('_token', '{{csrf_token()}}');
				var addr = $('#address').val();
				var ward_id = $('#ward_id').children("option:selected").text();
				ward_id = `${addr} ${ward_id}`;
				Formdata.set('ward_id', ward_id);
				$.ajax({
					url: '{{ route('admin.product.create') }}',
					method: 'post',
					processData: false,
					contentType: false,
					data: Formdata
				}).done( result=>{
					var msg = result.msg;
					if (result.errors) {
						(msg.hasOwnProperty('name')) ? $('#name').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('province_id')) ? $('#province_id').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('price')) ? $('#price').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('area')) ? $('#area').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('contact_phone')) ? $('#contact_phone').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('cate_id')) ? $('#cate_id').parent().addClass('has-warning'): null;
						(msg.hasOwnProperty('ward_id')) ? $('#ward_id').parent().addClass('has-warning'): null;
					}else{
						swal({
							title: 'Thông báo!!',
							text: msg,
							icon: 'success'
						}).then(function(){
							window.location.reload();
						});
					}
				});
			});
		});
	</script>
@endsection