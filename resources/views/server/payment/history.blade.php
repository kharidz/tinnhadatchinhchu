@extends('layouts.server')

@section('title','Lịch sử nạp tiền')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Lịch sử nạp tiền</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered table-hover" id="table-history">
						<thead>
							<tr>
								<th>Id</th>
								<th>Email</th>
								<th>Số tiền</th>
								<th>Nội dung chuyển khoản</th>
							</tr>
						</thead>
						<tbody>
							@foreach($historys as $k => $history)
								<tr>
									<td>{{$k+1}}</td>
									<td>{{$history->getmail()}}</td>
									<td>{{number_format($history->amount)}} VNĐ</td>
									<td>{{$history->content}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection


@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		$('#table-history').DataTable({
			'language': {
				'search': 'Tìm kiếm:',
				'paginate': {
					'first': 'Đầu',
					'last': 'Cuối',
					'next': 'Sau',
					'previous': 'Trước'
				},
				'info': 'Hiển thị _START_ đến _END_ trong _TOTAL_ kết quả',
				'infoEmpty': 'Hiển thị 0 đến 0 trong 0 kết quả',
				'zeroRecords': 'không có dữ liệu trong bảng'
			},
		});
	</script>
@endsection