@extends('layouts.server')
@section('title', 'Cập nhật bài đăng')
@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
	<form class="row" id="upload-form">
		@csrf
		
		<div class="col-md-8">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="title">Tiêu đề bài viết</label>
						<input type="text" name="name" id="name" class="form-control">
						<span id="errtitle" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="content"></label>
						<textarea id="content" name="content" rows="3"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Đăng</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="catepost">Danh mục</label>
						<select name="catepost[]" id="catepost" class="form-control select2-multi" multiple>
							@foreach ($categories as $categorie)
								@if (!$categorie->status)
									@continue
								@endif
								<option value="{{$categorie->id}}">{{$categorie->name}}</option>
							@endforeach
						</select>
						<span id="errcate" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_image">Ảnh đại diện</label>
						 <div class="input-group">
						   <span class="input-group-btn">
						     <a id="lfm" data-input="seo_image" data-preview="holder" class="btn btn-primary">
						       <i class="fa fa-picture-o"></i> Choose
						     </a>
						   </span>
						   <input id="seo_image" name="seo_image" class="form-control" type="text">
						 </div>
						<span id="errimg" class="help-block"></span>
						<img id="holder" style="margin-top:15px;max-height:100px;">
					</div>
					<div class="form-group">
						<label for="seo_desc">Mô tả ngắn</label>
						<textarea class="form-control" id="excerpt" placeholder="Mô tả seo..." name="excerpt"></textarea>
						<span id="errexcerpt" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_desc">Tiêu Đề seo</label>
						<input type="text" class="form-control" id="seo_title" placeholder="Tiêu Đề seo..." name="seo_title" />
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_desc">Mô tả seo</label>
						<textarea class="form-control" id="seo_desc" placeholder="Mô tả seo..." name="seo_desc"></textarea>
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<button type="submit" id="submit" class="btn btn-primary">Đăng</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection
@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			/**
			 * Setup Ckeditor
			 */
			var options = {
				filebrowserImageBrowseUrl: '/filemanager?type=Images',
				filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
				filebrowserBrowseUrl: '/filemanager?type=Files',
				filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
			};
			var editor = CKEDITOR.replace('content', options);

			$('input').focus(function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
			});
			$('#seo_image').focus(function(){
				$(this).parent().parent().removeClass('has-warning');
				$(this).parent().next().text('');
			});
			$('textarea').focus(function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
			});
			$('#lfm').filemanager('image');

			$('.select2-multi').select2();


			$('#upload-form').on('submit', function(e){
				e.preventDefault();
				$.ajaxSetup({
					headers:{
						'X-CSRF-TOKEN': $('input[name="_token"]').val()
					}
				});

				var Formdata = new FormData($(this)[0]);
				Formdata.set('content', editor.getData());
				$.ajax({
					url: '{{ route('admin.post.create') }}',
					method: 'post',
					processData: false,
					contentType: false,
					data: Formdata
				}).done(function(result){
					if (result.error) {
						var msg = result.msg;
						if ($.type(msg) == 'string') {
							swal({
								title: "Thông Báo",
								text: result.msg,
								icon: "warning",
								button: "Oke!",
							});
						}else{
							if (msg.hasOwnProperty('seo_desc')) {
								$('#errdesc').parent().addClass('has-warning');
								$('#errdesc').text(msg.seo_desc[0]);
							}
							if (msg.hasOwnProperty('name')) {
								$('#errtitle').parent().addClass('has-warning');
								$('#errtitle').text(msg.name[0]);
							}
							if (msg.hasOwnProperty('content')) {
								$('#errcont').parent().addClass('has-warning');
								$('#errcont').text(msg.content[0]);
							}
							if (msg.hasOwnProperty('seo_image')) {
								$('#errimg').parent().addClass('has-warning');
								$('#errimg').text(msg.seo_image[0]);
							}
							if (msg.hasOwnProperty('excerpt')) {
								$('#errexcerpt').parent().addClass('has-warning');
								$('#errexcerpt').text(msg.excerpt[0]);
							}
						}
					}else{
						swal({
							title: "Thông Báo",
							text: result.msg,
							icon: "success",
							button: "Oke!",
						}).then((willDelete) =>{
							window.location.reload();
						});
					}
				});
			});
		});
	</script>
@endsection