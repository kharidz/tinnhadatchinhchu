@extends('layouts.server')
@section('title', 'Danh sách trang')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">@yield('title')</h3>
				</div>
				<div class="box-body">
					<table id="datapage" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Tiêu đề</th>
								<th>Trạng thái</th>
								<th>Tiêu đề seo</th>
								<th>Thời gian</th>
								<th>Hàng động</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($listpage as $k => $page)
								<tr>
									<td>{{$k+1}}</td>
									<td>{{$page->name}}</td>
									<td>@if ($page->status)
										Đăng
										@else
										Nháp
									@endif</td>
									<td>{{$page->seo_title}}</td>
									<td>{{$page->created_at}}</td>
									<td>
										<a href="{{ route('client.page', $page->slug) }}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-eye"></i></a>
										<a href="{{ route('admin.page.update', $page->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-edit"></i></a>
										<button data-id="{{$page->id}}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-remove"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('#datapage').DataTable({
				'language': {
					'search': 'Tìm kiếm:',
					'paginate': {
						'first': 'Đầu',
						'last': 'Cuối',
						'next': 'Sau',
						'previous': 'Trước'
					},
					'info': 'Hiển thị _START_ đến _END_ trong _TOTAL_ kết quả',
					'infoEmpty': 'Hiển thị 0 đến 0 trong 0 kết quả',
					'zeroRecords': 'không có dữ liệu trong bảng'
				}
			});

			$('.btn-delete').on('click',function(){
				var id = $(this).attr('data-id');
				swal({
					title: "Bạn có chắc không?",
					text: "Sau khi xóa, bạn sẽ không thể khôi phục tệp tưởng tượng này!",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						$.ajax({
							url: '{{ route('admin.page.index') }}',
							type: 'DELETE',
							data: {
								id: id,
								_token: '{{ csrf_token() }}'
							},
						})
						.done(function(result) {
							if (!result.errors) {
								swal({
									title: 'Thông báo!',
									icon: 'success',
									text: 'Xóa trang thành công'
								}).then((willDone) =>{
									window.location.reload();
								});
							}else{
								swal({
									title: 'Thông báo!',
									icon: 'error',
									text: `${($.type(result.msg) =='string') ? result.msg : result.msg.id[0]}`
								}).then((willDone) =>{
									window.location.reload();
								});
							}
						})
						.fail(function() {
							swal({
								text: "Quá trình xóa bị dám đoạn",
								icon: "warning"
							});
						});
						
					} else {
						swal("Bạn đã hủy xóa thành công!");
					}
				});
			});
		});
	</script>
@endsection