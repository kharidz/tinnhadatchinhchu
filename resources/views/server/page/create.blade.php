@extends('layouts.server')
@section('title', 'Thêm trang mới')
@section('css')

@endsection
@section('content')
	<form action="{{ route('admin.page.create') }}" id="upload-form" method="post" class="row" enctype="multipart/form-data">
		@csrf
		{{-- Start Editor --}}
		<div class="col-md-8">
			<div class="box">
				<div class="box-body pad">
					<div class="form-group">
						<label for="title">Tiêu Đề</label>
						<input type="text" id="title" name="title" placeholder="Tiêu đề bài viết" class="form-control">
						<span id="errtitle" class="help-block"></span>
					</div>
					<label for="content">Nội Dung Bài Viết</label>
					<textarea class="textarea" id="content" name="content" placeholder="Place some text here"></textarea>
					<span id="errcont" class="help-block"></span>
				</div>
			</div>	
		</div>
		{{-- End Editor --}}
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Đăng</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="seo_image">Ảnh đại diện</label>
						 <div class="input-group">
						   <span class="input-group-btn">
						     <a id="lfm" data-input="seo_image" data-preview="holder" class="btn btn-primary">
						       <i class="fa fa-picture-o"></i> Choose
						     </a>
						   </span>
						   <input id="seo_image" name="seo_image" class="form-control" type="text">
						 </div>
						<span id="errimg" class="help-block"></span>
						<img id="holder" style="margin-top:15px;max-height:100px;">
					</div>
					<div class="form-group">
						<label for="seo_desc">Tiêu Đề seo</label>
						<input type="text" class="form-control" id="seo_title" placeholder="Tiêu Đề seo..." name="seo_title" />
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="seo_desc">Mô tả seo</label>
						<textarea class="form-control" id="seo_desc" placeholder="Mô tả seo..." name="seo_desc"></textarea>
						<span id="errdesc" class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="status">Trạng thái</label>
						<select name="status" id="status" class="form-control">
							<option value="1">Đăng</option>
							<option value="0">Nháp</option>
						</select>
					</div>
					<div class="form-group">
						<button type="submit" id="submit" class="btn btn-primary">Đăng</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			var options = {
			    filebrowserImageBrowseUrl: '/filemanager?type=Images',
			    filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
			    filebrowserBrowseUrl: '/filemanager?type=Files',
			    filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
			  };
			var editor = CKEDITOR.replace('content', options);

			$('#lfm').filemanager('image');

			$('input').focus(function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
			});
			$('#seo_image').focus(function(){
				$(this).parent().parent().removeClass('has-warning');
				$(this).parent().next().text('');
			});
			$('textarea').focus(function(){
				$(this).parent().removeClass('has-warning');
				$(this).next().text('');
			});
			$('#upload-form').on('submit', function(e){
				e.preventDefault();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('input[name="_token"]').val()
					}
				});
				var formData = new FormData($(this)[0]);
				formData.set('content', editor.getData());
				$.ajax({
					url: '{{ route('admin.page.create') }}',
					method: 'post',
					processData: false,
					contentType: false,
					data: formData
				}).done(function(result){
					if (result.errors) {
						var msg = result.msg;
						if ($.type(msg) == 'string') {
							swal({
								title: "Thông Báo",
								text: result.msg,
								icon: "warning",
								button: "Oke!",
							});
						}else{
							if (msg.hasOwnProperty('seo_desc')) {
								$('#errdesc').parent().addClass('has-warning');
								$('#errdesc').text(msg.seo_desc[0]);
							}
							if (msg.hasOwnProperty('title')) {
								$('#errtitle').parent().addClass('has-warning');
								$('#errtitle').text(msg.title[0]);
							}
							if (msg.hasOwnProperty('content')) {
								$('#errcont').parent().addClass('has-warning');
								$('#errcont').text(msg.content[0]);
							}
							if (msg.hasOwnProperty('seo_image')) {
								$('#errimg').parent().addClass('has-warning');
								$('#errimg').text(msg.seo_image[0]);
							}
						}
					}else{
						swal({
							title: "Thông Báo",
							text: result.msg,
							icon: "success",
							button: "Oke!",
						}).then((willDelete) =>{
							window.location.reload();
						});
					}
				});
			});
		});
	</script>
@endsection