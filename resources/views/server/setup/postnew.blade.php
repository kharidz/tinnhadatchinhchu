@extends('layouts.server')

@section('title', 'Cài đặt Banner Trang ')

@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugin/dropify/dist/css/dropify.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@yield('title')</h3>
                </div>
                <div class="box-body">
                    @if(session()->has('success'))
                        <div class="alert alert-success">{{session()->get('success')}}</div>
                    @endif
                    <form action="#" method="post" class="row">
                        @csrf
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="new_title">Tiêu đề Tin Mới</label>
                                <input type="text" name="new_title" value="{{$new_title->opt_value}}" id="new_title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="new_content">Nội dung Tin Mới</label>
                                <textarea class="form-control" name="new_content" id="new_content" cols="30" rows="10">{{$new_content->opt_value}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="seo_image">{{$new_image->opt_title}}</label>
                                <div class="input-group">
                                           <span class="input-group-btn">
                                             <a id="lfm" data-input="new_image" data-preview="holder" class="btn btn-primary">
                                               <i class="fa fa-picture-o"></i> Choose
                                             </a>
                                           </span>
                                    <input id="new_image" name="new_image" value="{{$new_image->opt_value}}" class="form-control" type="text">
                                </div>
                                <img id="holder" src="{{$new_image->opt_value}}" style="margin-top:15px;max-height:100px;">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(function(){
            $('#lfm').filemanager('image');
            var options = {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/filemanager?type=Files',
                filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
            };
            var editor = CKEDITOR.replace('new_content', options);
        });

    </script>
@endsection
