@extends('layouts.server')

@section('title', 'Cài đặt Banner Trang ')

@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugin/dropify/dist/css/dropify.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Thêm Banner</h3>
                </div>
                <form action="#" method="post" class="box-body" id="upload-form">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Tiêu đề</label>
                                <input type="text" name="title" id="title" class="form-control">
                                <span class="help-block" id="error_title"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="seo_image">Ảnh</label>
                                <div class="input-group">
                                           <span class="input-group-btn">
                                             <a id="lfm" data-input="images" data-preview="holder" class="btn btn-primary">
                                               <i class="fa fa-picture-o"></i> Choose
                                             </a>
                                           </span>
                                    <input id="images" name="images" class="form-control" type="text">
                                    <span class="help-block" id="error_image"></span>
                                </div>
                                <img id="holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content">Noi Dung</label>
                        <textarea name="content" id="content" class="form-control" cols="20" rows="5"></textarea>
                        <span class="help-block" id="error_content"></span>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Thêm</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-7">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Banner</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Ảnh</th>
                                <th>Tiêu đề</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>

                        <tbody id="load-data">
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="editbanner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" class="modal-content" method="post" id="update-banner">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cập nhật danh mục</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(function(){
            var options = {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/filemanager?type=Files',
                filebrowserUploadUrl: '/filemanager/upload?type=Files&_token=',

            };
            // var ed_content = CKEDITOR.replace('ed_content', options);
            var content = CKEDITOR.replace('content', options);
            content.config.extraPlugins = 'font';
            $('#lfm').filemanager('image');
            var Callback = async function(){
                await $.ajax({
                    url: '{{route('admin.setup.banner.data')}}',
                    method: 'get'
                }).done(function(result){
                    var temp = '';
                    result.forEach(function(item, k){
                        temp += `
                        <tr>
                            <td><img width="100px" src="${item.images}" alt=""></td>
                            <td>${item.title}</td>
                            <td>
                                <button data-edit="${item.idbanner}" data-remote="false" data-toggle="modal" data-target="#editbanner" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-edit"></i></button>
                                <button data-id="${item.idbanner}" class="btn btn-primary btn-xs btn-delete" id="btn-delete"><i class="fa fa-fw fa-remove"></i></button></td>
                        </tr>
                        `;
                    });
                    $('#load-data').html(temp);
                });
                $('.btn-delete').on('click', function(){
                    var id = $(this).attr('data-id');
                    swal({
                        title: "Bạn có chắc không?",
                        text: "Sau khi xóa, bạn sẽ không thể khôi phục tệp tưởng tượng này!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: `{{ route('admin.setup.banner.delete') }}/${id}`,
                                    method: 'get'
                                }).done(function(res){
                                    swal({
                                        title: 'Thông Báo!',
                                        text: res.msg,
                                        icon: 'success'
                                    }).then(()=>{
                                        Callback();
                                    });
                                });
                            } else {
                                swal("Your imaginary file is safe!");
                            }
                        });
                });
            };
            Callback();

            $('#editbanner').on('show.bs.modal', function(e){
                var id = $(e.relatedTarget).data('edit');
                $.ajax({
                    url: `{{ route('admin.setup.banner.modal') }}/${id}`,
                    method: 'get',
                    processData: false
                }).done(function(result){
                    if (!result) {
                        swal({
                            title: 'Thông Báo!!!',
                            text: 'Không tìm thấy danh mục này',
                            icon: 'warning'
                        }).then(will=>{
                            window.location.reload();
                        });
                        $('#editbanner').modal('hide');
                    }
                    $('.modal-body').html(result);
                    $('#lffm').filemanager('image');
                    var ed_content = CKEDITOR.replace('ed_content', options);
                    ed_content.config.extraPlugins = 'font';
                    $('#update-banner').on('submit', function(e){
                        var id = $('#ed-id').val();
                        e.preventDefault();
                        $.ajaxSetup({
                            headers:{
                                'X-CSRF-TOKEN': $('input[name="_token"]').val()
                            }
                        });

                        var Formdata = new FormData($(this)[0]);
                        Formdata.set('ed_content', ed_content.getData());
                        $.ajax({
                            url: `{{ route('admin.setup.banner.modal') }}/${id}`,
                            method: 'post',
                            processData: false,
                            contentType: false,
                            data: Formdata
                        }).done(function(result){
                            if (result.error) {
                                var msg = result.msg;
                                if ($.type(msg) == 'string') {
                                    swal({
                                        title: "Thông Báo",
                                        text: result.msg,
                                        icon: "warning",
                                        button: "Oke!",
                                    });
                                }else{
                                    if (msg.hasOwnProperty('ed_title')) {
                                        $('#errtitle').parent().addClass('has-warning');
                                        $('#errtitle').text(msg.ed_title[0]);
                                    }
                                    if (msg.hasOwnProperty('ed_image')) {
                                        $('#errupimg').parent().addClass('has-warning');
                                        $('#errupimg').text(msg.ed_image[0]);
                                    }
                                    if (msg.hasOwnProperty('ed_content')) {
                                        $('#errcont').parent().addClass('has-warning');
                                        $('#errcont').text(msg.ed_content[0]);
                                    }
                                }
                            }else{
                                swal({
                                    title: "Thông Báo",
                                    text: result.msg,
                                    icon: "success",
                                    button: "Oke!",
                                }).then((willDelete) =>{
                                    window.location.reload();
                                });
                            }
                        });
                    });
                });
            });

            $('#upload-form').on('submit', function(e){
                e.preventDefault();
                var Formdata = new FormData($(this)[0]);
                Formdata.set('content', content.getData());
                $.ajax({
                    url: '{{route('admin.setup.banner')}}',
                    method: 'post',
                    processData: false,
                    contentType: false,
                    data: Formdata
                }).done(function (result) {
                    var msg = result.msg;
                    if (!result.errors){
                        swal({
                            title: 'Thông báo!',
                            text: msg,
                            icon: 'success'
                        }).then(()=>{
                            window.location.reload();
                        });
                    }else{
                        if(msg.hasOwnProperty('title')){
                            $('#error_title').parent().addClass('has-warning');
                            $('#error_title').text(msg.title[0]);
                        }
                        if(msg.hasOwnProperty('images')){
                            $('#error_image').parent().addClass('has-warning');
                            $('#error_image').text(msg.images[0]);
                        }
                        if(msg.hasOwnProperty('content')){
                            $('#error_content').parent().addClass('has-warning');
                            $('#error_content').text(msg.content[0]);
                        }
                    }
                });
            });
        });

    </script>
@endsection
