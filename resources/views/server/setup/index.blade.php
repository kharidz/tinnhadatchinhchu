@extends('layouts.server')

@section('title', 'Cài đặt trang')

@section('css')
@endsection

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">@yield('title')</h3>
                    </div>
                    <div class="box-body">
                       @if(session()->has('success'))
                           <div class="alert alert-success">{{session()->get('success')}}</div>
                        @endif
                        <form action="#" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group @if($errors->has('title'))
                                        has-warning
                                    @endif">
                                        <label for="">{{$title->opt_title}}</label>
                                        <input type="text" name="title" id="title" value="{{$title->opt_value}}" class="form-control">
                                        @if($errors->has('title'))
                                            <span class="help-block">{{$errors->first('title')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group @if($errors->has('address'))
                                        has-warning
                                    @endif">
                                        <label for="">{{$address->opt_title}}</label>
                                        <input type="text" name="address" id="address" value="{{$address->opt_value}}" class="form-control">
                                        @if($errors->has('address'))
                                            <span class="help-block">{{$errors->first('address')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group @if($errors->has('phone'))
                                        has-warning
                                    @endif">
                                        <label for="">{{$phone->opt_title}}</label>
                                        <input type="text" name="phone" id="phone" value="{{$phone->opt_value}}" class="form-control">
                                        @if($errors->has('phone'))
                                            <span class="help-block">{{$errors->first('phone')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group @if($errors->has('amount'))
                                        has-warning
                                    @endif">
                                        <label for="">{{$amount->opt_title}}</label>
                                        <input type="text" name="amount" id="amount" value="{{$amount->opt_value}}" class="form-control">
                                        @if($errors->has('amount'))
                                            <span class="help-block">{{$errors->first('amount')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="seo_image">{{$about_img->opt_title}}</label>
                                        <div class="input-group">
                                           <span class="input-group-btn">
                                             <a id="lfm" data-input="about_img" data-preview="holder" class="btn btn-primary">
                                               <i class="fa fa-picture-o"></i> Choose
                                             </a>
                                           </span>
                                            <input id="about_img" name="about_img" class="form-control" value="{{$about_img->opt_value}}" type="text">
                                        </div>
                                        <span id="errimg" class="help-block"></span>
                                        <img id="holder" style="margin-top:15px;max-height:100px;" src="{{$about_img->opt_value}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="seo_image">{{$logo->opt_title}}</label>
                                        <div class="input-group">
                                           <span class="input-group-btn">
                                             <a id="lfms" data-input="logo" data-preview="holder" class="btn btn-primary">
                                               <i class="fa fa-picture-o"></i> Choose
                                             </a>
                                           </span>
                                            <input id="logo" name="logo" value="{{$logo->opt_value}}" class="form-control" type="text">
                                        </div>
                                        <span id="errimg" class="help-block"></span>
                                        <img id="holder" src="{{$logo->opt_value}}" style="margin-top:15px;max-height:100px;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group @if($errors->has('text_home'))
                                            has-warning
@endif">
                                        <label for="">{{$text_home->opt_title}}</label>
                                        <textarea type="text" name="text_home" id="text_home" class="form-control" rows="5">{{$text_home->opt_value}}</textarea>
                                        @if($errors->has('text_home'))
                                            <span class="help-block">{{$errors->first('text_home')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @if($errors->has('about_content'))
                                            has-warning
@endif">
                                        <label for="">{{$about_content->opt_title}}</label>
                                        <textarea type="text" name="about_content" id="about_content" class="form-control" rows="5">{{$about_content->opt_value}}</textarea>
                                        @if($errors->has('about_content'))
                                            <span class="help-block">{{$errors->first('about_content')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(function(){
            $('#lfm').filemanager('image');
            $('#lfms').filemanager('image');
            var options = {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/filemanager?type=Files',
                filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
            };
            var editor = CKEDITOR.replace('about_content', options);
            var text_home = CKEDITOR.replace('text_home', options);
        });

    </script>
@endsection
