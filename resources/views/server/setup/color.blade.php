@extends('layouts.server')


@section('title', 'Đổi màu background')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@yield('title')</h3>
                </div>
                @if(session()->has('succesi'))
                    <div class="alert alert-success">{{session()->get('succesi')}}</div>
                @endif
                <form action="{{route('admin.setup.color')}}" method="post" class="box-body">
                    @csrf
                    <div class="form-group">
                        <label for="bg-banner">Background Color Banner</label>
                        <input type="color" value="{{$bgcolor->opt_value}}" name="bgbanner" id="bgbanner" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="custom-css">Tùy chỉnh css</label>
                        <textarea name="customcss" id="customcss" type="text" rows="5" cols="10" class="form-control">{{$customcss->opt_value}}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <link href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/codemirror.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/codemirror.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/mode/css/css.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/mode/htmlmixed/htmlmixed.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/mode/javascript/javascript.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.13.4/mode/xml/xml.js"></script>
    <script>
        $(function(){
            var htmlEditor = CodeMirror.fromTextArea(document.getElementById("customcss"), {
                lineNumbers: true,
                mode: 'htmlmixed',
                // theme: 'default',
            });
        })
    </script>
@endsection