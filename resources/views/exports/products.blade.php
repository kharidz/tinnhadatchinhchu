<table>
    <thead>
        <tr>
            <th>Ngày</th>
            <th>Loại</th>
            <th>Tên Dự Án</th>
            <th>Nội dung</th>
            <th>Số Điện thoại</th>
            <th>Địa Chỉ</th>
            <th>Giá</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $k => $product)
            <tr>
                <td>{{$product->created_at->format('d/m/Y')}}</td>
                <td>{{$product->getcate()}}</td>
                <td>{{$product->name}}</td>
                <td>{{strip_tags(html_entity_decode($product->content))}}</td>
                <td>{{$product->contact_phone}}</td>
                <td>{{$product->getdistrict()}}</td>
                <td>{{($product->price) ? number_format($product->price). "VNĐ": "Liên hệ"}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
