<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::guard('admin')->user()->fullname }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <a href="{{URL::to('/')}}" target="blank" class="btn btn-primary btn-block">Xem website</a>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview {{active(['admin.page.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-sticky-note"></i> <span>Trang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.page.*'], 'block', 'none')}}">
            <li class="{{active(['admin.page.index'], 'active')}}"><a href="{{ route('admin.page.index') }}"><i class="fa fa-circle-o"></i> Tất cả các trang</a></li>
            <li class="{{active(['admin.page.create'], 'active')}}"><a href="{{ route('admin.page.create') }}"><i class="fa fa-circle-o"></i> Thêm trang mới</a></li>
          </ul>
        </li>
        <li class="treeview {{active(['admin.categories.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-tags"></i> <span>Danh mục bài viết</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.categories.*'], 'block', 'none')}}">
            <li class="{{active(['admin.categories.index'], 'active')}}"><a href="{{ route('admin.categories.index') }}"><i class="fa fa-circle-o"></i> Danh sách danh mục</a></li>
            <li class="{{active(['admin.categories.create'], 'active')}}"><a href="{{ route('admin.categories.create') }}"><i class="fa fa-circle-o"></i> Thêm danh mục mới</a></li>
          </ul>
        </li>
        <li class="treeview {{active(['admin.posts.*', 'admin.post.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-book"></i> <span>Bài viết</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.posts.*', 'admin.post.*'], 'block', 'none')}}">
            <li class="{{active(['admin.posts.index'], 'active')}}"><a href="{{ route('admin.posts.index') }}"><i class="fa fa-circle-o"></i> Danh sách bài viết</a></li>
            <li class="{{active(['admin.post.create'], 'active')}}"><a href="{{ route('admin.post.create') }}"><i class="fa fa-circle-o"></i> Thêm bài viết mới</a></li>
          </ul>
        </li>
        <li class="treeview {{active(['admin.product.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-product-hunt"></i> <span>Sản Phẩm</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.product.*'], 'block', 'none')}}">
            <li class="{{active(['admin.product.index'], 'active')}}"><a href="{{ route('admin.product.index') }}"><i class="fa fa-circle-o"></i> Danh sách sản phẩm</a></li>
            <li class="{{active(['admin.product.create'], 'active')}}"><a href="{{ route('admin.product.create') }}"><i class="fa fa-circle-o"></i> Thêm sản phẩm mới</a></li>
          </ul>
        </li>
        <li class="{{active(['admin.pro_category.index'], 'active')}}">
          <a href="{{ route('admin.pro_category.index') }}">
            <i class="fa fa-tags"></i> <span>Danh mục sản phẩm</span>
          </a>
        </li>
        <li class="treeview {{active(['admin.custom.*','admin.admin.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-user"></i> <span>Quản Lý Tài Khoản</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.custom.*','admin.admin.*', 'admin.payment.history'], 'block', 'none')}}">
            <li class="{{active(['admin.custom.index'], 'active')}}"><a href="{{ route('admin.custom.index') }}"><i class="fa fa-circle-o"></i> Khách hàng</a></li>
            <li class="{{active(['admin.admin.index'], 'active')}}"><a href="{{ route('admin.admin.index') }}"><i class="fa fa-circle-o"></i> Quản trị</a></li>
            <li class="{{active(['admin.payment.history'], 'active')}}"><a href="{{ route('admin.payment.history') }}"><i class="fa fa-circle-o"></i> Lịch Sử Nạp Tiền</a></li>
          </ul>
        </li>
	    <li class="treeview {{active(['admin.setup.*'], 'menu-open')}}">
          <a href="#">
            <i class="fa fa-user"></i> <span>Cài đặt</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: {{active(['admin.setup.*'], 'block', 'none')}}">
            <li class="{{active(['admin.setup.index'], 'active')}}"><a href="{{ route('admin.setup.index') }}"><i class="fa fa-circle-o"></i> Cài đặt trang</a></li>
            <li class="{{active(['admin.setup.banner'], 'active')}}"><a href="{{ route('admin.setup.banner') }}"><i class="fa fa-circle-o"></i> Chỉnh sửa Banner</a></li>
            <li class="{{active(['admin.setup.post'], 'active')}}"><a href="{{ route('admin.setup.post') }}"><i class="fa fa-circle-o"></i> Cập nhật bài đăng mới</a></li>
            <li class="{{active(['admin.setup.color'], 'active')}}"><a href="{{ route('admin.setup.color') }}"><i class="fa fa-circle-o"></i> Tùy chỉnh CSS</a></li>
          </ul>
        </li>
          <li class="treeview {{active(['admin.campaign.*'], 'menu-open')}}">
          <a href="#">
              <i class="fa fa-user"></i> <span>Chiến dịch gửi mail</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu " style="display: {{active(['admin.campaign.*'], 'block', 'none')}}">
              <li class="{{active(['admin.campaign.index'], 'active')}}"><a href="{{ route('admin.campaign.index') }}"><i class="fa fa-circle-o"></i> Danh Sách</a></li>
              <li class="{{active(['admin.campaign.create'], 'active')}}"><a href="{{ route('admin.campaign.create') }}"><i class="fa fa-circle-o"></i> Khởi tạo</a></li>
          </ul>
          </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
