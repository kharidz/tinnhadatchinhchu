<footer>
    <div class="email-info">
        <div class="container">
            <div class="title-home text-center">
                <h2>Stay tuned</h2>
                <p>Sign up to receive updates. You won’t want to miss out on any of our exciting new products.</p>
            </div>
            <form action="{{route('client.subscribes.post')}}" method="post">
                @csrf
                <input type="email" name="email" class="form-control" placeholder="Your email address">
                @if($errors->has('email'))
                    <span style="color:red;font-weight: 900;">{{$errors->first('email')}}</span>
                @endif
                <button type="submit" class="btn"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </form>
            <ul class="social-ft">
                <li><a href="#"><img src="{{asset('img/twitter.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/facebook.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/googleplus.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/instagram.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/youtube.png')}}" alt=""></a></li>
                <li><a href="#"><img src="{{asset('img/pinterest.png')}}" alt=""></a></li>
            </ul>
        </div>
    </div>
    <div class="ft">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="copyright-text copyright-left">
                        <strong>2016 © {{$title}}.</strong>
                    </div>

                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="footer_menu">
                        <ul>
                            <li><a href="{{URL::to('p/gioi-thieu')}}" title="Giới thiệu">Giới thiệu</a></li>
                            <li><a href="{{URL::to('p/san-pham-dich-vu')}}" title="Tin rao">Sản phẩm - Dịch vụ</a></li>
                            <li><a href="{{URL::to('product')}}" title="Tin rao">Nhà đất chính chủ</a></li>
                            <li><a href="{{URL::to('p/bang-gia')}}" title="Tin rao">Bảng giá</a></li>
                            <li><a href="{{URL::to('tintuc')}}" title="Tin tức">Tin tức</a></li>
                            <li><a href="{{URL::to('p/lien-he')}}" title="Liên hệ">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="{{ asset('js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
@yield('js')
</body>
</html>
