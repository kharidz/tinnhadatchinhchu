<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') {{($title) ? $title : env('APP_NAME')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <meta name="robots" content="noindex, nofollow">
    @yield('css')
</head>
<body>
<header>
    <div class="container">
        <div class="top-head">
            <div class="topbar-left-content">
                <ul id="top_menu" class="top-menu">
                    @auth
                        <li>
                            <span class="sprite login">&nbsp;</span>
                            <a href="{{ route('logout') }}" title="Thoát">Thoát</a>
                        </li>
                        <li><span class="icon-member">&nbsp;</span> <a href="/member">Trang cá nhân</a></li>
                        <li>
                            <span class="sprite register">&nbsp;</span><a href="javascript:ShowProfile()"
                                                                          title="Thay đổi thông tin cá nhân">{{Auth::user()->fullname}}</a>
                        </li>

                        @else
                        <li><span class="sprite register">&nbsp;</span><a href="{{ route('register') }}"
                                                                          title="Đăng ký tài khoản">Đăng ký</a></li>
                        <li><span class="sprite login">&nbsp;</span><a href="/login" title="Đăng nhập hệ thống">Đăng
                                nhập</a></li>
                    @endauth
                </ul>
            </div>
            <div class="topbar-right-content">
                <ul class="social-icons clearfix">
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li> <a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                    <li class="google"><a href="#" title="Google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>

                    <li class="youtube"><a href="#" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <li class="instagram"><a href="#" title="Instgram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                    <li class="pinterest"> <a href="#" title="Pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="menu">
            <div class="row">
                <div class="col-md-4 col-xs-6 col-sm-6">
                    <div class="logo">
                        <a title="{{$title}}" href="/"><img src="{{ $logo }}" alt="{{$title}}"></a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-6 col-sm-6">
                    <div class="menu-site">
                        <button class="btn btn-show-menu hidden-md hidden-lg"><i class="fa fa-bars"></i></button>
                        <div class="menu-box">
                            <div class="bg-menu hidden-md hidden-lg"></div>
                            <ul class="main-menu">
                                <li><a href="{{URL::to('p/gioi-thieu')}}" title="Giới thiệu">Giới thiệu</a></li>
                                <li><a href="{{URL::to('p/san-pham-dich-vu')}}" title="Tin rao">Sản phẩm - Dịch vụ</a></li>
                                <li><a href="{{URL::to('product')}}" title="Tin rao">Nhà đất chính chủ</a></li>
                                <li><a href="{{URL::to('p/bang-gia')}}" title="Tin rao">Bảng giá</a></li>
                                <li><a href="{{URL::to('tintuc')}}" title="Tin tức">Tin tức</a></li>
                                <li><a href="{{URL::to('p/lien-he')}}" title="Liên hệ">Liên hệ</a></li>
                            </ul>
                            <button class="btn btn-hide-menu hidden-md hidden-lg"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header><!-- /header -->
<div class="new">
    <div class="bn-about" style="background-image: url('/img/work-with-us-hero-1.jpg')">
        <div class="container">
            <h1><span>@yield('title')</span></h1>
        </div>
    </div>

