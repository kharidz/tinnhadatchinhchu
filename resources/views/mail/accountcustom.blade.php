<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thông tin tài khoản của bạn</title>
</head>
<body>
	Chào <b>{{ $user['fullname'] }}</b>
	<p>Dưới đây là thông tin tài khoản của bạn</p>
	<p>Email: {{ $user['username'] }}</p>
	<p>Mật khẩu: {{ $passwd }}</p>
	<p>Bạn truy cập đường dẫn sau để đăng nhập: <a href="{{ route('login') }}">Tại đây</a></p>
</body>
</html>