<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thông tin tài khoản quản trị của bạn</title>
</head>
<body>
	Chào <b>{{ $user['fullname'] }}</b>
	<p>Dưới đây là thông tin tài khoản quản trị của bạn</p>
	<p>Email: {{ $user['username'] }}</p>
	<p>Mật khẩu: {{ $passwd }}</p>
	<p>Bạn truy cập đường dẫn sau để đăng nhập vào trang quản trị: <a href="{{ route('admin.index') }}">Tại đây</a></p>
</body>
</html>