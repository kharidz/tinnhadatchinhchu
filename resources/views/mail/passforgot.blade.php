<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quên mật khẩu tài khoản</title>
</head>
<body>
	<p>Chào {{$user->fullname}}</p>
	<p>Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu của bạn.</p>
	<p><a href="{{ route('password.forgot.reset', $user->hash_code) }}">Nhấn vào đây để đổi mật khẩu của bạn.</a></p>
</body>
</html>