<!DOCTYPE html>
<html>
<head>
	<title>Xac Nhan Tai Khoan</title>
</head>
<body>
	<p>Chào quý khách <b>{{$user->fullname}}</b></p>
	<p>Chúc mừng Quý Khách đã đăng ký thành công</p>
	<ul>
		<li>
			<p>
				Để xác nhận e-mail của Quý Khách và chính thức trở thành thành viên
			</p>
			<p>
				Quý khách vui lòng nhấp chuột vào đây liên kết sau: <a href="{{ route('active', $user->hash_code) }}">Xác nhận E-mail đăng ký</a>
			</p>
		</li>
		<li>
			<p>
				Nếu trình duyệt không tự động mở ra, Quý Khách hãy sao chép liên kết dưới đây và dán vào trình duyệt để xác nhận.
			</p>
			<p>
				Liên kết: {{ route('active', $user->hash_code) }}
			</p>
		</li>
	</ul>
</body>
</html>